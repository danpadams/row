<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Angular View</title>

        <!-- Base Javascript -->
        <script type="text/javascript" src="/lib/angular.min.1.4.7.js"></script>
        <script type="text/javascript" src="/lib/ocLazyLoad.min.js"></script>
        <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/angular-ui-router/0.2.8/angular-ui-router.min.js"></script>
        <script type="text/javascript" src="/lib/ngStorage.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-sanitize.js"></script>
        <script type="text/javascript" src="/lib/select.min.js"></script>
        <script type="text/javascript" src="/lib/angular-spinners.min.js"></script>
        <script type="text/javascript" src="/lib/dirPagination.js"></script>
        <script type="text/javascript">
            var infoChi = angular.module("infoChi", [
                "ui.router",
                "ngStorage",
                'ngSanitize',
                'ui.select',
                'angularSpinners',
                'angularUtils.directives.dirPagination'
                ,
                "oc.lazyLoad"
            ]);
        </script>
        <script type="text/javascript" src="/js/states.js"></script>
        <script type="text/javascript" src="/js/infoChi.js"></script>
        <!-- Stylesheets -->
        <link rel="stylesheet" type="text/css" href="/css/selectize.default.css">
        <link rel="stylesheet" type="text/css" href="/css/selectize.css">
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/font-awesome-4.6.3/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/css/infochi.css">            
        <link rel="stylesheet" type="text/css" href="/css/style.css">
        <!-- Scripts -->
        <script type="text/javascript" src="/js/index.js"></script>
        <script type="text/javascript" src="/js/home.js"></script>
    </head>
    <body ng-app="infoChi">
    <spinner name="mainSpinner" class="spinner">
        <h1>
            <i class="fa fa-spinner fa-spin fa-5x" aria-hidden="true"></i><br>
            <span>Please Wait!</span>
        </h1>
    </spinner>
    <div id="container" ng-controller="defaultLayoutCtrl">
        <div class="center">
            <span style="font-size: 300%">
                <a id="areaTitle" style="color: #000; text-decoration: none;"
                   href="/">Reachout Wizard</a>
            </span>
            <br>
            {{current.Client.name}}
        </div>
        <div style="background-color: #006600; height: 5px;"></div>
        <div id="content row">
            <?php
            // if ($authUser) {
            ?>
            <div class="col-sm-2 well well-sm" style="text-align: center;">
                <a href="#/home">My Home</a>
            </div>
            <div class="col-sm-1 well well-sm" style="text-align: center;">
                <a href="/home">Home</a>
            </div>
            <div class="col-sm-3 well well-sm" style="text-align: center;">
                <a href="/messages">My Messages<span ng-if="newMsgs > 0"> ({{newMsgs}})</span></a>
            </div>
            <div class="col-sm-3 well well-sm" style="text-align: center;">
                <a href="http://www.infochi.net">About infoChi</a>
            </div>
            <div class="col-sm-3 well well-sm" style="text-align: center;">
                <a href="" ng-click="proLogout()">Logout</a>
            </div>
            <?php
            // }
            ?>
            <div class="col-xs-12">
                <ng-view></ng-view>
            </div>
        </div

    </div>
    <div>
        <h5 class="text-right">
            <br>
            <?php
            //if ($authUser) {
            ?>
            Questions or Comments - <a href="/users/feedback">Send Us Feedback</a><br>
            <?
            //}
            ?>
            <br>
            <strong>&copy;<?php echo date('Y');?> infoChi Christian Computing</strong><br>
            Scottsdale, Arizona
        </h5>
    </div>
</body>
</html>
