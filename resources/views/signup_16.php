<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Glamour Nails By Jamie</title>

    <!-- Base Javascript -->
    <script type="text/javascript" src="/js/jquery-1.11.3.js"></script> <!-- Datepicker -->
    <script type="text/javascript" src="/js/bootstrap.js"></script> <!-- Datepicker -->
    <script type="text/javascript" src="/lib/angular.min.1.4.7.js"></script>
    <!--<script type="text/javascript" src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>-->
    <!--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-animate.js"></script>-->

    <!-- Tooltips - Part of Angular Bootstrap -->
    <script type="text/javascript" src="/lib/ui-bootstrap-tpls-1.3.2.min.js"></script>

    <script type="text/javascript" src="/lib/angular-route.js"></script>
    <script type="text/javascript" src="/lib/ngStorage.js"></script>
    <script type="text/javascript"
            src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-sanitize.js"></script>
    <script type="text/javascript" src="/lib/select.min.js"></script>
    <script type="text/javascript" src="/lib/angular-spinners.min.js"></script>
    <script type="text/javascript" src="/lib/angular-flash.js"></script>
    <!-- Datepicker -->
    <script type="text/javascript" src="/js/moment-with-locales.min.js"></script>
    <script type="text/javascript" src="/lib/datetimepicker.js"></script>
    <script type="text/javascript" src="/lib/datetimepicker.templates.js"></script>
    <link rel="stylesheet" href="/css/datetimepicker.css"/>

    <!-- Graphing -->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js" charset="utf-8"></script>-->
    <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/nvd3/1.8.1/nv.d3.min.js"></script>-->
    <!--<script src="https://rawgit.com/krispo/angular-nvd3/v1.0.4/dist/angular-nvd3.js"></script>-->
    <script type="text/javascript" src="/lib/dirPagination.js"></script>
    <script type="text/javascript">
        var infoChi = angular.module("infoChi", [
            "ngRoute",
            "ngStorage",
            "ngFlash",
            'ngSanitize',
            'ui.select',
            'angularSpinners',
            'angularUtils.directives.dirPagination',
            'ui.bootstrap.datetimepicker',
            'ui.bootstrap'
        ]);
    </script>
    <!--<script type="text/javascript" src="/js/routes.js"></script>-->
    <script type="text/javascript" src="/js/infoChi.js"></script>
    <!-- Stylesheets -->
    <link rel="stylesheet" type="text/css" href="/css/selectize.default.css">
    <link rel="stylesheet" type="text/css" href="/css/selectize.css">
    <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
    <!--<link rel="stylesheet" type="text/css" href="/font-awesome-4.6.3/css/font-awesome.min.css">-->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css"
          integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/v4-shims.css">-->
    <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="/css/infochi.css">
    <link rel="stylesheet" type="text/css" href="/css/style.css">
    <!-- Scripts -->
    <script type="text/javascript" src="/js/people/create.js"></script>
</head>
<body ng-app="infoChi">
<spinner name="mainSpinner" class="spinner">
    <h1>
        <i class="fa fa-spinner fa-spin fa-5x" aria-hidden="true"></i><br>
        <span>Please Wait!</span>
    </h1>
</spinner>
<div id="container" ng-controller="peopleCreateController">
    <div class="center">
            <span style="font-size: 300%">
                <a id="areaTitle" style="color: #000; text-decoration: none;"
                   href="/">Glamour Nails By Jamie</a>
            </span>
    </div>
    <div style="background-color: #006600; height: 5px;"></div>
    <flash-message>
        <div class="well"></div>
    </flash-message>
    <div class="content row">
        <form ng-submit="submit()" class="well">
            <div>
                <div class="cos-xs-12 col-sm-3">
                    First Name
                </div>
                <div class="col-xs-12 col-sm-9">
                    <input ng-model="data.fname" type="text" size="60">
                </div>
            </div>
            <div>&nbsp;</div>
            <div>
                <div class="cos-xs-12 col-sm-3">
                    Last Name
                </div>
                <div class="col-xs-12 col-sm-9">
                    <input ng-model="data.sname" type="text" size="60">
                </div>
            </div>
            <div>&nbsp;</div>
            <div>
                <div class="cos-xs-12 col-sm-3">
                    Cell Phone
                </div>
                <div class="col-xs-12 col-sm-9">
                    <input ng-model="data.cell" type="text" size="60">
                </div>
            </div>
            <div>&nbsp;</div>
            <div>
                <div class="cos-xs-12 col-sm-3">
                    Email
                </div>
                <div class="col-xs-12 col-sm-9">
                    <input ng-model="data.email" type="text" size="60">
                </div>
            </div>
            <div>&nbsp;<br></div>
            <div>How did you hear about us?<br>
                <span>
                    <ui-select ng-model="data.metaDataEnum.value" theme="selectize"
                               ng-disabled="disabled" style="width: 400px;"
                               on-select="updateMetaEnum($select.selected)" allow-clear> <ui-select-no-choice>
                        </ui-select-no-choice> <ui-select-match
                                placeholder="Let us know?">{{data.metaDataEnum.value.name }}</ui-select-match> <ui-select-choices
                                repeat="dat in metaDataEnum[9] | filter: $select.search"> <span
                                    ng-bind-html="dat.name | highlight: $select.search"></span> </ui-select-choices>
                    </ui-select>
                </span></div>
            <div>&nbsp;<br></div>
            <div>
                <input type="checkbox" ng-model="data.checkbox[51]">
                I am interested in hosting a home nail bar (home party).
            </div>
            <div>
                <input type="checkbox" ng-model="data.checkbox[52]">
                I am interested in hosting a catalog/FaceBook party (online party).
            </div>

            <div>
                <input type="checkbox" ng-model="data.checkbox[53]">
                I would like to learn about the VIP Customer Group on Facebook.
            </div>
            <div>
                <input type="checkbox" ng-model="data.checkbox[54]">
                I am interested in learning more about what you do.
            </div>
            <div>&nbsp;<br></div>
            <div>
                My prefered method of being contacted is:<br>
                <input type="radio" ng-model="data.preferred" value="email" id="email" name="preferred">
                <label for="email">EMail</label><br>
                <input type="radio" ng-model="data.preferred" value="sms" id="sms" name="preferred">
                <label for="text">Text Message</label>
            </div>
            <div>&nbsp;</div>
            <div class="row">
                <div class="col-sm-offset-9 col-sm-3">
                    <input type="submit" value="Submit"
                           class="btn btn-block btn-success center">
                </div>
            </div>
        </form>
    </div>
</div>
<div>
    <h5 class="text-right">
        <br>
        <?php
        //if ($authUser) {
        ?>
        Questions or Comments - <a href="/users/feedback">Send Us Feedback</a><br>
        <?
        //}
        ?>
        <br>
        <strong>&copy;<?php echo date('Y'); ?> infoChi Christian Computing</strong><br>
        Scottsdale, Arizona
    </h5>
</div>


</body>
</html>
