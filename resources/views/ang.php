<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Reachout Wizard</title>

        <!-- Base Javascript -->
        <script type="text/javascript" src="/js/jquery-1.11.3.js"></script> <!-- Datepicker -->
        <script type="text/javascript" src="/js/bootstrap.js"></script> <!-- Datepicker -->
        <script type="text/javascript" src="/lib/angular.min.1.4.7.js"></script>
        <!--<script type="text/javascript" src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>-->
        <!--<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-animate.js"></script>-->   

        <!-- Tooltips - Part of Angular Bootstrap -->
        <script type="text/javascript" src="/lib/ui-bootstrap-tpls-1.3.2.min.js"></script>

        <script type="text/javascript" src="/lib/angular-route.js"></script>
        <script type="text/javascript" src="/lib/ngStorage.js"></script>
        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.3/angular-sanitize.js"></script>
        <script type="text/javascript" src="/lib/select.min.js"></script>
        <script type="text/javascript" src="/lib/angular-spinners.min.js"></script>
        <script type="text/javascript" src="/lib/angular-flash.js"></script>
        <!-- Datepicker -->
        <script type="text/javascript" src="/js/moment-with-locales.min.js"></script>
        <script type="text/javascript" src="/lib/datetimepicker.js"></script>
        <script type="text/javascript" src="/lib/datetimepicker.templates.js"></script>
        <link rel="stylesheet" href="/css/datetimepicker.css"/>

        <!-- File Upload -->
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/danialfarid-angular-file-upload/12.2.13/ng-file-upload.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/danialfarid-angular-file-upload/12.2.13/ng-file-upload-shim.min.js"></script>

        <!-- Graphing -->            
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.6/d3.min.js" charset="utf-8"></script>-->
        <!--<script src="https://cdnjs.cloudflare.com/ajax/libs/nvd3/1.8.1/nv.d3.min.js"></script>-->
        <!--<script src="https://rawgit.com/krispo/angular-nvd3/v1.0.4/dist/angular-nvd3.js"></script>-->
        <script type="text/javascript" src="/lib/dirPagination.js"></script>
        <script type="text/javascript">
            var infoChi = angular.module("infoChi", [
                "ngRoute",
                "ngStorage",
                "ngFlash",
                'ngSanitize',
                'ui.select',
                'angularSpinners',
                'angularUtils.directives.dirPagination',
                'ui.bootstrap.datetimepicker',
                'ui.bootstrap'
            ]);
        </script>
        <script type="text/javascript" src="/js/routes.js"></script>
        <script type="text/javascript" src="/js/infoChi.js"></script>
        <!-- Stylesheets -->
        <link rel="stylesheet" type="text/css" href="/css/selectize.default.css">
        <link rel="stylesheet" type="text/css" href="/css/selectize.css">
        <link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
        <!--<link rel="stylesheet" type="text/css" href="/font-awesome-4.6.3/css/font-awesome.min.css">-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/v4-shims.css">-->
        <link rel="stylesheet" type="text/css" href="/css/jquery-ui.css">
        <link rel="stylesheet" type="text/css" href="/css/infochi.css">            
        <link rel="stylesheet" type="text/css" href="/css/style.css">
        <!-- Scripts -->
        <script type="text/javascript" src="/js/index.js"></script>
        <script type="text/javascript" src="/js/home.js"></script>

        <script type="text/javascript" src="/js/clients/index.js"></script>
        <script type="text/javascript" src="/js/clients/edit.js"></script>
        <script type="text/javascript" src="/js/clients/create.js"></script>

        <script type="text/javascript" src="/js/events/index.js"></script>
        <script type="text/javascript" src="/js/events/create.js"></script>
        <script type="text/javascript" src="/js/events/invite.js"></script>
        <script type="text/javascript" src="/js/events/attendance.js"></script>
        <script type="text/javascript" src="/js/events/attendance_history.js"></script>

        <script type="text/javascript" src="/js/messages/index.js"></script>
        <script type="text/javascript" src="/js/messages/view.js"></script>

        <script type="text/javascript" src="/js/log_logins/index.js"></script>

        <script type="text/javascript" src="/js/logs/index.js"></script>
        <script type="text/javascript" src="/js/logs/view.js"></script>
        <script type="text/javascript" src="/js/logs/person.js"></script>

        <script type="text/javascript" src="/js/meta_columns/index.js"></script>
        <script type="text/javascript" src="/js/meta_columns/view.js"></script>
        <script type="text/javascript" src="/js/meta_columns/create.js"></script>

        <script type="text/javascript" src="/js/numbers/index.js"></script>

        <script type="text/javascript" src="/js/outgoing_messages/index.js"></script>
        <script type="text/javascript" src="/js/outgoing_messages/view.js"></script>

        <script type="text/javascript" src="/js/p_branches/view.js"></script>
        
        <script type="text/javascript" src="/js/p_trees/index.js"></script>
        <script type="text/javascript" src="/js/p_trees/view.js"></script>

        <script type="text/javascript" src="/js/people/index.js"></script>
        <script type="text/javascript" src="/js/people/create.js"></script>
        <script type="text/javascript" src="/js/people/find.js"></script>
        <script type="text/javascript" src="/js/people/view.js"></script>
        <script type="text/javascript" src="/js/people/address.js"></script>

        <script type="text/javascript" src="/js/person_groups/index.js"></script>
        <script type="text/javascript" src="/js/person_groups/create.js"></script>
        <script type="text/javascript" src="/js/person_groups/view.js"></script>

        <script type="text/javascript" src="/js/queue_messages/index.js"></script>
        <script type="text/javascript" src="/js/queue_messages/create.js"></script>

        <script type="text/javascript" src="/js/shorts/index.js"></script>
        <script type="text/javascript" src="/js/shorts/view.js"></script>

        <script type="text/javascript" src="/js/enum_m_c_types/index.js"></script>
        <script type="text/javascript" src="/js/enum_m_c_types/create.js"></script>
        <script type="text/javascript" src="/js/enum_m_c_types/view.js"></script>

        <script type="text/javascript" src="/js/enum_p_kinds/index.js"></script>
        <script type="text/javascript" src="/js/enum_p_kinds/create.js"></script>
        <script type="text/javascript" src="/js/enum_p_kinds/view.js"></script>

        <script type="text/javascript" src="/js/enum_p_types/index.js"></script>
        <script type="text/javascript" src="/js/enum_p_types/create.js"></script>
        <script type="text/javascript" src="/js/enum_p_types/view.js"></script>

        <script type="text/javascript" src="/js/enum_tooltips/index.js"></script>
        <script type="text/javascript" src="/js/enum_tooltips/create.js"></script>
        <script type="text/javascript" src="/js/enum_tooltips/view.js"></script>

        <script type="text/javascript" src="/js/users/index.js"></script>
        <script type="text/javascript" src="/js/users/create.js"></script>
        <script type="text/javascript" src="/js/users/view.js"></script>

        <script type="text/javascript" src="/js/custom_emails/index.js"></script>
        <script type="text/javascript" src="/js/custom_emails/view.js"></script>

        <script type="text/javascript" src="/js/domains/index.js"></script>
    </head>
    <body ng-app="infoChi">
    <spinner name="mainSpinner" class="spinner">
        <h1>
            <i class="fa fa-spinner fa-spin fa-5x" aria-hidden="true"></i><br>
            <span>Please Wait!</span>
        </h1>
    </spinner>
    <div id="container" ng-controller="defaultLayoutCtrl">
        <div class="center">
            <span style="font-size: 300%">
                <a id="areaTitle" style="color: #000; text-decoration: none;"
                   href="/">Reachout Wizard</a>
            </span>
            <br>
            {{Current.Client.name}}
        </div>
        <div style="background-color: #006600; height: 5px;"></div>
        <div id="content row">
            <?php
            // if ($authUser) {
            ?>
            <div class="col-sm-3 well well-sm" style="text-align: center;">
                <a href="#/home">My Home</a>
            </div>
            <div class="col-sm-3 well well-sm" style="text-align: center;">
                <a href="#/messages">My Messages<span ng-if="newMsgs > 0"> ({{newMsgs}})</span></a>
            </div>
            <div class="col-sm-3 well well-sm" style="text-align: center;">
                <a href="http://www.infochi.net">About infoChi</a>
            </div>
            <div class="col-sm-3 well well-sm" style="text-align: center;">
                <a href="" ng-click="proLogout()">Logout</a>
            </div>
            <?php
            // }
            ?></div>
        <div class="content row">
            <flash-message>
                <div class="well"></div>
            </flash-message>
        </div>
        <div class="content row">
            <div class="col-xs-12">
                <ng-view></ng-view>
            </div>
        </div>
    </div>
    <div>
        <h5 class="text-right">
            <br>
            <?php
            //if ($authUser) {
            ?>
            Questions or Comments - <a href="http://www.reachoutwizard.com/contact/">Send Us Feedback</a><br>
            <?
            //}
            ?>
            <br>
            <strong>&copy;<?php echo date('Y');?> infoChi Christian Computing</strong><br>
            Scottsdale, Arizona
        </h5>
    </div>


</body>
</html>
