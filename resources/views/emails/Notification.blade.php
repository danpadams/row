<p>You have a new message.</p>
<p>From: <blockquote>{{$Note['user_message']['message']['from']}}
    @if (isset($Note['user_message']['message']['person']['id']))
        {{$Note['user_message']['message']['person']['fname']}} {{$Note['user_message']['message']['person']['sname']}}
    @endif
</blockquote></p>
    @if ($Note['user_message']['message']['brief'])
       Message Brief:
<blockquote>
    {{$Note['user_message']['message']['brief']}}
    @if ($Note['user_message']['message']['brief'] != $Note['user_message']['message']['body'])
    ...
    @endif
</blockquote>
    @endif
<p>
    <a href="{{$Link}}">Login and View Your Message.</a>
</p>
<div style="font-size:smaller;">Brought to you by Reachout Wizard.</div>