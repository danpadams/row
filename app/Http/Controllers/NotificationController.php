<?php

namespace App\Http\Controllers;

use App\Model\Notification;
use App\Mail\NotificationMessage as NotificationMessageMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class NotificationController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    public function run() {
        $notifications = Notification::where('notified', '=', 0)
                ->where('passed', '=', 0)
                ->with('UserMessage')
                ->with('UserMessage.User')
                ->with('UserMessage.Message')
                ->with('UserMessage.Message.Person')
                ->with('UserMessage.MStatus')
                ->with('PersonMethod')
                ->with('PersonMethod.Person')
                ->with('PersonMethod.PersonMethodType')
                ->get();
//        echo "<p>" . __METHOD__ . ':' . __LINE__ . "</p>\n";
//        print_r($notifications);

        foreach ($notifications as $notification) {
            $note = $notification->toArray();
//            echo "<p>" . __METHOD__ . ':' . __LINE__ . "</p>\n";
//            print_r($note);

            $passed = 0;
            $notified = false;

            // Do not Process checks
            if ($note['user_message']['m_status_id'] == 3) {
                // Individual's Message is already Closed
                $passed = 3;
            }
            if ($note['user_message']['m_status_id'] == 4) {
                // Individual's Message is already Closed
                $passed = 4;
            }

            if (!$passed) {
                // Check Preferred Method
                if ($note['person_method']['person_method_type_id'] == 10) {
//                if (!is_numeric($notification['PersonMethod']['method'])) {
//                    $this->log(__METHOD__ . print_r($notification, true));
//                    $notification['PersonMethod']['person_method_type_id'] = 3;
//                }
                }

                switch ($note['person_method']['person_method_type_id']) {
                    case 3:
                        $notified = $this->sendEmail($note);
                        break;
                }
            }

            // Save Notification so it is not run again
            if ($notified) {
                $notification->notified = 1;
            }
            if ($passed) {
                $notification->passed = $passed;
            }
            if ($notified || $passed) {
                echo ("\tNotification Sent ID:" . $note['id'] . "\n");
                $notification->save();
            }
        }
        
        echo "End\n";
    }

    private function sendEmail($note) {
        $App = config('app.url');
        Mail::to($note['person_method']['method'])
                            ->send(new NotificationMessageMail($note, $App));

        return true;
    }

}
