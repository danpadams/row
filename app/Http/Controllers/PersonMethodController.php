<?php

namespace App\Http\Controllers;

use App\Model\Client;
use Illuminate\Http\Request;
use App\Model\PersonMethod;
use App\Model\PersonMethodType;
use Illuminate\Support\Facades\Log;

class PersonMethodController extends Controller
{

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
//        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Helps front-end determine if contact is from current client and if a change is necessary
     * Does not invoke the change, just tells the front end that a change is needed
     * This is due to the fact that there are many steps needed for a change on the front end
     *
     * @param string $pm_id
     * @return array
     */
    public function check_client($pm_id)
    {
        $retval['pm_id'] = $pm_id;

        $PersonMethod = PersonMethod::where('id', $pm_id)
            ->with('Person')
            ->first();
        $retval['client_id'] = $PersonMethod->person->client_id;

        $retval['change_client'] = false;
        if ($this->request->session()->get('client.id') != $PersonMethod->person->client_id) {
            $retval['change_client'] = true;
        }

        return $retval;
    }

    /**
     * @param string $pm_ids CSV list
     * @return array
     */
    public function check_recips($pm_ids)
    {
        if (is_numeric($pm_ids)) {
            $ids = [$pm_ids];
        } else {
            $ids = explode(',', $pm_ids);
        }
        $retval['ids'] = $ids;

        $Methods = PersonMethod::whereIn('id', $ids)
            ->with('Person')
            ->get()
            ->toarray();

        $optout = false;
        $email_only = false;
        for ($i = 0; $i < count($Methods) && !($optout || $email_only); $i++) {
            // Person Opt Out
            if (!$optout && $Methods[$i]['person']['opt_out_status']) {
                $optout = true;
            }
            // Email Only
            if ((!$email_only && $this->request->session()->get('client.hasNumbers') == 0) && !(strpos($Methods[$i]['method'], '@') !== FALSE)) {
                $email_only = true;
            }
        }


        // Person Opt Out
        if ($optout) {
            $retval['messages'][] = [
                'message' => 'One or more of the people you are sending messages to have \'Opt Out\' status, as a result the system is unable to send a direct message to them',
                'statusFlash' => 'warning'
            ];
        }

        if ($email_only) {
            $retval['messages'][] = [
                'message' => 'This client is configured as Email Only, as a result you will not be able to send messages to phone numbers',
                'statusFlash' => 'danger'
            ];
        }

        return $retval;
    }


    public function create()
    {
        $this->detectType();
        $PersonMethod = $this->save();
        $PersonMethod->message = 'The method has been created.';
        $PersonMethod->status = 'success';
        return $this->validateMethod($PersonMethod);
    }

    public function update()
    {
        $PersonMethod = $this->save();
        $PersonMethodType = new PersonMethodType();
        $method_name = $PersonMethodType->lookup($this->request->person_method_type_id);
        $PersonMethod->message = "The '$method_name' method has been updated.";
        return $PersonMethod;
    }

    private function save()
    {
        // Update a method
        if ($this->request->id) {
            $PersonMethod = PersonMethod::where('id', '=', $this->request->id)->first();
        } else {
            $PersonMethod = new PersonMethod();
        }
        $PersonMethod->setProperty('person_id', $this->request)
            ->setProperty('person_method_type_id', $this->request)
            ->setProperty('method', $this->request);
        $PersonMethod->save();
        return $PersonMethod;
    }

    public function delete()
    {
        // Delete an item permanently
        PersonMethod::where('id', '=', $this->request->id)->delete();
        $retval = [
            'message' => 'The selected method has been deleted'
        ];
        return $retval;
    }

    private function detectType()
    {
        if ($this->request->person_method_type_id == 0) {
            // Check for Email
            if (strpos($this->request->method, '@') !== false) {
                $this->request->person_method_type_id = 3;
            }
            // Check for Phone
            if ($this->request->person_method_type_id == 0 && is_numeric($this->request->method)) {
                $this->request->person_method_type_id = 1;
            }
        }
    }

    private function validateMethod($PersonMethod)
    {
        if ($PersonMethod->person_method_type_id == 0) {
            $PersonMethod->message = 'The method\'s type will need to be adjusted.';
            $PersonMethod->status = 'warning';
        }
        $valid = 0;
        if (!$valid && is_numeric($PersonMethod->metod)) {
            $valid = 1;
        }
        if (!$valid && strpos($this->request->method, '@') === false) {
            $valid = 1;
        }
        if (!$valid) {
            $PersonMethod->message = 'Examine the method as the format does not appear to be correct.';
            $PersonMethod->status = 'warning';
        }
        return $PersonMethod;
    }
}
