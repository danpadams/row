<?php

namespace App\Http\Controllers;

use App\Component\Attachment;
use App\Component\Feature;
use App\Model\Log as LogModel;
use App\Model\LogNote;
use App\Model\Message;
use App\Model\PersonMethod;
use App\Model\Domain;
use App\Model\CustomEmail;
use App\Component\Settings;
use App\Model\UserDetail as User;
use App\Model\UserMessage;
use App\Component\Messaging;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EmailController extends Controller
{

    private $request;
    private $Messaging;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->Messaging = new Messaging();
    }

    /**
     * Universal receive from Mailgun
     *
     * @return \Illuminate\Http\Response
     */
    public function receive()
    {
        $data = $this->request->toArray();
        //Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($data, true));
        $addresses = $this->getAddresses($data);
        $client_id = 0;

        // Start Log
        $Log = new LogModel();
        $Log->action = 'Inbound Message';
        $Log->save();

        // Get UserIds for delivery of message
        switch ($addresses['To']['Type']) {
            case 'custom':
                $Domain = Domain::where('domain', '=', $addresses['To']['Domain'])
                    ->first();
                $CustomEmail = CustomEmail::where('domain_id', '=', $Domain->id)
                    ->where('username', '=', $addresses['To']['Recip'])
                    ->first();
                // CatchAll
                if (empty($CustomEmail)) {
                    $CustomEmail = CustomEmail::where('domain_id', '=', $Domain->id)
                        ->where('username', '=', 'catchall')
                        ->first();
                }
                $Settings = new Settings($CustomEmail->settings);
                $UserIds = $Settings->getUsers('recipients');
                $client_id = $CustomEmail->client_id;
//                $client_id = $Settings->getClientId();
                $presub = $CustomEmail->name . ': ';

                break;
            case 'user':
                $User = User::find($addresses['To']['Recip']);
                $client_id = $User->primary_client_id;
                $UserIds = [$User->id];
                $presub = '';
                break;
        }

        // Save message and deliver
        if (count($UserIds)) {
            $message_id = $this->saveMessage($addresses, $client_id, $Log->id, $presub);

            for ($i = 0; $i < count($UserIds); $i++) {
                // Place in Mailbox of User
                $UserMessage = new UserMessage();
                $UserMessage->message_id = $message_id;
                $UserMessage->m_status_id = 1; // New
                $UserMessage->user_id = $UserIds[$i];
                $UserMessage->save();

                // Add Notify by Email
                Messaging::notifybyEmail($UserIds[$i], $UserMessage->id);
            }
        }

        // Finish out log
        $Log->brief = $this->request->subject;
        $Log->client_id = $client_id;
        $Log->version = 2;
        $Log->save();
    }

    private function getAddresses($data = array())
    {
        $addresses = array(
            'To' => array(
                'Actual' => $data ['recipient']
            ),
            'From' => array(
                'Actual' => $data ['From']
            )
        );
        foreach ($addresses as $key => $address) {
            $addresses [$key] = $this->parseAddress($address ['Actual']);
            if ($key == 'To') {
                $sects = explode('@', $addresses [$key] ['Address']);
                $addresses[$key]['Domain'] = $sects [1];
                $addy = array_reverse(explode('.', $sects[0]));

                if (isset($addy[1])) {
                    $addresses[$key]['Type'] = $addy[1];
                } else {
                    $addresses[$key]['Type'] = 'custom';
                }
                $addresses[$key]['Recip'] = $addy[0];
            }
        }

        return $addresses;
    }

    private function parseAddress($Actual)
    {
        $Address = array();
        $pos = strpos($Actual, '<');
        if ($pos) {
            // Add Part to remove <>
            $Name = substr($Actual, 0, $pos);
            $Address ['Name'] = $Name;
            $Actual = substr($Actual, ($pos + 1), (strpos($Actual, '>', $pos) - ($pos + 1)));
        }
        $Address ['Address'] = $Actual;
        return $Address;
    }

    private function saveMessage($addresses, $client_id, $log_id, $presub)
    {
        $Message = new Message();
        $data = $this->request->toArray();

        // Create Message
        if (!empty($data['body-html'])) {
            $Message->body = $data['body-html'];
        }
        if (empty($Message->body) && !empty($data['body-plain'])) {
            $Message->body = $data['body-plain'];
        }
        $Message->brief = $presub . $this->request->Subject;
        $Message->from = $addresses['From']['Address'];
        $Message->to = $addresses['To']['Address'];
        $Message->client_id = $client_id;

        // Fetch Person Message is From - check from address
        $PersonMethodModel = new PersonMethod();
        $PersonMethod = $PersonMethodModel->find($addresses['From']['Address'], $client_id);
        if (!empty($PersonMethod[0])) {
            $Message->person_method_id = $PersonMethod[0]->id;
            $Message->person_id = $PersonMethod[0]->person_id;
        }

        // Set & Create Message
        $Message->m_type_id = 3;
        $Message->log_id = $log_id;
        $Message->save();
        $message_id = $Message->id;
        // Check for Attachment if Feature is Held by Client
        $Features_Held = $this->get_features($client_id);
        // Feature 13 - Attachments
        if (in_array(13, $Features_Held)) {
            foreach ($this->request->toArray() as $key => $datum) {
                if (strpos($key, 'attachment') !== false &&
                    strpos($key, 'attachment-count') == false) {

                    Attachment::saveAttachment($datum, $client_id, $log_id);
                }
            }
        }
        Log::info(__METHOD__ . ':' . __LINE__ . ' Message ID: ' . $message_id);
        $this->recordLogNote($log_id, $message_id, $addresses);

        return $message_id;
    }

    /**
     * Set & create lognote
     *
     * @param int $log_id
     * @param int $message_id
     * @param array $addresses
     * @return int $LogNote->id
     */
    private function recordLogNote($log_id, $message_id, $addresses)
    {
        $LogNote = new LogNote();
        $LogNote->message_id = $message_id;
        $LogNote->version = 2;
        $LogNote->inbound = 1;
        $LogNote->log_id = $log_id;

        $Message = Message::find($message_id);
        if (!empty($Message)) {
            $LogNote->person_id = $Message->person_id;
        }

        $LogNote->setProperty('subject', $this->request);
        $LogNote->setProperty('body', $this->request);
        // From Address
        if (isset($addresses['From']['Name'])) {
            $LogNote->from_name = $addresses['From']['Name'];
        }
        $LogNote->from_addy = $addresses['From']['Address'];
        // To Address
        if (isset($addresses['To']['Name'])) {
            $LogNote->dest_name = $addresses['To']['Name'];
        }
        $LogNote->dest_addy = $addresses['To']['Address'];

        // Subject, Body
        $LogNote->setProperty('subject', $this->request);
        $LogNote->setProperty('body', $this->request);

        $LogNote->save();
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($LogNote->id, true));
        return $LogNote->id;
    }

    private function get_features($client_id)
    {
        $FeatureList = Feature::list([
            'client_id' => $client_id,
            'client_role' => 'admin'
        ]);
        foreach ($FeatureList as $feature) {
            $retval[] = $feature->feature->id;
        }
        return $retval;
    }
}
