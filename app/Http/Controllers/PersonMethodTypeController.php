<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\PersonMethodType;
use Illuminate\Support\Facades\Log;

class PersonMethodTypeController extends Controller {

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function lis() {
        $PersonMethodTypeModel = new PersonMethodType();
        $Types = $PersonMethodTypeModel->get();
        return $Types;
    }

}
