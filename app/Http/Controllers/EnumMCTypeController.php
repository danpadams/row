<?php

namespace App\Http\Controllers;

use App\Model\MCType as myModel;
use App\Model\MetaEnumValue as EnumModel;
use Illuminate\Http\Request;

class EnumMCTypeController extends Controller {

    public function __construct(Request $request) {
        // Require Logged In
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $Data = myModel::get();
        return $Data;
    }

    public function view($dat_id) {
        $Item = myModel::find($dat_id);
        return $Item;
    }

    public function update() {
        $Item = myModel::find($this->request->id);
        $Item->setProperty('name', $this->request)
                ->setProperty('display', $this->request)
                ->setProperty('client_id', $this->request);
        $Item->save();
        return $Item;
    }

    public function create() {
        $Item = new myModel();
        $Item->setProperty('name', $this->request)
                ->setProperty('display', $this->request);
        $Item->client_id = 0;
        $Item->save();
        return $Item;
    }

    public function enum_view($dat_id) {
        $Model = new EnumModel();
        $retval = $Model->where('mc_type_id', '=', $dat_id)
                ->get();
        return $retval;
    }

    public function enum_update() {
        $Enum = EnumModel::find($this->request->id);
        $Enum->setProperty('name', $this->request);
        $Enum->save();

        return $Enum;
    }

    public function enum_create() {
        $Enum = new EnumModel();
        $Enum->setProperty('name', $this->request)
                ->setProperty('mc_type_id', $this->request);
        $Enum->save();
        return $Enum;
    }

    public function enum_remove() {
        EnumModel::find($this->request->id)
                ->delete();
        return [];
    }

}
