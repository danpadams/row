<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Person;
use App\Model\PersonMethod;
use App\Model\PersonMethodType;
use App\Model\LandingPage;
use App\Model\ZipCode;
use App\Component\PhoneSystem;
use App\Component\Distance;
use App\Component\Internal;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class PersonController extends Controller
{

    private $request;
    private $phoneSystem;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->phoneSystem = new PhoneSystem();
    }

    public function index($deleted)
    {
        Log::info(__METHOD__);
        $PersonModel = new Person();
        $People = $PersonModel
            ->where('client_id', '=', $this->request->session()->get('client.id'))
            ->where('deleted', '=', $deleted)
            ->orderBy('sname')
            ->orderBy('fname')
            ->get();

        foreach ($People as $Person) {
            if (!empty($Person->lname)) {
                $Person->label = $Person->sname . ', ' . $Person->fname;
            } else {
                $Person->label = $Person->fname;
            }
        }
        return $People;
    }

    /*
     * Heere
     */
    public function create()
    {
        $Person = new Person();
        list($client_id, $checkval) = LandingPage::getCurrent($this->request);

        $Person->setProperty('fname', $this->request)
            ->setProperty('sname', $this->request)
            ->setProperty('address', $this->request)
            ->setProperty('city', $this->request)
            ->setProperty('state', $this->request)
            ->setProperty('zip', $this->request);
        $Person->client_id = $client_id;
        $Person->save();

        $person_id = $Person->id;
        // Create Internal if Domain Variable
        if (!empty($checkval)) {
            $Internal = new Internal();
            // Set Message
            $Message = [
                'action' => 'Internal Notification',
                'brief' => 'Signup from Blog.',
                'body' => 'A new signup has been received from the Blog website'
            ];
            $From = [
                'from' => 'system@reachoutwizard.com',
                'from_name' => 'ReachoutWizard System'
            ];
            $user_ids = [1, 2];

            $Internal->create($Message, $client_id, $user_ids, now(), $From, $person_id);
        }

        return $Person;
    }

    public function update()
    {
        if ($this->request->id > 0) {
            // Retreive data for an individual Short
            $PersonModel = new Person();
            $Person = $PersonModel->find($this->request->id);
        }
        Log::info(__METHOD__);
        $Person->setProperty('fname', $this->request)
            ->setProperty('sname', $this->request)
            ->setProperty('address', $this->request)
            ->setProperty('city', $this->request)
            ->setProperty('state', $this->request)
            ->setProperty('zip', $this->request)
            ->setProperty('deleted', $this->request)
            ->setProperty('opt_out_status', $this->request);
        $Person->save();
        return $Person;
    }

    public function find($method_text)
    {
        $PersonMethodModel = new PersonMethod();
        $Objs = $PersonMethodModel
            ->with('Person')
            ->where('method', 'like', '%' . $method_text . '%')
            ->get();

        $person_ids = [];
        $retval = [];

        foreach ($Objs as $Obj) {
            if ($Obj->person_id == 0) {
                continue;
            }
            if ($Obj->person->deleted == 1) {
                continue;
            }
            if ($Obj->person->client_id != $this->request->session()->get('client.id')) {
                continue;
            }

            if (!in_array($Obj->person_id, $person_ids)) {
                $person_ids[] = $Obj->person_id;
                $retval[] = $Obj->toarray();
            }
        }

        return $retval;
    }

    public function view($person_id)
    {
        // Retreive data for an individual Person
        $PersonModel = new Person();
        $Person = $PersonModel->find($person_id);

        return $Person;
    }

    public function view_methods($person_id)
    {
        $retval = [];
        // Retreive contact_methods for an individual Person
        $PersonMethodModel = new PersonMethod();
        $PersonMethodTypeModel = new PersonMethodType();
        $Methods = $PersonMethodModel
//                ->with('PersonMethodType')
            ->where('person_id', '=', $person_id)
            ->get();
        foreach ($Methods as $Method) {
            $Element = $Method->toArray();
            $Element['Type'] = $PersonMethodTypeModel->lookup($Method->person_method_type_id);
            $Element['forMethod'] = $this->phoneSystem->formatPhone($Method->method);
            $retval[] = $Element;
        }
        return $retval;
    }

    public function address()
    {
        $PersonModel = new Person();
        // For Lookup
        $PersonMethodTypeModel = new PersonMethodType();
        // Retrieve list of all people that are active in company.
        $People = $PersonModel
            ->where('client_id', '=', $this->request->session()->get('client.id'))
            ->where('deleted', '=', 0)
            ->with('PersonMethod')
            ->orderBy('sname')
            ->orderBy('fname')
            ->get();

        $retval = [];
        foreach ($People as $Person) {
            $Element = $Person->toArray();
            for ($i = 0; $i < count($Element['person_method']); $i++) {
                $Method = $Element['person_method'][$i];
                $Method['person_method_formatted'] = $this->phoneSystem->formatPhone($Method['method']);
                $Method['person_method_type_name'] = $PersonMethodTypeModel->lookup($Method['person_method_type_id']);
                $Method['label'] = $Method ['person_method_type_name'] . ': ' . $Method ['person_method_formatted'];
                $Element['PersonMethod'][] = $Method;
            }
            unset($Element['person_method']);
            $retval[] = $Element;
        }

        return $retval;
    }

    public function distance($localZipCode)
    {
        // Generate list of distances for each zip within current client
        // Get active zips
        $PersonModel = new Person();
        $ZipCodeModel = new ZipCode();
        // Get Local Zip Codes Lat/Long
        $ZipCodeModel->checkZipCode($localZipCode);
        $People = $PersonModel
            ->where('client_id', '=', $this->request->session()->get('client.id'))
            ->where('deleted', '=', 0)
            ->where('zip', '!=', '')
            ->with('ZipCode')
            ->distinct('zip')
            ->get(['zip']);
        $Zips = $People->toArray();

        $ZipCodes = [];

        for ($i = 0; $i < count($Zips); $i++) {
            $ZipCodes[$Zips[$i]['zip_code']['id']] = $Zips[$i]['zip_code'];
        }
        $Distance = new Distance();
        for ($i = 0; $i < count($Zips); $i++) {
            $ZipCodes[$Zips[$i]['zip_code']['id']]['Distance'] = $Distance->vincentyGreatCircleDistance(
                $Zips[$i]['zip_code']['lat'], $Zips[$i]['zip_code']['long'], $ZipCodeModel->lat, $ZipCodeModel->long, 3959
            );
        }

        return $ZipCodes;
    }

    public function checkZipCodes()
    {
//        echo __METHOD__ . "\n";
//        return;
        $PersonModel = new Person();
        $ZipCodeModel = new ZipCode();

        // Get Zip Codes list, and where lookup is not present
        $People = Person::where('deleted', '=', 0)
            ->where('persons.zip', '!=', '')
//                ->with('ZipCode')
            ->leftjoin('zip_codes as zip_code', 'zip_code.id', '=', 'persons.zip')
//                ->whereNull('ZipCode.id')
            ->whereNull('zip_code.id')
            ->distinct('persons.zip')
            ->get(['persons.zip']);

        $data = $People->toArray();
//        print_r($data);
//        return;

        $retval = [];
        // If lookup is not found, run lookup
        for ($i = 0; $i < count($data); $i++) {
//                if (!$data[$i]['zip_code']) {
            $retval[] = $data[$i]['zip'];
            $ZipCodeModel->checkZipCode($data[$i]['zip']);
//                }
        }


//        echo __METHOD__ . "\n";
//        print_r($retval);
        return $retval;
    }

}
