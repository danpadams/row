<?php

namespace App\Http\Controllers;

use App\Model\Person;
use Illuminate\Http\Request;
use App\Model\UserMessage;
use App\Model\Message;
use App\Model\Log as LogModel;
use App\Model\MType;
use App\Model\MStatus;
use App\Model\PersonMethod;
use App\Model\ClientUser;
use App\Model\UserDetail as User;
use App\Component\Time;
use App\Component\PhoneSystem;
use Illuminate\Support\Facades\Log;

class MessageController extends Controller {

    private $Time;
    private $PhoneSystem;
    private $request;
    private $MStatus;
    private $MType;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->middleware('auth');
        $this->request = $request;
        $this->Time = new Time($request);
        $this->PhoneSystem = new PhoneSystem();
        $this->MStatus = new MStatus();
        $this->MType = new MType();
    }

    public function index($status = 0, $client_id = 0) {
        $retval = [];
        $UserMessageModel = UserMessage::with('message')
                ->with('message.person')
                ->with('message.client')
                ->where('user_id', '=', $this->request->session()->get('user.id'))
                ->orderBy('created', 'desc')
        ;

        // Max Status Level set by Front End
        if ($status) {
            $UserMessageModel = $UserMessageModel->where('m_status_id', '<=', $status);
        }
        $UserMessages = $UserMessageModel->get();
        foreach ($UserMessages as $UserMessage) {
            if ($client_id) {
                if ($UserMessage->message->client_id != $client_id) {
                    // Skip Item if not the correct client
                    continue;
                }
            }
            $retval[] = $this->formatMessage($UserMessage->toArray());
        }

        return $retval;
    }

    private function formatMessage($UserMessage) {
        // Date to Local Format and Timezone
        if ($UserMessage['message']['created'] != '0000-00-00 00:00:00') {
            $Created = $UserMessage['message']['created'];
        } else {
            $Created = $UserMessage['message']['created_at'];
        }
        if ($UserMessage['updated'] != '0000-00-00 00:00:00') {
            $Updated = $UserMessage['updated'];
        } else {
            $Updated = $UserMessage['updated_at'];
        }
        $UserMessage['message']['Created'] = $this->Time->toLocal($Created);
        $UserMessage['Updated'] = $this->Time->toLocal($Updated);

        if (!$UserMessage['updated_at'])
            $UserMessage['updated_at'] = $UserMessage['updated'];
        $UserMessage['Updated'] = $this->Time->toLocal($UserMessage['updated_at']);
        if ($UserMessage['message']['read_date']) {
            $UserMessage['message']['ReadDate'] = $this->Time->toLocal($UserMessage['message']['read_date']);
        }
        // Format From Address
        $UserMessage['message']['forFrom'] = $this->PhoneSystem->formatPhone($UserMessage['message']['from']);
        $UserMessage['message']['From'] = $UserMessage['message']['forFrom'];
        if (isset($UserMessage['message']['person']['id'])) {
            if (!empty($UserMessage['message']['person']['sname'])) {
                $UserMessage['message']['person']['forName'] = $UserMessage['message']['person']['sname'] . ', ' . $UserMessage['message']['person']['fname'];
            } else {
                $UserMessage['message']['person']['forName'] = $UserMessage['message']['person']['fname'];
            }
            $UserMessage['message']['forFrom'] .= ' - ' . $UserMessage['message']['person']['forName'];
        }

        // Using a do/while to enable the break statement to short curcuit logic
        $UserMessage['showClose'] = 0;
        do {
            // Allow close if message is standard open
            if ($UserMessage['m_status_id'] == 3) {
                $UserMessage['showClose'] = 1;
                break;
            }
            // Do not allow close if already closed
            if ($UserMessage['m_status_id'] == 4) {
                break;
            }
            // If brief == body, then allow close
            if ($UserMessage['message']['brief'] == $UserMessage['message']['body']) {
                $UserMessage['showClose'] = 1;
                break;
            }
        } while (false); // Allow Breakout of Loop Early
        // Status
        if (isset($UserMessage['m_status_id'])) {
            $UserMessage['m_statusName'] = $this->MStatus->lookup($UserMessage['m_status_id'], 'name');
        }
        if (isset($UserMessage['m_status_id'])) {
            $UserMessage['m_statusLabel'] = $this->MStatus->lookup($UserMessage['m_status_id'], 'label');
        }
        // Type
        if (isset($UserMessage['message']['m_type_id'])) {
            $UserMessage['message']['m_typeName'] = $this->MType->lookup($UserMessage['message']['m_type_id'], 'name');
        }
        if (isset($UserMessage['message']['m_type_id'])) {
            $UserMessage['message']['m_typeLabel'] = $this->MType->lookup($UserMessage['message']['m_type_id'], 'label');
        }

        return $UserMessage;
    }

    private function fetchMessage($id) {
        $UserMessage = UserMessage::where('user_id', '=', $this->request->session()->get('user.id'))
                ->where('message_id', '=', $id)
//                ->with('MStatus')
                ->with('Message')
                ->with('Message.Person')
                ->with('Message.Client')
                ->with('Message.Person')
                ->with('Message.MType')
                ->first();
        return $UserMessage;
    }

    public function view($message_id) {
        $UserMessage = $this->fetchMessage($message_id);
        /*
         * @todo: Verify Permissions
         */
        // Mark as Open if New, UserMessage Level
        if ($UserMessage->m_status_id == 1) {
            $UserMessage->m_status_id = 3;
        }
        // Mark Message as Updated
        $UserMessage->save();
        // Display Stuff
        return $this->formatMessage($UserMessage->toArray());
    }

    public function contact_lookup($message_id) {
        $retval = [];
        $retval['SetContact'] = 0;

        list($retval, $Message, $from) = $this->getMessage($message_id, $retval);

        $PersonMethods = PersonMethod::where('method', '=', $from)
                ->with('Person')
                ->get();
        foreach ($PersonMethods as $PersonMethod) {
            if ($PersonMethod->person->deleted) {
                continue;
            }
            if ($PersonMethod->person->client_id != $Message->client_id) {
                continue;
            }
            $MethodFound = $PersonMethod;
            break;
        }

        if (isset($MethodFound)) {
            $Message = Message::find($message_id);
            $Message->person_method_id = $MethodFound->id;
            $Message->person_id = $MethodFound->person_id;
            $Message->save();
            $retval['SetContact'] = 1;
            $retval['PersonMethod'] = $MethodFound;
        }
        return $retval;
    }

    public function contact_create($message_id, $person_id = 0)
    {
        $retval = [];
        $retval['ContactCreated'] = 0;

        try {
            list($retval, $Message, $from) = $this->getMessage($message_id, $retval);

            $client_id = $Message->client_id;
            $retval['from'] = $from;

            if ($person_id == 0) {
                $Person = new Person();
                $Person->client_id = $client_id;
                $Person->fname = $from;
                $Person->save();
                $person_id = $Person->id;
            }

            $PersonMethod = new PersonMethod();
            $PersonMethod->person_id = $person_id;
            // Set Method Type based on messag
            $PersonMethod->person_method_type_id = $Message->m_type_id;
            $PersonMethod->method = $from;
            $PersonMethod->save();

            $Message->person_method_id = $PersonMethod->id;
            $Message->person_id = $person_id;
            $Message->save();

            $retval['ContactCreated'] = 1;
        } catch (\Exception $e) {
            // TODO Notify Dan
        }
        return $retval;
    }

    public function users_with_copy($message_id) {
        $Users = UserMessage::where('message_id', '=', $message_id)
                ->with('User')
                ->get();
        return $Users;
    }

    public function share_users() {
        $retval = [];
        // Get current allowed clients
        $allowedClients = [$this->request->session()->get('client.id')];
        $ClientUsers = ClientUser::whereIn('client_id', $allowedClients)
                ->with('User')
                ->get();
        foreach ($ClientUsers as $ClientUser) {
            if (isset($ClientUser->User->id)) {
                // Allow for Future idea of not having a user more than once
                if (true) {
                    $retval[] = [
                        "id" => $ClientUser->User->id,
                        "label" => $ClientUser->User->email_name . ' - ' . $ClientUser->User->email_address
                    ];
                }
            }
        }
        return $retval;
    }

    public function share_notify() {
        // Share Message
        $UserMessage = new UserMessage();
        $UserMessage->setProperty('message_id', $this->request)
                ->setProperty('user_id', $this->request);
        $UserMessage->m_status_id = 1;
        $UserMessage->save();

        // Add Notify
        $this->PhoneSystem->notifybyEmail($this->request->user_id, $UserMessage->id);
    }

    public function msg_close() {
        $UserMessage = UserMessage::find($this->request->user_message_id);
        /*
         * @todo: Verify Persmissions
         */
        $UserMessage->m_status_id = 4;
        $UserMessage->save();
    }

    public function msg_mark_spam() {
        $UserMessage = UserMessage::find($this->request->user_message_id);
        /*
         * @todo: Verify Persmissions
         */
        $UserMessage->m_status_id = 4;
        $UserMessage->save();

        $Message = Message::find($UserMessage->message_id);
        $Message->message_spam_source_id = 1;
        $Message->save();

        $Log = LogModel::find($Message->log_id);
        $Log->hide = 1;
        $Log->save();
    }

    public function msg_status() {
        $UserMessage = UserMessage::find($this->request->user_message_id);
        /*
         * @todo: Verify Persmissions
         */
        if ($this->request->status_id != 2) {
            /*
             * @todo: Log Error
             */
            return;
        }
        $UserMessage->m_status_id = $this->request->status_id;
        $UserMessage->save();
    }

    private function getMessage($message_id, $retval){
        $Message = Message::find($message_id);

        $from = $Message['from'];
        if (substr($from, 0, 2) == '+1') {
            $from = substr($from, 2);
        }
        $retval['from'] = $from;
        $retval['client_id'] = $Message->client_id;

        return array($retval, $Message, $from);
    }

}
