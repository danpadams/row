<?php

namespace App\Http\Controllers;

use App\Model\ClientFeature;
use App\Model\ClientUserFeature;
use Illuminate\Http\Request;
use App\Model\UserDetail as myModel;
use App\Model\User;
use App\Model\ClientUser;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Web\ClientController;

class UserController extends Controller
{

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function index()
    {
        $ClientUser = new ClientUser();
        $Data = User::with('ClientUser')
            ->with('ClientUser.Client')
            ->get();
        $retval = [];
        $user_role = $this->request->session()->get('user.role');
        $client_id = $this->request->session()->get('client.id');

        foreach ($Data as $Dat) {
            $element = $Dat->toArray();
            // Clients to Add
            $addClients = $ClientUser->getAddList($element['id']);
            // Check ID
            $element['Clients'] = [];
            foreach ($element['client_user'] as $CU) {
                if ($user_role != 'admin' && $CU['client']['id'] != $client_id) {
                    continue;
                }
                $element['Clients'][] = [
                    'id' => $CU['client']['id'],
                    'name' => $CU['client']['name'],
                    'role' => $CU['role'],
                    'client_user_id' => $CU['id']
                ];
            }
            unset($element['client_user']);
            // Clients to Add
            if ($this->request->session()->get('user.role') == 'admin') {
                $myClients = [];
                foreach ($element['Clients'] as $myClient) {
                    $myClients[$myClient['id']] = $myClient;
                }

                $element['addClients'] = $ClientUser->getAddList($myClients);
            } else {
                $element['addClients'] = [];
            }
            $retval[] = $element;
        }
        return $retval;
    }

    public function view($dat_id)
    {
        // Check ID for permissions to edit / view
        $Item = myModel::find($dat_id);
        return $Item;
    }

    public function update()
    {
        // Check ID for permissions to edit / view
        try {
            if (!isset($this->request->person_id) || $this->request->person_id == 0) {
                throw new \Exception('Person must be set');
            }
            $Item = myModel::find($this->request->id);
            $Item->setProperty('primary_client_id', $this->request)
                ->setProperty('person_id', $this->request)
                ->setProperty('email_name', $this->request)
                ->setProperty('email_primary', $this->request)
                ->setProperty('email_address', $this->request);
            $Item->save();
            $Item->message = 'User\'s Data Update Successfull.';
            $Item->status = 'success';
        } catch (\Exception $e) {
            $Item = array(
                'message' => $e->getMessage(),
                'status' => 'danger'
            );
        }
        return $Item;
    }

    // List Supplier --
    public function clients($user_id)
    {
        $retval = [];
        $ClientUsers = ClientUser::where('user_id', '=', $user_id)
            ->with('Client')
            ->get();
        foreach ($ClientUsers as $ClientUser) {
            $retval[] = [
                'id' => $ClientUser->Client->id,
                'label' => $ClientUser->Client->name
            ];
        }
        return $retval;
    }

    public function people($client_id)
    {
        $retval = [];
        $People = \App\Model\Person::where('client_id', '=', $client_id)
            ->where('deleted', '=', 0)
            ->get();
        foreach ($People as $Person) {
            $retval[] = [
                'id' => $Person->id,
                'label' => $Person->sname . ', ' . $Person->fname
            ];
        }
        return $retval;
    }

    private function getUser($email)
    {
        $user = User::where('email', $email)->first();
        if (!isset($user->id)) {
            $user = new User();
            $user->create();
            $user->email = $email;
            $user->save();
        }
        return $user;
    }

    public function updPasswd()
    {
        try {
            Log::info($this->request->email);
            $User = $this->getUser($this->request->email);
            Log::info(__METHOD__ . ':' . $User->id);

            if (($this->request->session()->get('user.role') != 'admin') ||
                ($this->request->session()->get('user.username')) == $this->request->email) {
                if (!Hash::check($this->request->old_password, $User->password)) {
                    throw(new \Exception('Incorrect Old Password'));
                }
            }
            $User->password = Hash::make($this->request->new_password);
            $User->save();
            $User->message = 'Password Update Sucessfull.';
            $User->status = 'success';
        } catch (\Exception $e) {
            $User = array(
                'message' => $e->getMessage(),
                'status' => 'danger'
            );
        }
        return $User;
    }

    public function deleteFromClient()
    {
        // Todo - Add Security Check - for the feature needed
        $id = $this->request->id;
        $retval = ['success' => false, 'id' => $id];
        try {
            $ClientUser = ClientUser::find($id);
            $ClientUser->delete();
            $retval['success'] = true;
        } catch (\Exception $e) {
            Log::info(__METHOD__ . ':' . __LINE__ . ' Error Deleting ClientUser-' . $id . ' : ' . $e->getMessage());
        }
        return $retval;
    }

    public function addToClient()
    {
        $client_id = $this->request->client_id;
        $user_id = $this->request->user_id;
        $ClientUser = new ClientUser();
        $ClientUser->user_id = $user_id;
        $ClientUser->client_id = $client_id;
        $ClientUser->role = 'user';
        $ClientUser->save();
        return $ClientUser;
    }

    public function create()
    {
        $ClientCont = new ClientController($this->request);
        Log::info(__METHOD__ . ':' . __LINE__ . ' - ' . $this->request->session()->get('client.id'));
        return $ClientCont->create($this->request->session()->get('client.id'));
    }

    public function features_list()
    {
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . $this->request->session()->get('client.id'));
        $clientFeatures = ClientFeature::where('client_id', '=', $this->request->session()->get('client.id'))
            ->with('Feature')
            ->get();
        $retval = [];
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($clientFeatures->toarray(), true));
        foreach ($clientFeatures as $feature) {
            $retval[] = [
                "id" => $feature->feature_id,
                "name" => $feature->feature->name
            ];
        }
        return $retval;
    }

    private function features_user_($user_id)
    {
        $userFeatures = ClientUserFeature::where('client_id', '=', $this->request->session()->get('client.id'))
            ->where('user_id', '=', $user_id)
            ->with('Feature')
            ->get();
        $retval = [];
        foreach ($userFeatures as $feature) {
            $retval[] = [
                "id" => $feature->id,
                "feature_id" => $feature->feature_id,
                "name" => $feature->feature->name
            ];
        }
        return $retval;

    }

    public function features_user($user_id)
    {
        return $this->features_user_($user_id);
    }

    public function feature_add()
    {
        $user_id = $this->request->user_id;
        $feature_id = $this->request->feature_id;
        $client_id = $this->request->session()->get('client.id');
        $userFeature = new ClientUserFeature();
        $userFeature->user_id = $user_id;
        $userFeature->feature_id = $feature_id;
        $userFeature->client_id = $client_id;
        $userFeature->save();

        return $this->features_user_($user_id);
    }

    public function feature_remove()
    {
        $user_id = $this->request->user_id;
        $item_id = $this->request->item_id;
        $userFeature = ClientUserFeature::find($item_id);
        $userFeature->delete();

        return $this->features_user_($user_id);
    }

}