<?php

namespace App\Http\Controllers\Web;

use App\Model\ClientFeature;
use App\Model\Number;
use App\Model\Person;
use App\Model\PersonMethodType;
use App\Model\OutgoingMessage;
use App\Model\Short;
use App\Model\ClientUser;
use App\Model\CustomEmail;
use App\Model\Feature;
use App\Component\PhoneSystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use App\Model\CustomEmailList;

class ListController extends Controller
{

    private $PhoneSystemComponent;
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->PhoneSystemComponent = new PhoneSystem();
    }

    public function email()
    {
        // Generate Email Address used for sending a message out
        $retval = [];
        $Session = $this->request->session();
        $CustomEmails = $this->getCustomEmails();

        if (empty($CustomEmails)) {
            $Element1['email_name'] = $Session->get('user.name');
            $Element1['email_address'] = 'user.' . $Session->get('user.id') . '@' . config('row.domain.System');
            $Element1['label'] = '' . $Element1['email_name'] . ' - ' . $Element1['email_address'] . '';
//            if ($Session->get('user.email_primary')) {
                $retval[] = $Element1;
//                unset($Element1);
//            }

//            $element ['email_name'] = $Session->get('user.name');
//            $element ['email_address'] = $Session->get('user.email');
//            $element ['label'] = '' . $element['email_name'] . ' - ' . $element['email_address'] . ' (External)';
//            $retval[] = $element;
//            if (isset($Element1)) {
//                $retval[] = $Element1;
//                unset($Element1);
//            }

            $retval = $this->getMyCustomEmail($this->request->session()->get('client.id'), $this->request->session()->get('user.id'), $retval);
        } else {
            $retval = $CustomEmails;
        }
        return $retval;
    }

    private function getCustomEmails()
    {
        $retval = [];

        $emailName = $this->request->Session()->get('user.name');

        $CustomEmailLists = CustomEmailList::where('client_id', '=', $this->request->session()->get('client.id'))
            ->where('user_id', '=', $this->request->session()->get('user.id'))
            ->orderBy('sort')
            ->get();

        foreach ($CustomEmailLists as $CustomEmailList) {
            $element['email_name'] = $emailName;
            if (!empty($CustomEmailList->name)) {
                $element['email_name'] = $CustomEmailList->name;
            }
            $element['email_address'] = $CustomEmailList->email;
            $element['label'] = '' . $element['email_name'] . ' - ' . $element['email_address'];
            if (!empty($CustomEmailList->suffix)) {
                $element['label'] .= ' ' . $CustomEmailList->suffix;
            }
            $retval[] = $element;
        }
        return $retval;
    }

    public function phone()
    {
        $NumbersModel = new Number();
        $Numbers = $NumbersModel
            ->where('closed', '=', 0)
            ->where('client_id', '=', $this->request->session()->get('client.id'))
            ->get();
        $retval = [];
//        $PhoneSystemComponent = new PhoneSystem();
        foreach ($Numbers as $Number) {
            $retval[] = [
                "id" => $Number->number,
                "number" => $Number->number,
                "forNumber" => $this->PhoneSystemComponent->formatPhone($Number->number)
            ];
        }
        return $retval;
    }

    public function contacts()
    {
        $retval = [];

        $PersonModel = new Person();
        $People = $PersonModel
            ->where('deleted', '=', 0)
            ->where('client_id', '=', $this->request->session()->get('client.id'))
            ->where('opt_out_status', 0)
            ->with('PersonMethod')
            ->get();

        $PersonMethodTypeModel = new PersonMethodType();
        foreach ($People as $Person) {
            // Skip if Opted Out
            if ($Person->opt_out_status == 1) {
                continue;
            }
            $Methods = $Person->PersonMethod->toArray();
            for ($i = 0; $i < count($Methods); $i++) {
                // Run Check for Email Only
                if (($this->request->session()->get('client.hasNumbers') == 0) && !(strpos($Methods[$i]['method'], '@') !== FALSE)) {
                    continue;
                }
                // Create Element for Display 
                $Element = [
                    'person_method_id' => $Methods[$i]['id'],
                    'type_id' => $Methods[$i]['person_method_type_id'],
                    'method' => $Methods[$i]['method'],
                    'person_id' => $Person->id,
                    'name' => $Person->sname . ', ' . $Person->fname,
                ];
                $Element['type'] = $PersonMethodTypeModel->lookup($Element['type_id']);
                $Element['forMethod'] = $this->PhoneSystemComponent->formatPhone($Element['method']);
                $Element['label'] = $Element['name'] . ': ' . $Element['type'] . ' - ' . $Element['forMethod'];
                $retval[] = $Element;
            }
        }

        return $retval;
    }

    public function contacts_email()
    {
        $retval = [];

        $ClientUsers = ClientUser::where('client_id', '=', $this->request->session()->get('client.id'))
            ->with('User')
            ->with('User.Person')
            ->with('User.Person.PersonMethod')
            ->get();

//        $People = Person::where('deleted', '=', 0)
//                ->where('client_id', '=', $this->request->session()->get('client.id'))
//                ->with('PersonMethod')
//                ->get();

        $PersonMethodTypeModel = new PersonMethodType();
        foreach ($ClientUsers as $ClientUser) {
            if ($ClientUser->User && $ClientUser->User->Person) {
                $Person = $ClientUser->User->Person;
            } else {
                continue;
            }

//            break;
            $Methods = $Person->PersonMethod->toArray();
            for ($i = 0; $i < count($Methods); $i++) {
                // Run Check for Email Only
                if ($Methods[$i]['person_method_type_id'] != 3) {
                    continue;
                }

                // Create Element for Display 
                $Element = [
                    'user_id' => $ClientUser->user_id,
                    'person_method_id' => $Methods[$i]['id'],
                    'type_id' => $Methods[$i]['person_method_type_id'],
                    'method' => $Methods[$i]['method'],
                    'person_id' => $Person->id,
                    'name' => $Person->sname . ', ' . $Person->fname,
                ];
                $Element['type'] = $PersonMethodTypeModel->lookup($Element['type_id']);
                $Element['forMethod'] = $this->PhoneSystemComponent->formatPhone($Element['method']);
                $Element['label'] = $Element['name'] . ': ' . $Element['type'] . ' - ' . $Element['forMethod'];
                $retval[] = $Element;
            }
        }

        return $retval;
    }

    public function outgoing_message()
    {
        $retval = [];
        // Generate list of Peset Messages
        $OutgoingMessageModel = new OutgoingMessage();
        $List = OutgoingMessage::where('client_id', '=', $this->request->session()->get('client.id'))
            ->with('Short')
            ->get();

        foreach ($List as $Lis) {
            $retval[] = $Lis->toArray();
        }
        return $retval;
    }

    public function short()
    {
        $ShortModel = new Short();
        $List = Short::where('client_id', '=', $this->request->session()->get('client.id'))
            ->where('deleted', '=', 0)
            ->get();

        $retval = [];
        foreach ($List as $Lis) {
            $retval[] = [
                "id" => $Lis->id,
                "name" => $Lis->name
            ];
        }
        return $List;
    }

    public function response()
    {

    }

//    $this->getMyCustomEmail($client_id, $user_id, $retval);
    private function getMyCustomEmail($client_id, $user_id, $retval = [])
    {
        $Customs = CustomEmail::where('client_id', '=', $client_id)
            ->with('Domain')
            ->orderBy('name')
            ->orderBy('username')
            ->get();
        foreach ($Customs as $Custom) {
            $address = $Custom->username . '@' . $Custom->domain->domain;
            $retval[] = [
                'email_name' => $Custom->name,
                'email_address' => $address,
                'label' => $Custom->name . ' - ' . $address . ' (Custom)'
            ];
        }

        return $retval;
    }

    public function features()
    {
        $Features = Feature::get();
        $clientFeatures = $this->currentFeatures();
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($clientFeatures, true));
        $retval = [];
        foreach ($Features as $feature) {
            if (!in_array($feature->id, $clientFeatures)) {
                $retval[] = [
                    "id" => $feature->id,
                    "name" => $feature->name
                ];
            }
        }
        return $retval;
    }

    //
    private function currentFeatures()
    {
        $ClientFeatures = ClientFeature::where('client_id', '=', $this->request->session()->get('client.id'))->get();
        $clientFeatures = [];
        foreach ($ClientFeatures as $clientFeature) {
            $clientFeatures[] = $clientFeature->feature_id;
        }
        return $clientFeatures;
    }
}
