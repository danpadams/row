<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Model\PBranch;
use App\Model\PTree;
use App\Model\PType;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class PBranchController extends Controller
{

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function gen_tree($p_type_id, $p_tree_id)
    {
        return $this->generate($p_type_id, 'p_tree', $p_tree_id, 0);
    }

    public function gen_branch($p_type_id, $p_tree_id, $parent_id)
    {
        return $this->generate($p_type_id, 'p_branch', $p_tree_id, $parent_id);
    }

    private function generate($p_type_id, $set_type, $p_tree_id, $parent_id)
    {
        $location_name = 'p_branch';

        // Search for existing branch that could be used
        $PBranch = PBranch::where('client_id', '=', $this->request->session()->get('client.id'))
            ->where('parent_id', '=', $parent_id)
            ->where('p_tree_id', '=', $p_tree_id)
            ->where('p_type_id', '=', $p_type_id)
            ->first();

        $Data = [];
        if ($PBranch) {
            // Already Existing Branch Node
            $Data['id'] = $PBranch->id;
            $Data['name'] = $PBranch->name;
        } else {
            // Create new Branch to be used
            $PBranch = new PBranch();
            $PBranch->parent_id = $parent_id;
            $PBranch->p_tree_id = $p_tree_id;
            $PBranch->p_type_id = $p_type_id;
            $PBranch->client_id = $this->request->session()->get('client.id');
            $PBranch->settings = serialize([]);
            $PBranch->next_id = 0;
            $PBranch->name = 'To Be Named';
            $PBranch->save();

            $Data['id'] = $PBranch->id;
            $Data['name'] = $PBranch->id;
            $Data['p_tree_id'] = $p_tree_id;
        }

        // Set Parent node to reference the new node as the next item
        if ($set_type === 'p_tree') {
            // Using tree for parent node
            if ($p_tree_id) {
                PTree::where('id', '=', $p_tree_id)
                    ->update(['p_branch_id' => $Data['id']]);
            } else {
                $PType = PType::where('id', '=', $p_type_id)
                    ->first();

                // Generate new PTree
                $PTree = new PTree();
                $PTree->name = 'To be named';
                $PTree->p_branch_id = $Data['id'];
                $PTree->p_kind_id = $PType->p_kind_id;
                $PTree->client_id = $this->request->session()->get('client.id');
                $PTree->save();
                $Data['p_tree_id'] = $PTree->id;

                // Update $PBrach
                $PBranch->p_tree_id = $Data['p_tree_id'];
                $PBranch->save();
            }
        }

        if ($set_type === 'p_branch') {
            PBranch::where('id', '=', $parent_id)
                ->update(['next_id' => $Data['id']]);
        }
        return $Data;
//    }
    }

    /**
     * @param $p_branch_id int
     * @return mixed
     */
    public function view($p_branch_id)
    {
        $PBranch = PBranch::where('id', '=', $p_branch_id)
            ->with('Tree')
            ->with('Type')
            ->with('ParentBranch')
            ->with('NextBranch')
            ->with('NextBranch.type')
            ->first();
        $PBranch->Settings = unserialize($PBranch->settings);
        if (!empty($PBranch->Settings['branch_yes'])) {
            $PBranch->branch_yes = $PBranch->Settings['branch_yes'];
        }
        if (!empty($PBranch->Settings['branch_no'])) {
            $PBranch->branch_no = $PBranch->Settings['branch_no'];
        }
        return $PBranch;
    }

    public function save()
    {
        $Data = $this->request->toArray();
        $Data['settings'] = serialize($this->request->Settings);
        $PBranch = PBranch::find($this->request->id);
        $PBranch->setFromArray('name', $Data)
            ->setFromArray('settings', $Data);
        $PBranch->save();

        $PBranch->message = 'The Data was saved successfully';
        $PBranch->status = 'success';
        return $PBranch;
    }
}
