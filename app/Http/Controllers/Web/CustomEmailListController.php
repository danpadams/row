<?php

namespace App\Http\Controllers\Web;

use App\Model\CustomEmailList;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CustomEmailListController extends Controller
{

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index($dat_id)
    {
        return $this->getData($dat_id);
    }

    public function save()
    {
        $user_id = 0;
        foreach ($this->request->data as $datum) {
            $CustomEmail = CustomEmailList::find($datum['id']);
            if ($user_id == 0) {
                $user_id = $datum['user_id'];
            }
            $CustomEmail->sort = $datum['sort'];
            $CustomEmail->save();
        }

        $retval = $this->getData($user_id);
        $retval['message'] = 'Custom Email List Save Sucessfull.';
        $retval['status'] = 'success';

        return $retval;
    }

    private function getData($user_id)
    {
        $CustomEmailList = CustomEmailList::where('client_id', '=', $this->request->session()->get('client.id'))
            ->where('user_id', '=', $user_id)
            ->orderBy('sort')
            ->get()
            ->toarray();

        return ['CustomEmailList' => $CustomEmailList];
    }

}
