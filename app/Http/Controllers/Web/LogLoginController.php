<?php

namespace App\Http\Controllers\Web;

use App\Model\LogLogin;
use App\Component\Time;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class LogLoginController extends Controller {

    private $Time;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->Time = new Time($request);
    }

    public function index($offset = 0) {
        $Logins = LogLogin::with('User')
                ->with('Client')
                ->orderBy('created_at', 'DESC')
                ->offset($offset)
                ->limit(100)
                ->get();
        $retval = [];
        foreach ($Logins as $Login) {
            $element = $Login->toArray();
            Log::info(__METHOD__ . print_r($element, true));
            $element['effTime'] = $this->Time->toLocal($element['created_at']);
            $retval[] = $element;
        }
        return $retval;
    }

}
