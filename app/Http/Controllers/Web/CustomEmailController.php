<?php

namespace App\Http\Controllers\Web;

use App\Model\CustomEmail;
use App\Http\Controllers\Controller;
use App\Model\Domain;
use App\Model\QueueCustom;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CustomEmailController extends Controller
{

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $CustomEmails = CustomEmail::where('client_id', '=', $this->request->session()->get('client.id'))
            ->where('deleted','=',0)
            ->with('Domain')
            ->get();
        foreach ($CustomEmails as $customEmail) {
            $customEmail->Settings = unserialize($customEmail->settings);
        }

        return $CustomEmails;
    }

    public function view($dat_id = null)
    {
        $CustomEmails = CustomEmail::with('Domain')->find($dat_id);
        $CustomEmails->Settings = unserialize($CustomEmails->settings);
        return $CustomEmails;
    }

    public function save()
    {
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($this->request->toArray(), true));
        // Save Data
        if ($this->request->id == 0) {
            $CustomEmail = new CustomEmail();
        } else {
            $CustomEmail = CustomEmail::find($this->request->id);
        }
        $CustomEmail->client_id = $this->request->session()->get('client.id');
        $CustomEmail->setProperty('name', $this->request);
        $CustomEmail->setProperty('username', $this->request);
        $CustomEmail->setProperty('domain_id', $this->request);
        $CustomEmail->settings = serialize($this->request->settings);
        $CustomEmail->save();

        // Trigger QueueCustom
        $this->triggerQueueCustom('custom_email', $this->request->session()->get('client.id'));

        // Prepare new Data
        $CustomEmail->message = 'The email has been saved.';
        $CustomEmail->domain = Domain::find($CustomEmail->domain_id);
        $CustomEmail->Settings = unserialize($CustomEmail->settings);
        return $CustomEmail;
    }

    public function delete($deleted = 1)
    {
        $CustomEmail = CustomEmail::find($this->request->id);
        $CustomEmail->deleted = $deleted;
        $CustomEmail->save();
    }

    private function triggerQueueCustom($queue_name, $client_id)
    {
        $queueCustom = new QueueCustom();
        $queueCustom->queue = $queue_name;
        $queueCustom->client_id = $client_id;
        $queueCustom->save();
    }
}
