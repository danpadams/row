<?php

namespace App\Http\Controllers\Web;

use App\Model\Domain;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class DomainController extends Controller {

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function index() {
        $Domains = Domain::with('Client')
            ->where('client_id','=', $this->request->session()->get('client.id'))
            ->get();

        return $Domains;
    }

    public function admin() {
        $Domains = Domain::with('Client')
            ->get();

        return $Domains;
    }

    public function view($dat_id) {
        $Domain = Domain::find($dat_id);
        return $Domain;
    }

    public function save() {
        // Save Data
        $CustomEmail = CustomEmail::find($this->request->id);
        $CustomEmail->setProperty('name', $this->request);
        $CustomEmail->settings = serialize($this->request->settings);
        $CustomEmail->save();
        
        // Prepare new Data
        $CustomEmail->Settings = unserialize($CustomEmail->settings);
        $CustomEmail->message = 'The email has been saved.';
        $CustomEmail->domain = Domain::find($CustomEmail->domain_id);
        return $CustomEmail;
    }

}
