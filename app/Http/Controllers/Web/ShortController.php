<?php

namespace App\Http\Controllers\Web;

use App\Model\Short;
use App\Component\BaseConvert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;

class ShortController extends Controller {

    private $request;
    private $BaseConvert;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->middleware('auth');
        $this->request = $request;
        $this->BaseConvert = new BaseConvert();
    }

    public function index($deleted = 0) {
        // Get list of All Active Shorts within Client
        Log::info(__METHOD__ . ' ' . $deleted);
        $ShortModel = new Short();
        $Shorts = $ShortModel
                ->where('client_id', '=', $this->request->session()->get('client.id'))
                ->where('deleted', '=', $deleted)
                ->get();

        $retval = [];
        foreach ($Shorts as $Short) {
            $Element = $Short->toArray();
            $Element['Code'] = $this->BaseConvert->toBase($Short->id);
            $retval[] = $Element;
        }

        return $retval;
    }

    public function view($short_id) {
        // Retreive data for an individual Short
        $ShortModel = new Short();
        $Short = $ShortModel->find($short_id);
        // Code Data for output Array
        $retval = $Short->toArray();
        $retval['Code'] = $this->BaseConvert->toBase($Short->id);

        return $retval;
    }

    public function update() {
        if ($this->request->id) {
            // Retreive data for an individual Short
            $Short = Short::find($this->request->id);
            $message = 'updated';
        } else {
            // Generate new short for database
            $Short = new Short();
            $message = 'created';
        }
        // Todo: Change to common function
        // ->setProperty('address', $this->request)
        $Short->setProperty('name', $this->request)
                ->setProperty('url', $this->request)
                ->setProperty('deleted', $this->request);
        $Short->client_id = $this->request->session()->get('client.id');
        $Short->save();

        $Short->Code = $this->BaseConvert->toBase($Short->id);
        $Short->message = 'The Short URL has been ' . $message . '.';
        return $Short;
    }

}
