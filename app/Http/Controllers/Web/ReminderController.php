<?php

namespace App\Http\Controllers\Web;

use App\Component\Internal;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ReminderController extends Controller {

    private $request;
    private $session;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function create() {
        $Internal = new Internal();
        // Set Message
        $Message = [
            'action' => 'Internal Reminder',
            'brief' => 'A Reminder has come due.',
            'body' => $this->request->message
        ];
        $from = [
            'from' => 'system@reachoutwizard.com',
            'from_name' => 'ReachoutWizard System'
        ];
        $client_id = $this->request->session()->get('client.id');
        $user_ids = [$this->request->session()->get('user.id')];

        $Internal->create($Message, $client_id, $user_ids, $this->request->processtime, $from, $this->request->person_id);

        return ['message' => 'The Reminder has been created.'];
    }

}
