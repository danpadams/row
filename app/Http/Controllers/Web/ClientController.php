<?php

namespace App\Http\Controllers\Web;

use App\Mail\NotificationMessage as NotificationMessageMail;
use App\Mail\setupAdminMessage;
use App\Model\CommonSetting;
use App\Model\Person;
use App\Model\PersonMethod;
use App\Model\UserDetail;
use Illuminate\Http\Request;
use App\Model\Client;
use App\Model\ClientFeature;
use App\Model\ClientUser;
use App\Model\Feature;
use App\Model\User;
use App\Model\Number;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ClientController extends Controller
{
    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function index()
    {
        $Clients = Client::where('closed', '=', '0')->get();
        return $Clients;
    }

    public function view($client_id)
    {
        Log::info(__METHOD__ . ':' . __LINE__ . ' Client ID: ' . $client_id);
        $Client = Client::with('ClientFeature')
            ->with('ClientFeature.Feature')
            ->find($client_id)
            ->toArray();
         $Client_A = $Client;
        $Features = [];
        foreach ($Client['client_feature'] as $client_feature) {
            $element = [
                "id" => $client_feature['id'],
                "name" => $client_feature['feature']['name'],
                "feature_id" => $client_feature['feature']['id']
            ];
            $Features[] = $element;
        }

        return ['Client' => $Client, 'Features' => $Features];
    }

    public function remove_feature()
    {
        $ClientFeature = ClientFeature::find($this->request->id);
        $ClientFeature->delete();
        return $this->view($this->request->client_id);
    }

    public function add_feature()
    {
        $client_id = $this->request->session()->get('client.id');
        $feature_id = $this->request->feature_id;
        $this->giveFeatures($client_id, [$feature_id]);
    }


    public function use_client($id, Request $request)
    {
        // 1
//        $this->Client->id = 
        $newClientId = $id;
//        if (!$this->Client->exists()) {
//            throw new NotFoundException(__('Invalid client'));
//        }
        $role = $request->session()->get('client.role');
        // If !Admin User

        if ($role == 'admin') {
            $Client = Client::where('id', '=', $id)
                ->first();
            $request->session()->put('client.id', $Client->id);
            $request->session()->put('user.role', 'admin');
            $request->session()->put('client.name', $Client->name);
            $request->session()->put('client.has_sms', $Client->has_sms);
            $request->session()->put('client.has_voice', $Client->has_voice);
        } else {
            $ClientUserModel = new ClientUser();
            $ClientUser = $ClientUserModel
                ->with('Client')
                ->where('client_id', '=', $id)
                ->where('user_id', '=', $request->session()->get('user.id'))
                ->first();
            if ($ClientUser->id) {
                $request->session()->put('client.id', $id);
                $request->session()->put('user.role', $ClientUser->role);
                $request->session()->put('client.name', $ClientUser->client->name);
                $request->session()->put('client.has_sms', $ClientUser->client->has_sms);
                $request->session()->put('client.has_voice', $ClientUser->client->has_voice);
            }
        }
        $NumberModel = new Number();
        $Numbers = $NumberModel->where('numbers.client_id', '=', $id)
            ->get();
        if (count($Numbers)) {
            $request->session()->put('client.hasNumbers', 1);
        } else {
            $request->session()->put('client.hasNumbers', 0);
        }
    }

    public function create_client()
    {
        Log::info(__METHOD__ . ':' . __LINE__ . ' ');
        $allow = false;
        if ($this->request->session()->get('user.role') == 'admin') {
            $allow = true;
        }
        return $this->create(0, $allow);
    }

    public function create($client_id = 0, $allow = false)
    {
        $client_role = ($client_id == 0) ? 'admin' : 'user';
        $create = true;
        $msg = '';
        $clear = 0;

        Log::info(__METHOD__ . ':' . __LINE__ . ' New Client ID: ' . $client_id);
        $Settings = CommonSetting::where('setting_name', '=', 'signup_allow')->first();
        Log::info(__METHOD__ . ':' . __LINE__ . ' _' . $Settings->setting_value . '_');
        if ($create && ($Settings->setting_value != '1' && empty($client_id)) && !$allow) {
            Log::info(__METHOD__ . ':' . __LINE__ . ' ');
            $msg = 'Setup Disabled by Admin';
            $clear = 1;
            $create = false;
        }
        if ($create) {
//            if (!$this->uniqueUsername($this->request->data['Client']['username'])) {
//                $clear = 0;
//                $msg = 'Error: Username/Email must be unique to system. If you already have a login, please contact the admin.';
//                $create = false;
//            }
        }
        if ($create) {
            $data = [
                'client_name' => $this->request->client_name,
                'email' => $this->request->email,
                'fname' => $this->request->fname,
                'sname' => $this->request->sname,
                'phone' => $this->request->phone
            ];
            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($data, true));

            // Create new Client
            if (empty($client_id)) {
                $Client = new Client();
                $Client->name = $this->request->client_name;
                $Client->save();
                $client_id = $Client->id;
            }
            Log::info(__METHOD__ . ':' . __LINE__ . ' New Client ID: ' . $client_id);
//            if (false) {
            // Add Default Features to new Client
            $this->giveFeatures($client_id, []);

            // Create User
            $User = new User();
            $User->name = $this->request->fname . ' ' . $this->request->sname;
            $User->email = $this->request->email;
            $User->save();
            Log::info(__METHOD__ . ':' . __LINE__ . ' New User ID: ' . $User->id);

            // Set Data for New User
            $UserDetail = new UserDetail();
            $UserDetail->username = $this->request->email;
            $UserDetail->timezone = 'America/Phoenix';
            $UserDetail->primary_client_id = $client_id;
            $UserDetail->email_address = $this->request->email;
            $UserDetail->email_primary = 1;
            $UserDetail->email_name = $this->request->fname . ' ' . $this->request->sname;
            $UserDetail->role = 'user';
            $UserDetail->password = '';
            $UserDetail->save();
            Log::info(__METHOD__ . ':' . __LINE__ . ' New UserDetail ID: ' . $UserDetail->id);

            // Create Person
            $Person = new Person();
            $Person->client_id = $client_id;
            $Person->fname = $this->request->fname;
            $Person->sname = $this->request->sname;
            Log::info(__METHOD__ . ':' . __LINE__ . ' New Person ID: ' . $Person->id);
            $Person->save();

            // Update New User
            $UserDetail->person_id = $Person->id;
            $UserDetail->save();

            // Create Contact Method - Email
            $this->create_person_method($Person->id, $this->request->email, 3);
            $this->create_person_method($Person->id, $this->request->email, 10);
            $this->create_person_method($Person->id, $this->request->phone, 2);

            // Assign User to Client just Created
            $ClientUser = new ClientUser();
            $ClientUser->client_id = $client_id;
            $ClientUser->user_id = $User->id;
            $ClientUser->role = $client_role;
            $ClientUser->save();

            // Send Message - to admin
            if ($client_role == 'admin') {
                $this->setupNotify(1, $data, $Client);
            }
            // Send Message - to New User
        }
        return ['client_id' => $client_id, 'msg' => $msg, 'clear' => $clear];
    }

    public function give_default_features()
    {
        $this->giveFeatures($this->request->session()->get('client.id'));
        return 1;
    }

    private function create_person_method($person_id, $method, $type_id)
    {
        $PersonMethod = new PersonMethod();
        $PersonMethod->person_id = $person_id;
        $PersonMethod->person_method_type_id = $type_id;
        $PersonMethod->method = $method;
        $PersonMethod->save();

        Log::info(__METHOD__ . ':' . __LINE__ . ' New PersonMethod ID: ' . $PersonMethod->id);
        return $PersonMethod->id;
    }

    private function giveFeatures($client_id, $add_features = array())
    {
        $Feature_IDs = [];
        $myFeature_ids = [];
        // Get List of Default Features
        $DefaultFeatures = Feature::where('default', '=', 1)->get()->toArray();
        $myFeatures = ClientFeature::where('client_id', '=', $client_id)->get()->toArray();
        foreach ($myFeatures as $myFeature) {
            $myFeature_ids[] = $myFeature['feature_id'];
        }
        // Extra Features
        foreach ($add_features as $add) {
            if (!in_array($add, $myFeature_ids)) {
                $Feature_IDs[] = $add;
            }
        }
        foreach ($DefaultFeatures as $defaultFeature) {
            if (!in_array($defaultFeature['id'], $myFeature_ids)) {
                $Feature_IDs[] = $defaultFeature['id'];
            }
        }
        foreach ($Feature_IDs as $feature_ID) {

            $ClientFeature = new ClientFeature();
            $ClientFeature->client_id = $client_id;
            $ClientFeature->feature_id = $feature_ID;
            $ClientFeature->save();
        }
    }

    private function setupNotify($type, $data, $Client)
    {
        switch ($type) {
            case 1:
                $Settings = CommonSetting::where('setting_name', '=', 'email_admin')->first();
                Mail::to($Settings->setting_value)
                    ->send(new setupAdminMessage($data, $Client->toArray()));
                break;
        }

        return true;
    }
}
