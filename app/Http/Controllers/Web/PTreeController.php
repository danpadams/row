<?php

namespace App\Http\Controllers\Web;

use App\Model\PTree;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;

class PTreeController extends Controller {

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function index($closed = 0) {
        $PTrees = PTree::where('client_id', '=', $this->request->session()->get('client.id'))
                ->where('closed','=',$closed)
                ->with('PKind')
                ->get();
        return $PTrees;
    }

    public function view($p_tree_id) {
        $PTree = PTree::where('id', '=', $p_tree_id)
                ->with('PBranch')
                ->first();
        return $PTree;
    }

    public function update() {
        $PTree = PTree::find($this->request->id);
        $PTree->setProperty('name', $this->request);
        $PTree->save();
        Log::Info(__METHOD__ . print_r($PTree->toArray(), true));
        return $PTree;
    }

    public function change_state() {
        // Just change the value of the closed field
        if ($this->request->id) {
            $PTree = PTree::find($this->request->id);
            if ($PTree->closed) {
                $PTree->closed = 0;
            } else {
                $PTree->closed = 1;
            }
            
            $PTree->save();
        }
        return [];
    }

}
