<?php

namespace App\Http\Controllers\Web;

use App\Component\Twilio;
use App\Model\CommonSetting;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;
use App\Component\Attachment as AttachmentComp;
use App\Model\Attachment;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class AttachmentController extends Controller
{

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function uploadFile()
    {
        $Attachment = AttachmentComp::saveFile($this->request->file, $this->request->session()->get('client.id'));

        return $Attachment;
    }

    public function getAttachmentsByLogID($log_id)
    {
        $Attachments = Attachment::where('log_id', '=', $log_id)->get()->toarray();
        $retval = [];
        foreach ($Attachments as $key => $attachment) {
            $retval[] = AttachmentComp::getDetails($attachment);
        }
        return $retval;
    }

    public function downloadByID($id)
    {
        // Todo : https://www.twilio.com/docs/sms/api/media-resource#default-content-type
        $Twilio = new Twilio();
        $Attachment = Attachment::find($id);
        $retval = $Twilio->getMedia($Attachment->path);

        return $retval;
    }
}
