<?php

namespace App\Http\Controllers;

use App\Model\Tooltip;
use Illuminate\Http\Request;

class EnumTooltipController extends Controller {

    public function __construct(Request $request) {
        // Require Logged In
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $Tooltips = Tooltip::get();
        return $Tooltips;
    }

    public function view($property_id) {
        $Tooltip = Tooltip::find($property_id);
        return $Tooltip;
    }

    public function update() {
        $Tooltip = Tooltip::find($this->request->id);
        $Tooltip->setProperty('title', $this->request)
                ->setProperty('body', $this->request)
                ->setProperty('link', $this->request);
        $Tooltip->save();
        return $Tooltip;
    }

    public function create() {
        $Tooltip = new Tooltip();
        $Tooltip->setProperty('title', $this->request)
                ->setProperty('body', $this->request)
                ->setProperty('link', $this->request);
        $Tooltip->save();
        return $Tooltip;
    }

}
