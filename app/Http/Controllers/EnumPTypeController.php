<?php

namespace App\Http\Controllers;

use App\Model\PType as myModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EnumPTypeController extends Controller
{

    public function __construct(Request $request)
    {
        // Require Logged In
        $this->middleware('auth');
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $Data = myModel::with('PKind')
            ->get();
        return $Data;
    }

    public function view($dat_id)
    {
        $Item = myModel::find($dat_id);
        return $Item;
    }

    public function update()
    {
        $Item = myModel::find($this->request->id);
        $Item->setProperty('name', $this->request)
            ->setProperty('p_kind_id', $this->request)
            ->setProperty('filename_view', $this->request)
            ->setProperty('function_twiml', $this->request)
            ->setProperty('show_next_module', $this->request)
            ->setProperty('active', $this->request);
        $Item->updated = date('Y-m-d H:i:s');
        $Item->save();
        return $Item;
    }

    public function create()
    {
        $Item = new myModel();
        $Item->setProperty('name', $this->request)
            ->setProperty('p_kind_id', $this->request)
            ->setProperty('filename_view', $this->request)
            ->setProperty('function_twiml', $this->request);
        $Item->updated = date('Y-m-d H:i:s');
        $Item->created = date('Y-m-d H:i:s');
        $Item->save();
        return $Item;
    }

    public function kinds($p_kind_id)
    {
        if ($p_kind_id > 0) {
            $PTypes = myModel::where('p_kind_id', '=', $p_kind_id)
                ->where('active', '=', 1)
                ->get();
        } else {
            $PTypes = myModel::where('active', '=', 1)
                ->get();
        }
        return $PTypes;
    }

}
