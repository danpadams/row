<?php

namespace App\Http\Controllers;

use App\Model\MetaData;
use App\Model\MDValueText;
use App\Component\Time;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MetaDataController extends Controller
{

    private $request;
    private $Time;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
//        $this->middleware('auth');
        $this->request = $request;
        $this->Time = new Time($request);
    }

    public function person($person_id)
    {
        $MetaDataModel = new MetaData();
        $MetaData = $MetaDataModel
            ->where('person_id', '=', $person_id)
            ->with('MDValueText')
            ->with('MDValueText.MetaEnumValue')
            ->with('MetaColumn')
            ->with('MetaColumn.MCType')
            ->get();
        $retval = [];
        foreach ($MetaData as $metaDatum)
        {
            $element = $metaDatum->toarray();
            $element['Updated'] = $this->Time->toLocal($element['updated_at']);
            $retval[] = $element;
        }
        return $retval;
    }

    public function column_add()
    {
        $MetaData = new MetaData();
        $MetaData->setProperty('meta_column_id', $this->request)
            ->setProperty('person_id', $this->request);
        $MetaData->save();
        $MetaData->message = 'The Additional Information has been added.';
        $MetaData->status = 'success';
        return $MetaData;
    }

    public function column_remove()
    {
        $MetaDataModel = new MetaData();
        $MetaData = $MetaDataModel->find($this->request->id);
        $MetaData->delete();
        $retval = [
            'message' => 'The Additional Information has been removed.',
            'status' => 'success'
        ];
        return $retval;
    }

    public function update()
    {
        Log::info(__METHOD__);
        $MDValueTextModel = new MDValueText();
        if ($this->request->id == 0) {
            $MDValueText = $MDValueTextModel
                ->setProperty('person_id', $this->request)
                ->setProperty('meta_data_id', $this->request);
        } else {
            $MDValueText = $MDValueTextModel->find($this->request->id);
        }
        $MDValueText = $MDValueText->setProperty('value', $this->request);
        $MDValueText->save();
//        switch ($Data['mc_type_id']) {
//            default:
//                $Data = $this->update_md_value_text($Data);
//        }
//        $Data['value_orig'] = $Data['value'];
//        $this->infoChi->outputAs('json', $Data);
    }

    private function update_md_value_text($Data)
    {
        $MDValueTextModel = ClassRegistry::init('MDValueText');
        $MDValueTextModel->save($Data);
        $Data['id'] = $MDValueTextModel->id;
        return $Data;
    }

}
