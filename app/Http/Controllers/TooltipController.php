<?php

namespace App\Http\Controllers;

use App\Model\Tooltip;
use Illuminate\Http\Request;
//use Illuminate\Support\Facades\Log;

class TooltipController extends Controller {

    private $request;


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function view($tooltip_id) {
        $TooltipModel = new Tooltip();
        $Tooltip = $TooltipModel->find($tooltip_id);
        return $Tooltip;
    }
}
