<?php

namespace App\Http\Controllers;

use App\Model\Invite;
use App\Model\Response;
use App\Model\PersonMethodType;
use App\Component\PhoneSystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class InviteController extends Controller {

    private $request;
    private $phoneSystem;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->middleware('auth');
        $this->request = $request;
        $this->phoneSystem = new phoneSystem();
    }

    public function responses() {
        $data = Response::get();
        $retval = [];
        foreach ($data as $dat) {
            $retval[] = $dat;
        }
        return $retval;
    }

    public function invites($event_id) {
        // For Lookup
        $PersonMethodTypeModel = new PersonMethodType();

        // Fetch Data Needed
        $People = Invite::where('event_id', '=', $event_id)
                ->with('person')
                ->with('person.PersonMethod')
                ->with('response')
                ->get();

        $retval = [];
        foreach ($People as $Person) {
            $Element = $Person->person->toArray();
            $Element['invite_id'] = $Person->id;
            $Element['response_id'] = $Person->response->id;
            $Element['response_name'] = $Person->response->name;
            if ($Element['deleted'] == 1) {
                continue;
            }

            // Format Person Methods
            for ($i = 0; $i < count($Element['person_method']); $i++) {
                $Method = $Element['person_method'][$i];
                $Method['person_method_formatted'] = $this->phoneSystem->formatPhone($Method['method']);
                $Method['person_method_type_name'] = $PersonMethodTypeModel->lookup($Method['person_method_type_id']);
                $Method['label'] = $Method ['person_method_type_name'] . ': ' . $Method ['person_method_formatted'];
                $Element['PersonMethod'][] = $Method;
            }
            unset($Element['person_method']);
            $retval[] = $Element;
        }

        return $retval;
    }

    public function delete() {
        // Delete an item permanently
        Invite::where('id', '=', $this->request->id)->delete();
    }

    public function update() {
        // Update the basic information for the invite
        $Invite = Invite::where('id', '=', $this->request->id)->first();
        $Invite->setProperty('response_id', $this->request);
        $Invite->save();
    }

    public function create() {
        $Invite = new Invite();
        $Invite->setProperty('response_id', $this->request)
                ->setProperty('person_id', $this->request)
                ->setProperty('event_id', $this->request);

        $Invite->save();
    }

}
