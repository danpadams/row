<?php

namespace App\Http\Controllers\Flow;

use App\Component\PhoneSystem;
use App\Model\LogNote;
use App\Model\Log as LogModel;
use App\Model\PersonMethod;
use App\Model\Number;
use App\Model\PTree;
use App\Model\PBranch;
use App\Component\Twilio;
use App\Model\QueueMessage;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Component\Attachment;

// Test

class FlowController extends Controller
{

    /**
     * @var Twilio
     */
    private $Twilio;
    private $request;

    /**
     * @var PhoneSystem
     */
    private $PhoneSystem;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->Twilio = new Twilio();
        $this->PhoneSystem = new PhoneSystem();
    }

    public function start()
    {
        $post_data = $this->request->all();
        $Number = Number::getFromNumber($post_data['To']);

        $Log = new LogModel();
        $Log->client_id = $Number->client_id;
        $Log->action = 'Incoming Message';
        $Log->version = 2;
        $Log->created = date('Y-m-d H:i:s');
        $Log->save();
        Log::info(__METHOD__ . " Log_id: " . $Log->id);
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($post_data, true));

        $this->recordLogNote($Log->id, $Number->client_id);

        // Receive Phone Call, SMS Message
        if ($this->request->data ['SmsSid']) {
            $p_tree_id = $Number->p_tree_voice;
        } else {
            $p_tree_id = $Number->p_tree_sms;
        }
        $PTree = PTree::where('id', '=', $p_tree_id)->first();
        $pbranch_id = $PTree->p_branch_id;

        // Compute twiml redirect
        // Todo: Double Check to see that Branch Exists before sending Flow to Branch
        $twiml = $this->Twilio->redirect(url("flow/run/" . $pbranch_id . "/" . $Log->id));
        return $this->Twilio->wrapTwiml($twiml);
    }

    public function run($p_branch_id, $log_id)
    {
        // Public access for Twilio segment - restricted by type defined in p_branch
        $PBranch = PBranch::with('Type')
            ->where('id', '=', $p_branch_id)->first();
        $functionName = 'twiml' . $PBranch->Type->function_twiml;

        /**
         * This is done because the function name is dynamically returned from the DB based on the type of branch
         * @uses FlowController::twimlSmsInbox()
         * @uses FlowController::twimlSmsSend()
         * @uses FlowController::twimlSmsReply()
         * @uses FlowController::twimlVoiceGreeting()
         * @uses FlowController::twimlVoiceInbox()
         */
        list($twiml, $next_id) = $this->$functionName($PBranch->toArray(), $log_id);
        if (empty($next_id)) {
            $next_id = $PBranch['next_id'];
        }

        // If (next != 0) add redirect statement
        $twiml .= $this->twimlNext($next_id, $log_id);
        return $this->Twilio->wrapTwiml($twiml);
    }

    /**
     * Save the data for this LogNote
     *
     * @param int $log_id
     * @param int $client_id
     * @return int $log_note_id
     */
    public function recordLogNote($log_id, $client_id)
    {
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r('', true));
        $post_data = $this->request->all();
        $Person = PersonMethod::getPersonsFromMethod($post_data['From'], $client_id, true);

        $LogNote = new LogNote();
        $LogNote->log_id = $log_id;
        $LogNote->inbound = 1;
        $LogNote->from_addy = $post_data['From'];
        if (isset($Person['person']['id'])) {
            $LogNote->person_id = $Person['person']['id'];
            $LogNote->from_name = $Person['person']['fname'] . ' ' . $Person['person']['sname'];
        }
        $LogNote->dest_addy = $post_data['To'];
        $LogNote->assignFromArray('Body', $post_data, 'body');

        $LogNote->data = json_encode($post_data);
        $LogNote->version = 2;
        if (!empty($post_data['MessageSid'])) {
            $LogNote->twilio_sid = $post_data['MessageSid'];
        }
        $LogNote->save();
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r('', true));
        return $LogNote->id;
    }

    /**
     * Generate twiml for next section of processing.
     *
     * @param $next_id
     * @param $log_id
     * @return string twiml code
     */
    private function twimlNext($next_id, $log_id)
    {
        $twiml = '';

        if ($next_id) {
            $twiml = $this->Twilio->redirect(url("flow/run/" . $next_id . "/" . $log_id));
        }
        return $twiml;
    }

    private function twimlSmsInbox($PBranch, $log_id)
    {
        // Segment to record a message for the SMS
        $settings = unserialize($PBranch['settings']);

        // Save the Message
        $msg_id = $this->PhoneSystem->saveMessage($this->request->all(), $log_id);
        // Check for Attachment if Feature is Held by Client
        foreach ($this->request->toArray() as $key => $datum) {
            if (strpos($key, 'MediaUrl') !== false) {
                Attachment::saveFromUrl($datum, $PBranch['client_id'], $log_id);
            }
        }

        $this->PhoneSystem->deliverMessage($settings, $msg_id);
        return ['', 0];
    }

    /**
     * @param $PBranch
     * @param $log_id
     * @return array [twiml code, next_id]
     */
    private function twimlSmsSend($PBranch, $log_id)
    {
        return ['', 0];
    }

    /**
     * @param $PBranch
     * @param $log_id
     * @return array [twiml code, next_id]
     */
    private function twimlSmsReply($PBranch, $log_id)
    {
        $settings = unserialize($PBranch['settings']);
        $post_data = $this->request->all();

        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r('', true));

        // Get Person Data
        $PersonMethod = PersonMethod::getPersonsFromMethod($post_data['From'], $PBranch['client_id'], true);

        // Todo: Make sure this is a contact first.
        // Insert Method
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r('', true));
        $QueueMessage = new QueueMessage();
        $QueueMessage->log_id = $log_id;
        $QueueMessage->client_id = $PBranch['client_id'];
        $QueueMessage->user_id = 0;
        $QueueMessage->short_id = 0;
        $QueueMessage->processtime = date('Y-m-d H:i:s');
        $QueueMessage->processed = 0;
        $QueueMessage->subject = "SMS Reply";
        $QueueMessage->body = $settings['body'];
        $QueueMessage->created = date('Y-m-d H:i:s');
        $QueueMessage->updated = date('Y-m-d H:i:s');
        $QueueMessage->attachment_ids = json_encode([]);

        // Reverse the "from" and "to" with the message that triggered this one.
        $QueueMessage->from = $post_data['To'];
        if (empty($PersonMethod)) {
            list($QueueMessage->person_method_id, $QueueMessage->person_id) = PersonMethod::createPersonFromMethod($post_data['From'], $PBranch['client_id']);
        } else {
            $QueueMessage->person_id = $PersonMethod['person']['id'];
            $QueueMessage->person_method_id = $PersonMethod['id'];
        }
        $QueueMessage->to = $post_data['From'];
        $QueueMessage->m_type_id = 1;
        $QueueMessage->save();

        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r('', true));
        return ['', 0];
    }

    /**
     * Segment to Play a Greeting as the Module
     *
     * @param $PBranch
     * @param $log_id
     * @return array [twiml code, next_id]
     */
    public function twimlVoiceGreeting($PBranch, $log_id)
    {
        $Settings = unserialize($PBranch['settings']);
        $twiml = $this->PhoneSystem->twimlGreet($Settings);
        return [$twiml, 0];
    }

    /**
     * @param $PBranch
     * @param $log_id
     * @return array [twiml code, next_id]
     */
    private function twimlVoiceInbox($PBranch, $log_id)
    {
        return ['', 0];
    }

}
