<?php

namespace App\Http\Controllers;

use App\Model\Number;
use App\Component\PhoneSystem;
use App\Model\PTree;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class NumberController extends Controller {

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function index() {
        $retval = [];
        $Numbers = Number::where('closed','=',0)
            ->where('client_id', '=', $this->request->session()->get('client.id'))
                ->get();
        $PhoneSystem = new PhoneSystem();
        foreach ($Numbers as $Number) {
            $element = $Number->toArray();
            $element['forNumber'] = $PhoneSystem->formatPhone($element['number']);
            $retval[] = $element;
        }
        return $retval;
    }

    public function p_tree($p_kind_id = 1) {
        $pTrees = PTree::where('client_id', '=', $this->request->session()->get('client.id'))
                ->where('closed', '=', 0)
                ->where('p_kind_id', '=', $p_kind_id)
                ->get();
        return $pTrees;
    }
    
    public function update_p_tree()
    {
        $column = 'p_tree_' . $this->request->column;
        $number = Number::find($this->request->number_id);
        $number->$column = $this->request->pTree;
        $number->save();
    }

}
