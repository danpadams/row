<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;
use App\Model\UserDetail;
use App\Model\User;
use App\Model\ClientUser;
use App\Model\Number;
use App\Model\Client;
use App\Component\Login as LogLogin;

class LoginController extends Controller
{
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

    use AuthenticatesUsers;

    protected $ClientUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Set the username filed to the column 'username' rather than the default of 'email'
     */
//    public function username() {
//        return 'username';
//    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
//        $this->session = $request->session;
    }

    public function authenticated(Request $request)
    {
        // This function runs once upon login
        // Get User Info from DB, process then write to Session
        $User = UserDetail::where('username', '=', Auth::user()->email)->first();

        Session::put('user.username', $User->username);
        Session::put('user.name', $User->email_name);
        Session::put('user.email', $User->email_address);
        Session::put('user.email_primary', $User->email_primary);
        Session::put('user.id', $User->id);
        Session::put('user.role', $User->role);

        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($User->toArray(), true));
        $UserLogin = User::where('email', '=', Auth::user()->email)->first();
        $ClientUserModel = new ClientUser();
        $ClientUsers = $ClientUserModel
            ->where('user_id', '=', $UserLogin->id)
            ->get();
        if (count($ClientUsers)) {
            $CU = [];
            $CRoles = [];
            foreach ($ClientUsers as $ClientUserCheck) {
                $CU[] = $ClientUserCheck->client_id;
                $CRoles[$ClientUserCheck->client_id] = $ClientUserCheck->role;
            }
            if (in_array($User->primary_client_id, $CU)) {
                $client_id = $User->primary_client_id;
            } else {
                $client_id = $CU[0];
            }

            // Set Client
            // $this->setClient($client_id);
            $ClientModel = new Client();
            $Client = $ClientModel->where('clients.id', '=', $client_id)
                ->first();
            Session::put('client.name', $Client->name);
            Session::put('client.has_sms', $Client->has_sms);
            Session::put('client.has_voice', $Client->has_voice);
            $NumberModel = new Number();
            $Numbers = $NumberModel->where('numbers.client_id', '=', $client_id)
                ->get();
            if (count($Numbers)) {
                Session::put('client.hasNumbers', 1);
            } else {
                Session::put('client.hasNumbers', 0);
            }
            Session::put('client.id', $client_id);
            // Set Client
            Session::put('user.role', $User->role);
            Session::put('user.id', $User->id);
            Session::put('user.timezone', $User->timezone);
            Session::put('user.timezone', 'America/Phoenix');
            Session::put('user.person_id', $User->person_id);
            // ----
            $role = $User->role;

            // Check Role For Client
            if ($role == 'admin') {
                Session::put('client.role', 'admin');
            } else {
                Session::put('client.role', $CRoles[$client_id]);
            }
            $Login = new LogLogin($request);
            $Login->log();

            if (!empty($request->urlHash)) {
                Log::info(__METHOD__ . ' /' . $request->urlHash);
                return redirect('/' . $request->urlHash);
            } else {
                return redirect('/#/home');
            }
        } else {
            // Valid Login - No Clients Found - Force Logout with Message
            Log::info(__METHOD__ . ' | No Clients Found');
        }
        return redirect('/logout');
    }

}
