<?php

namespace App\Http\Controllers;

use App\Model\MetaColumn;
use App\Model\MetaEnumValue;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MetaEnumController extends Controller {

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
    }

    /**
     * @param $mc_type_id
     * @return mixed
     */
    public function options($mc_type_id) {
        $MetaEnumValueModel = new MetaEnumValue();
        $MetaEnumValues = $MetaEnumValueModel
            ->where('mc_type_id','=',$mc_type_id)
            ->orderBy('name')
            ->get();
        return $MetaEnumValues;
    }

    /**
     * @param $mc_type_id
     * @return mixed
     */
    public function active($mc_type_id) {
//        $MetaEnumValueModel = new MetaEnumValue();
//        $MetaEnumValues = $MetaEnumValueModel
        $MetaEnumValues = (new MetaEnumValue())
            ->where('mc_type_id','=',$mc_type_id)
            ->where('active','=','1')
            ->orderBy('name')
            ->get();
//$MetaEnumValues = ['data'=>'Test'];
        return $MetaEnumValues;
    }

    /**
     * @param $mc_type_id
     * @return mixed
     */
    public function boolean_type($mc_type_id = 1) {
        $Columns = MetaColumn::where('client_id','=',$this->request->session()->get('client.id'))
            ->where('mc_type_id','=',$mc_type_id)
            ->get();
            return $Columns;
    }
}
