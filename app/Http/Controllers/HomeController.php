<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\ClientUser;
use App\Model\ClientFeature;
use App\Model\ClientUserFeature;
use App\Component\Feature;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function features(Request $request)
    {
        // Feature Buttons for home page
        $Features = $this->feature_list();
        $retval = [];

        // Form Array for Home Screen
        foreach ($Features as $Feature) {
            $Data = $Feature->toArray();
            if (empty($Data['feature']['url_string'])) {
                continue;
            }
            $element = [
                "link" => $Data['feature']['url_string'],
                "label" => $Data['feature']['name'],
                "fa_icon" => $Data['feature']['icon_string'],
                "sort" => $Data['feature']['sort']
            ];
            // Use v5 for icon if available
            if ($Data['feature']['icon_v5']) {
                $element['fa_icon'] = $Data['feature']['icon_v5'];
            }
            $retval[] = $element;
        }
        return $retval;
    }

    public function get_features()
    {
        $Features = $this->feature_list();
        foreach ($Features as $feature) {
            $retval[] = $feature->feature->id;
        }
        return $retval;
    }

    /**
     * Todo: Desire is to call with needed parameters Feature::list()
     * @return mixed
     */
    private function feature_list()
    {
        return Feature::list([
            'user_role' => $this->request->session()->get('user.role'),
            'user_id' => $this->request->session()->get('user.id'),
            'client_role' => $this->request->session()->get('client.role'),
            'client_id' => $this->request->session()->get('client.id')
        ]);
    }

    public function admin(Request $request)
    {
        // Admin buttons for home page 
        // fa_icon info inplace for web app
        $Data = [];
        if ($request->session()->get('user.role') == 'admin') {
            $Data [] = [
                "link" => "clients/edit",
                "label" => "Manage Client: " . $request->session()->get('client.name'),
                'fa_icon' => 'fa-object-ungroup'
            ];
            $Data [] = [
                'link' => 'clients',
                'label' => 'All Clients',
                'fa_icon' => 'fa-object-group'
            ];
            $Data [] = [
                "link" => "users/client_id/0",
                "label" => "All Users",
                'fa_icon' => 'fa-users'
            ];
            $Data[] = [
                "link" => "log_logins",
                'label' => 'Log: Logins',
                'fa_icon' => 'fa-history'
            ];
            $Data[] = [
                "link" => "domains",
                'label' => 'Domains',
                'fa_icon' => 'fa-at'
            ];
            $Data [] = [
                "link" => 'p_types',
                'label' => 'Enumeration: Process Types',
                'fa_icon' => 'fa-database'
            ];
//            $Data [] = [
//                "link" => 'p_kinds',
//                'label' => 'Enumeration: Process Kinds',
//                'fa_icon' => 'fa-database'
//            ];
            $Data [] = [
                "link" => 'tooltips',
                'label' => 'Enumeration: Tooltips',
                'fa_icon' => 'fa-database'
            ];
            $Data[] = [
                "link" => 'm_c_types',
                'label' => 'Enumberation: Meta Column Types',
                'fa_icon' => 'fa-database'
            ];
        }
        return $Data;
    }

    public function clients(Request $request)
    {
        $retval = [];

        // Generate List of Clients of current user for messages list
        $ClientUserModel = new ClientUser();
        $ClientUsers = $ClientUserModel
            ->with('Client')
            ->where('user_id', '=', $request->session()->get('user.id'))
            ->get();

        // Format array as List for JSON
        foreach ($ClientUsers as $ClientUser) {
            $Data = $ClientUser->toArray();
            $retval[] = [
                "id" => $Data['client']['id'],
                "label" => $Data['client']['name'],
                "fa_icon" => "fa-arrows-alt"
            ];
        }

        return $retval;
    }

    public function list_clients()
    {
        $retval = [];

        // Generate List of Clients of current user for messages list
        $ClientUserModel = new ClientUser();
        $ClientUsers = $ClientUserModel
            ->with('Client')
            ->where('user_id', '=', $this->request->session()->get('user.id'))
            ->get();

        foreach ($ClientUsers as $CU) {
            $retval[] = [
                "id" => $CU->client->id,
                "name" => $CU->client->name
            ];
        }
        return $retval;
    }

    public function current(Request $request)
    {
        $retval['Client'] = $request->session()->get('client');
        $retval['User'] = $request->session()->get('user');
        return $retval;
    }

}
