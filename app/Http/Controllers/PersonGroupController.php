<?php

namespace App\Http\Controllers;

use App\Model\Person;
use App\Model\PersonGroup;
use App\Model\PersonGroupData;
use App\Component\PhoneSystem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class PersonGroupController extends Controller {

    private $request;
    private $PhoneSystem;
    private $PersonMethodType;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->middleware('auth');
        $this->request = $request;
        $this->PhoneSystem = new PhoneSystem();
        $this->PersonMethodType = new \App\Model\PersonMethodType();
    }

    public function index() {
        $Groups = PersonGroup::where('client_id', '=', $this->request->session()->get('client.id'))
                ->get();
        return $Groups;
    }

    public function detail($person_group_id) {
        $Detail = PersonGroupData::where('person_group_id', '=', $person_group_id)
                ->with('Person')
                ->with('Person.PersonMethod')
                ->get();
        $people = [];
        /**
         * @var PersonGroupData 
         */
        foreach ($Detail as $Det) {
            $Person = $Det->toArray();
            $Person = $Person['person'];
            $Person['pgd_id'] = $Det->id;
            // Foreach is needed as keys are non-numeric
            for ($i = 0; $i < count($Person['person_method']); $i++) {
                $Method = $Person['person_method'][$i];

//            foreach ($Person['person_method'] as $key => $Method)

                $Method['person_method_formatted'] = $this->PhoneSystem->formatPhone($Method['method']);
                $Method['person_method_type_name'] = $this->PersonMethodType->lookup($Method['person_method_type_id']);
                $Method['label'] = $Method ['person_method_type_name'] . ': ' . $Method ['person_method_formatted'];

                $Person['person_method'][$i] = $Method;
            }

            $people [] = $Person;
        }

        return $people;
    }

    public function view($person_group_id) {
        $Groups = PersonGroup::find($person_group_id);
        return $Groups;
    }

    public function add_list($person_group_id) {
//        $personIds = [];
//        $PersonGroupDataModel = ClassRegistry::init('PersonGroupData');
//        $PersonGroupData = $PersonGroupDataModel->find('all', array(
//            'conditions' => array(
//                'PersonGroupData.person_group_id' => $person_group_id,
//                'Person.client_id' => $this->Session->read('Client.id'),
//                'Person.deleted' => 0
//            ),
//            'fields' => array(
//                'PersonGroupData.*'
//            )
//        ));
//        
//        for ($i = 0; $i < count($PersonGroupData); $i++) {
//            $personIds [] = $PersonGroupData[$i]['PersonGroupData']['person_id'];
//        }

        $People = Person::where('client_id', '=', $this->request->session()->get('client.id'))
                ->where('deleted', '=', 0)
                ->get();

        $Data = array();
        foreach ($People as $Person) {
            $Element['person_id'] = $Person['id'];
            $Element['sname'] = $Person['sname'];
            $Element['fname'] = $Person['fname'];
            $Element['label'] = $Person['sname'] . ", " . $Person['fname'];
            $Data [] = $Element;
        }
        return $Data;
    }

    public function add_to_group() {
        $PersonGroupData = new PersonGroupData();
        $PersonGroupData->setProperty('person_id', $this->request)
                ->setProperty('person_group_id', $this->request);
        $PersonGroupData->save();
    }

    public function remove_from_group() {
        PersonGroupData::find($this->request->pgd_id)->delete();
    }

    public function person($person_id) {
        // Get List of Groups person is in. /people/view
        $PersonGroupData = PersonGroupData::where('person_id', '=', $person_id)
                        ->with('PersonGroup')->get();

        $retval = [];
        foreach ($PersonGroupData as $PersonGroup) {
            if ($PersonGroup->PersonGroup) {
                $retval[] = [
                    'id' => $PersonGroup->id,
                    'group_id' => $PersonGroup->PersonGroup->id,
                    'group_name' => $PersonGroup->PersonGroup->name
                ];
            }
        }
        return $retval;
    }

    public function create() {
        $PersonGroup = new PersonGroup();
        $PersonGroup->setProperty('name', $this->request);
        $PersonGroup->client_id = $this->request->session()->get('client.id');
        $PersonGroup->save();
        return $PersonGroup;
    }

    public function delete() {
        // Delete an item permanently
        PersonGroup::find($this->request->id)->delete();
        // On cascade delete
        PersonGroupData::where('person_group_id', '=', $this->request->id)->delete();
    }

}
