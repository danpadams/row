<?php

namespace App\Http\Controllers;

use App\Model\Attachment;
use App\Model\Message;
use App\Model\Log as LogModel;
use App\Model\LogNote;
use App\Component\PhoneSystem;
use App\Component\Time;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use App\Component\Attachment as AttachmentComp;

class LogController extends Controller
{

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->PhoneSystem = new PhoneSystem();
        $this->Time = new Time($request);
    }

    public function update($log_id)
    {
        if ($log_id != $this->request->id) {
            return;
        }
        $Log = LogModel::find($log_id);
        $Log->setProperty('brief', $this->request);
        $Log->save();
        return $Log;
    }

    public function index()
    {
        $Data = LogModel::where('client_id', '=', $this->request->session()->get('client.id'))
//                ->with('LogNote')
            ->with('LogNote.Person')
            ->where('hide', '=', 0)
            ->orderBy('created_at', 'DESC')
//            ->limit(5) // Temporary
            ->get();
        $retval = [];
        foreach ($Data as $Dat) {
            $element = $Dat->toArray();
            if (empty($element['log_note'])) {
                // Ignore if there are no log_notes
                // This is possible if an outbound message is recorded but not sent yet.
                continue;
            }
            $element['people'] = [];
            foreach ($Dat->LogNote->toArray() as $LogNote) {
                $find = array_search($LogNote['person']['id'], array_column($element['people'], 'id'));
                // https://stackoverflow.com/questions/6661530/php-multidimensional-array-search-by-value
                // array_search('100', array_column($userdb, 'uid'));

                if ($find === false) {
                    $Person = [
                        'id' => $LogNote['person']['id'],
                        'fname' => $LogNote['person']['fname'],
                        'sname' => $LogNote['person']['sname']
                    ];
                    $Person['label'] = $Person['sname'] . ', ' . $Person['fname'];
                    $element['people'][] = $Person;
                }
            }

            unset($element['log_note']);
            $retval[] = $element;
        }
        return $retval;
    }

    public function view($log_id)
    {
        $Data = LogModel::find($log_id);
        return $Data;
    }

    public function detail($log_id)
    {
        $Data = LogNote::where('log_id', '=', $log_id)
            ->with('Person')
            ->get()
            ->toarray();

        foreach ($Data as $key => $datum) {
            $Data[$key]['dest_formatted'] = $this->PhoneSystem->formatPhone($datum['dest_addy']);
            $Data[$key]['from_formatted'] = $this->PhoneSystem->formatPhone($datum['from_addy']);
            $Data[$key]['start_time'] = $this->Time->toLocal($datum['created']);
        }

        return $Data;
    }

    public function get_msg_id($log_id)
    {
        $retval = 0;
        $Message = Message::where('log_id', $log_id)->with('UserMessage')->first();
        if (!empty($Message->id)) {
            foreach ($Message->UserMessage as $UserMessage) {
                if ($UserMessage->user_id == $this->request->session()->get('user.id')) {
                    $retval = $UserMessage->message_id;
                    break;
                }
            }
        }
        return $retval;
    }

    public function attachments($log_id)
    {
        $Attachments = Attachment::where('log_id', '=', $log_id)->get();
        $retval = [];
        foreach ($Attachments as $attachment) {
            $retval[] = AttachmentComp::getDetails($attachment);
        }
        return $retval;
    }

    public function person($person_id)
    {
        $retval = [];
        $Data = LogNote::with('Attachment')
            ->where('person_id', '=', $person_id)
            ->orderBy('id', 'desc')
            ->limit(10)
            ->get();
        $Datas = $Data->toArray();

        // Calculated Values for each Lognote
        foreach ($Datas as $Data) {
            $element = $Data;
            $element['dest_formatted'] = $this->PhoneSystem->formatPhone($Data['dest_addy']);
            $element['from_formatted'] = $this->PhoneSystem->formatPhone($Data['from_addy']);
            $element['start_time'] = $this->Time->toLocal($Data['created']);
            if (!empty($Data['attachment'])) {
//                Log::info(__METHOD__ . ':' . __LINE__ . ' Attachments Found')
                $element['attachment'] = [];
                foreach ($Data['attachment'] as $attachment) {
                    $element['attachment'][] = AttachmentComp::getDetails($attachment);
                }
                // Todo Check for IsImage
            } else {
//                Log::info(__METHOD__ . ':' . __LINE__ . ' Empty Attachments');
                unset($element['attachment']);
            }
            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($retval, true));
            $retval[] = $element;
        }
//        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($retval, true));
        return $retval;
    }
}
