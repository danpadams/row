<?php

namespace App\Http\Controllers;

use App\Model\Event;
use App\Model\EventAttendance;
use App\Model\Response;
use App\Model\Person;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EventController extends Controller {

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function index() {
        $Events = Event::where('client_id', '=', $this->request->session()->get('client.id'))
                ->with('EventAttendance')
                ->with('Invite')
                ->get();
        return $Events;
    }

    public function view($event_id) {
        $Event = Event::find($event_id);

        return $Event;
    }

    public function people_list($val_num) {
        if ($val_num) {
            // Use Attendance Entry (Present, Absent, Excused)
            $AttendanceHistory = EventAttendance::where('event_id', '=', $val_num)
                    ->get();
            $Person_Ids = [];
            foreach ($AttendanceHistory as $Attendance) {
                $Person_Ids[] = $Attendance->person_id;
            }
        }

        $People = Person::where('client_id', '=', $this->request->session()->get('client.id'))
                ->where('deleted', '=', 0)
                ->get();

        $Data = [];
        foreach ($People as $Person) {
            if (isset($Person_Ids)) {
                // Ensure that the current person is in the Person_Ids list
                if (!in_array($Person['id'], $Person_Ids)) {
                    continue;
                }
            }
            $Element['person_id'] = $Person['id'];
            $Element['sname'] = $Person['sname'];
            $Element['fname'] = $Person['fname'];
            $Element['label'] = $Person['sname'] . ", " . $Person['fname'];
            $Data [] = $Element;
        }
        return $Data;
    }

    public function groups() {
        $Events = Event::where('client_id', '=', $this->request->session()->get('client.id'))
                ->with('EventAttendance')
                ->orderBy('startdate')
                ->get();

        $retval = [];
        foreach ($Events as $Event) {
            if (!$Event->EventAttendance->count()) {
                continue;
            }
            $retval[] = [
                "id" => $Event->id,
                "label" => $Event->name . ' (' . $Event->startdate . ')'
            ];
        }
        return $retval;
    }

    public function update() {
        // Update the basic information for the invite
        Log::info(__METHOD__ . print_r($this->request->toArray(), true));
        if ($this->request->id) {
            $Event = Event::where('id', '=', $this->request->id)->first();
        } else {
            $Event = new Event();
        }
        $Event->client_id = $this->request->session()->get('client.id');

        $Event->setProperty('name', $this->request)
                ->setProperty('msg_general', $this->request)
                ->setProperty('startdate', $this->request)
                ->setProperty('startdate', $this->request);
        $Event->save();
        return $Event;
    }

    public function delete() {
        Log::info(__METHOD__ . print_r($this->request->toArray(), true));
        Event::where('id', '=', $this->request->id)->delete();
    }
}
