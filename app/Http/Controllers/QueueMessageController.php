<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Log as LogModel;
use App\Model\LogNote;
use App\Model\QueueMessage;
use App\Model\Short;
use App\Model\Message;
use App\Model\UserMessage;
use App\Model\UserDetail as User;
use App\Model\Attachment;
use App\Component\infoChi;
use App\Component\Twilio;
use App\Component\BaseConvert;
use App\Component\PhoneSystem;
use App\Component\Internal;
use App\Component\Time;
use App\Mail\QueueMessage as QueueMessageMail;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Log;

class QueueMessageController extends Controller
{

    private $request;
    private $BaseConvert;
    private $infoChi;
    private $PhoneSystem;
    private $Time;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
        $this->BaseConvert = new BaseConvert();
        $this->infoChi = new infoChi();
        $this->PhoneSystem = new PhoneSystem();
        $this->Time = new Time($request);
    }

    public function pending()
    {
        $QueueMessages = (new QueueMessage())
            ->with('Person')
            ->where('client_id', '=', $this->request->session()->get('client.id'))
            ->where('processed', '=', 0)
            ->get();
        $retval = [];
        foreach ($QueueMessages as $queueMessage) {
            $element = $queueMessage->toarray();
            $element['DeliveryTime'] = $this->Time->toLocal($element['processtime']);
            $retval[] = $element;
        }
        return $retval;
    }

    public function create(Request $request)
    {
        Log::info(__METHOD__ . print_r($request->toArray(), true));

        // Setup Log to be used.
        $LogModel = new LogModel();
        $LogModel->client_id = $request->session()->get('client.id');
        $LogModel->action = 'Outbound Message';
        $LogModel->brief = "$request->subject";
        $LogModel->version = 2;
        $LogModel->created = date('Y-m-d H:i:s');
        $LogModel->save();
        Log::info(__METHOD__ . ' ' . $LogModel->id);

        $attachment_ids = [];
        foreach ($request->fileData as $attachment) {
            $attachment_ids[] = $attachment['id'];
            $FileRecord = Attachment::find($attachment['id']);
            $FileRecord->log_id = $LogModel->id;
            $FileRecord->save();
        }


        // Get Processtime
        $pt_ = strtotime($request->dateDropDownInput);
        if ($pt_) {
            $pt = date('Y-m-d H:i:s', $pt_);
        } else {
            $pt = date('Y-m-d H:i:s');
        }

        // Get Short_ID
        $short_id_ = $request->short_id;

        if ($short_id_) {
            $short_id = $short_id_;
        } else {
            $short_id = 0;
        }

        // if (@$this->request->data ['event_id'] && $this->request->data ['mark_as']) {
        //    $InviteModel = ClassRegistry::init('Invite');
        //    $InviteEvalCheckModel = ClassRegistry::init('InviteEvalCheck');
        // }
        // Set Data for All Messages to send
        $recips = $request->recipients;

        // Loop Recipients
        for ($i = 0; $i < count($recips); $i++) {
            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($recips, true));
            $QueueMessageModel = new QueueMessage();
            $QueueMessageModel->log_id = $LogModel->id;
            $QueueMessageModel->client_id = $request->session()->get('client.id');
            $QueueMessageModel->user_id = $request->session()->get('user.id');
            $QueueMessageModel->short_id = $short_id;
            $QueueMessageModel->processtime = $pt;
            $QueueMessageModel->processed = 0; // Testing Purposes - DO NOT PROCESS
            $QueueMessageModel->subject = "$request->subject";
            $QueueMessageModel->body = "$request->body";
            $QueueMessageModel->created = date('Y-m-d H:i:s');
            $QueueMessageModel->updated = date('Y-m-d H:i:s');
            $QueueMessageModel->from_name = "$request->from_name";
            $QueueMessageModel->attachment_ids = json_encode($attachment_ids);

            // $IEC = array();
            // $useIEC = true;  
            $QueueMessageModel->person_id = $recips[$i]['person_id'];
            $QueueMessageModel->person_method_id = $recips[$i]['method_id'];

            // Check Preferred Method
            if ($recips[$i]['type_id'] == 10) {
                if (strpos($recips[$i]['method'], '@') !== FALSE) {
                    // Set as Email
                    $recips[$i]['type_id'] = 3;
                } else {
                    // Set as SMS
                    $recips[$i]['type_id'] = 1;
                }
            }

            // Insert Method
            switch ($recips[$i]['type_id']) {
                case 1: // SMS
                    $QueueMessageModel->from = $request->from_phone;
                    $QueueMessageModel->to = $recips[$i]['method'];
                    $QueueMessageModel->m_type_id = 1;
                    break;
                case 3: // Email
                    $QueueMessageModel->from = $request->from_email;
                    $QueueMessageModel->to = $recips[$i]['method'];
                    $QueueMessageModel->m_type_id = 3;
                    break;
            }
            $QueueMessageModel->save();

            // if (@$this->request->data ['event_id'] && $this->request->data ['mark_as']) {
            // $InviteModel->updateAll(array(
//                        'Invite.response_id' => $this->request->data ['mark_as']
//                            ), array(
//                        'Invite.event_id' => $this->request->data ['event_id'],
//                        'Invite.person_id' => $Recipient ['person_id']
//                    ));
//                    // Add to InviteEvalChecks
//                    $IEC ['InviteEvalCheck'] ['event_id'] = $this->request->data ['event_id'];
//                    if ($useIEC) {
//                        $InviteEvalCheckModel->save($IEC);
//                    }
//                }
        }
    }

    /**
     * CronJob Function
     */
    public function run()
    {
        echo "Start - Send from Queue\n";
        $QueueMessageModel = new QueueMessage();

        $QueueMessages = $QueueMessageModel
            ->where('processtime', '<', date('Y-m-d H:i:s'))
            ->where('processed', '=', 0)
            ->with('person')
            ->with('client')
            ->with('Internal')
            ->with('Short')
            ->get();

        $Twilio = new Twilio();
        foreach ($QueueMessages as $QueueMessage) {
            echo "\t QueueMessage: {$QueueMessage->id}\n";
            $Processed = 0;
            $ErrorMessage = '';
            // Replace Variables
            $body = $this->infoChi->replaceVariables($QueueMessage->body, $QueueMessage->person_id);
            // Check Short URL
            if ($QueueMessage->short_id && $QueueMessage->short_id != 0) {
                // Code Data for output Array
                $code = $this->BaseConvert->toBase($QueueMessage->short_id);
                $link = 'http://' . config('row.domain.Short') . '/s/' . $code;
            } else {
                $link = '';
            }
            $Attachments = [];
            if ($QueueMessage->attachment_ids) {
                $Attachment_ids = json_decode($QueueMessage->attachment_ids);

                foreach ($Attachment_ids as $aid) {
                    $Attachments[] = Attachment::find($aid)->toarray();
                }
            }

//            Log::info(__METHOD__ . ':' . __LINE__ . print_r($QueueMessage->toArray(), true));
            try {
                switch ($QueueMessage->m_type_id) {
                    case 1:
                        // As the Twilio system is self contained, 
                        // and to gain knowledge of any message.
                        // the LogNote is written internally
                        $TwilioMessage = [
                            'From' => $QueueMessage->from,
                            'To' => $QueueMessage->to,
                            'Body' => $body . ' ' . $link,
                            'log_id' => $QueueMessage->log_id,
                            'person_id' => $QueueMessage->person_id,
                            'user_id' => $QueueMessage->user_id,
                            'from_addy' => $QueueMessage->from,
                            'from_name' => $QueueMessage->from_name,
                            'dest_name' => $QueueMessage->person->sname . ', ' . $QueueMessage->person->fname,
                            'dest_addy' => $QueueMessage->to,
                            'title' => $QueueMessage->subject,
                            'subject' => $QueueMessage->subject,
                            'TwilioId' => $QueueMessage->client->TwilioId,
                            'auth_token' => $QueueMessage->client->auth_token
                        ];
                        if (!empty($Attachments)) {
                            if (count($Attachments) == 1) {
                                $TwilioMessage['mediaUrl'] = config('app.url') . $Attachments[0]['path'];
                            } else {
                                $TwilioMessage['mediaUrl'] = [];
                                foreach ($Attachments as $Attachment) {
                                    $TwilioMessage['mediaUrl'][] = config('app.url') . $Attachment['path'];
                                }
                            }

                        }
                        $sid = $Twilio->sendMessage($TwilioMessage);

                        if ($sid) {
                            $Processed = 1;
                        }
                        break;
                    case 3:
                        // Formulate Message to Send.
                        Mail::to($QueueMessage->to)
                            ->send(new QueueMessageMail($QueueMessage, nl2br($body), $link, $Attachments));
                        echo "\t\tMail Sent\n";

                        $LogNote = new LogNote();

                        $QueueMessage->from_addy = $QueueMessage->from;
                        $QueueMessage->dest_name = $QueueMessage->person->sname . ', ' . $QueueMessage->person->fname;
                        $QueueMessage->dest_addy = $QueueMessage->to;

                        $LogNote->setProperty('log_id', $QueueMessage)
                            ->setProperty('person_id', $QueueMessage)
                            ->setProperty('from_name', $QueueMessage)
                            ->setProperty('from_addy', $QueueMessage)
                            ->setProperty('dest_name', $QueueMessage)
                            ->setProperty('dest_addy', $QueueMessage)
                            ->setProperty('subject', $QueueMessage)
                            ->setProperty('user_id', $QueueMessage)
                            ->setProperty('created', date('Y-m-d g:i:s'));
                        $LogNote->body = $body;
                        $LogNote->version = 2;
                        $LogNote->save();
                        $Processed = 1;
                        sleep(1);
                        break;
                }
            } catch (\Exception $ex) {
                $Processed = 2;
                $ErrorMessage = $ex->getMessage();
                $Internal = new Internal();
                // Set Message
                $Message = [
                    'action' => 'Internal Notification',
                    'brief' => 'QueueMessage Error.',
                    'body' => $ex->getMessage()
                ];
                $From = [
                    'from' => 'system@reachoutwizard.com',
                    'from_name' => 'ReachoutWizard System'
                ];
                $client_id = $QueueMessage->client_id;
                $user_ids = [1];

                $Internal->create($Message, $client_id, $user_ids, now(), $From, $QueueMessage->person_id);
            }
            // Check for Internal Messages to be created
            $Internals = $this->checkInternals($QueueMessage);
            if ($Internals) {
                $Processed = $Internals;
            }
            // Mark the message as sent
            if ($Processed != 0) {
                unset($QueueMessage->from_addy);
                unset($QueueMessage->dest_name);
                unset($QueueMessage->dest_addy);
                //Log::info(__METHOD__ . $Processed);
                $QueueMessage->processed = $Processed;
                $QueueMessage->error_message = $ErrorMessage;
                $QueueMessage->save();
            }
        }

        echo "End\n";
    }

    private function checkInternals($QueueMessage)
    {
        if ($QueueMessage->Internal->count()) {
            // Create Message
            $Message = new Message();
            $Message->log_id = $QueueMessage->log_id;
            $Message->brief = $QueueMessage->subject;
            $Message->m_type_id = 4;
            $Message->m_status_id = 1;
            $Message->client_id = $QueueMessage->client_id;
            $Message->from = $QueueMessage->from;
            $Message->to = $QueueMessage->to;
            $Message->person_id = $QueueMessage->person_id;
            $Message->person_method_id = 0;
            $Message->body = $QueueMessage->body;
            $Message->save();
            // Place Mesage
            foreach ($QueueMessage->Internal as $Internal) {
                // Place in Mailbox & Notify
                $UserModel = new User();
                $User = $UserModel->find($Internal->user_id);
                $message_id = $Message->id;
                Log::info(__METHOD__ . ' MessageId: ' . $message_id);

                if ($User) {
                    // Place in Mailbox of User
                    $UserMessage = new UserMessage();
                    $UserMessage->message_id = $message_id;
                    $UserMessage->m_status_id = 1; // New
                    $UserMessage->user_id = $User->id;
                    $UserMessage->save();

                    // Add Notify by Email
                    $this->PhoneSystem->notifybyEmail($User->id, $UserMessage->id);
                }
                // @todo: Mark as Processed
            }
            return 1;
        }
    }

    public function resend($log_id)
    {
        $retval = ['log_id' => $log_id];
        $data = QueueMessage::with('Short')->where('log_id', '=', $log_id)->get();
        Log::info(__METHOD__ . ':' . __LINE__ . print_r($data->toarray(), true));
        if (!empty($data->toarray())) {
            $retval['data'] = $data;
            $retval['body'] = $data[0]['body'];
            $retval['subject'] = $data[0]['subject'];
            if ($data[0]['short_id']) {
                $retval['short_id'] = $data[0]['short']['id'];
                $retval['short_name'] = $data[0]['short']['name'];
            } else {
                $retval['short_id'] = 0;
            }
            // Method_IDs
            $retval['method_ids'] = [];
            foreach ($data as $datum) {
                if ($datum->person_method_id) {
                    $retval['method_ids'][] = $datum->person_method_id;
                }
            }
        }
        return $retval;
    }
}
