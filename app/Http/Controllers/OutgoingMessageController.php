<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\OutgoingMessage as EnumModel;
use App\Model\Short;
use Illuminate\Support\Facades\Log;

class OutgoingMessageController extends Controller {

    private $request;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->middleware('auth');
        $this->request = $request;
    }

    public function index() {
        // Get list of Enums within Client
        $Enums = EnumModel::where('client_id', '=', $this->request->session()->get('client.id'))
                ->get();
        return $Enums;
    }

    public function view($outgoing_message_id) {
        // Retreive data for an individual Enum
        $OutgoingMessage = EnumModel::where('id', '=', $outgoing_message_id)
                ->with('Short')
                ->first();

        return $OutgoingMessage;
    }

    public function save() {
        if ($this->request->id) {
            $Item = EnumModel::find($this->request->id);
            $message = 'saved';
        } else {
            $Item = new EnumModel();
            $message = 'updated';
        }
        $Item->setProperty('title', $this->request)
                ->setProperty('body', $this->request)
                ->setProperty('short_id', $this->request);
        $Item->client_id = $this->request->session()->get('client.id');
        $Item->save();
        if ($this->request->short_id) {
            $Item->short = Short::find($this->request->short_id);
        }
        $Item->message = 'The Preset Message has been ' . $message . '.';
        return $Item;
    }

    public function delete() {
        $Item = EnumModel::find($this->request->id)->delete();
        return ['message' => 'The Preset Message has been deleted.'];
    }

}
