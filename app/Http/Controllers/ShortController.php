<?php

namespace App\Http\Controllers;

use App\Model\Short;
use App\Component\BaseConvert;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ShortController extends Controller
{

    // Uther /web functions are in the App\Http\Controllers\Web namespace

    private $request;
    private $BaseConvert;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        // Do not require a login
        $this->request = $request;
        $this->BaseConvert = new BaseConvert();
    }

    public function gotoShort($short_id)
    {
        // Get list of All Active Shorts within Client
        Log::info(__METHOD__ . ' ' . $short_id);
        $id = $this->BaseConvert->to10($short_id);
        Log::info(__METHOD__ . ' ' . $id);
        $Short = Short::find($id);
//        Log::info(__METHOD__ . ' ' . print_r($Short->toArray(), true));

        return Redirect::to($Short->url);
    }

}
