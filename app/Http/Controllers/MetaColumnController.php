<?php

namespace App\Http\Controllers;

use App\Model\MetaColumn;
use App\Model\MetaData;
use App\Model\PersonMethodType;
use App\Component\PhoneSystem;
use App\Model\MetaEnumValue;
use App\Model\MCType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class MetaColumnController extends Controller {

    private $request;
    public $phoneSystem;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->middleware('auth');
        $this->request = $request;
        $this->phoneSystem = new PhoneSystem();
    }

    public function index($type = 1) {
        if (!empty($type)) {
            $MetaColumns = MetaColumn::where('client_id', '=', $this->request->session()->get('client.id'))
                ->where('meta_column_type_id', '=', $type)
                ->with('MCType')
                ->get();
        } else {
            $MetaColumns = MetaColumn::where('client_id', '=', $this->request->session()->get('client.id'))
                ->with('MCType')
                ->get();
        }
        return $MetaColumns;
    }

    public function people($id) {
        // For Lookup
        $PersonMethodTypeModel = new PersonMethodType();

        $MetaColumn = MetaColumn::where('id', '=', $id)
            ->with('MCType')
            ->get()->toArray();
 
        // Fetch Data Needed
        if ($MetaColumn[0]['m_c_type']['display'] == 'enum') {
            // Extra
            $MetaData = MetaData::where('meta_column_id', '=', $id)
                ->with('person')
                ->with('person.PersonMethod')
                ->with('person.MetaData')
                ->with('person.MetaData.MDValueText')
                ->with('person.MetaData.MDValueText.MetaEnumValue')
                ->get();
        } else {
            $MetaData = MetaData::where('meta_column_id', '=', $id)
                ->with('person')
                ->with('person.PersonMethod')
                ->with('person.MetaData')
                ->with('person.MetaData.MDValueText')
                ->get();
        }

        $retval = [];
        $combos = [];
        foreach ($MetaData as $Person) {
            $Element = $Person->person->toArray();
            if ($Element['deleted'] == 1) {
                continue;
            }
            for ($i = 0; $i < count($Element['meta_data']); $i++) {
                if ($Element['meta_data'][$i]['meta_column_id'] == $id &&
                        !in_array($Element['id'] . '' . $Element['meta_data'][$i]['id'], $combos)) {
                    $MetaData = $Element['meta_data'][$i];
                    $Element['MetaData'] = $MetaData;
                    if (isset($Element['MetaData']['m_d_value_text']['meta_enum_value']['name'])) {
                        $Element['MetaDataValue'] = $Element['MetaData']['m_d_value_text']['meta_enum_value']['name'];
                    } else {
                        $Element['MetaDataValue'] = $Element['MetaData']['m_d_value_text']['value'];
                    }
                    
                    $combos[] = $Element['id'] . '' . $Element['meta_data'][$i]['id'];
                    break;
                }
            }
            unset($Element['m_d_value_text']);

            // Format Person Methods
            for ($i = 0; $i < count($Element['person_method']); $i++) {
                $Method = $Element['person_method'][$i];
                $Method['person_method_formatted'] = $this->phoneSystem->formatPhone($Method['method']);
                $Method['person_method_type_name'] = $PersonMethodTypeModel->lookup($Method['person_method_type_id']);
                $Method['label'] = $Method ['person_method_type_name'] . ': ' . $Method ['person_method_formatted'];
                $Element['PersonMethod'][] = $Method;
            }
            unset($Element['person_method']);
            $retval[] = $Element;
        }

        return $retval;
    }

    public function view($id) {
        $MetaColumn = MetaColumn::where('id', '=', $id)
                ->with('MCType')
                ->first();
        return $MetaColumn;
    }

    public function create() {
        if ($this->request->mc_type_id == -1) {
            // Create Enum
            $MCType = new MCType();
            $MCType->display = 'enum';
            $MCType->client_id = $this->request->session()->get('client.id');
            $MCType->name = $this->request->name;
            $MCType->save();
            $mc_type_id = $MCType->id;
        } else {
            $mc_type_id = $this->request->mc_type_id;
        }
        $MetaColumn = new MetaColumn();
        $MetaColumn->setProperty('name', $this->request)
            ->setProperty('meta_column_type_id', $this->request)
                ->setProperty('vname', $this->request);
        $MetaColumn->mc_type_id = $mc_type_id;
        $MetaColumn->client_id = $this->request->session()->get('client.id');
        $MetaColumn->save();
        return $MetaColumn;
    }

    public function update()
    {
        if ($this->request->id > 0) {
            $MetaColumn = MetaColumn::find($this->request->id);
            $MetaColumn->setProperty('name', $this->request)
                ->setProperty('vname', $this->request);
            $MetaColumn->save();
            return $MetaColumn;
        }
    }

    public function types() {
        // Gather MCTypes available for use in new MetaColumn
        // Get existing MCTypes that are already used and exclusive
        $Currents = MetaColumn::where('client_id', '=', $this->request->session()->get('client.id'))->get();
        $Cur_MCType_Ids = [];

        foreach ($Currents as $Current) {
            $Cur_MCType_Ids[] = $Current->mc_type_id;
        }

        // Formulate exact list of MCTypes available
        // Global MCTypes
        $retval = [];
        $Types = MCType::where('client_id', '=', 0)
                ->get();
        foreach ($Types as $Type) {
            $retval[] = [
                "id" => $Type->id,
                "name" => $Type->name
            ];
        }
        // Local MCTypes
        $localTypes = MCType::where('client_id', '=', $this->request->session()->get('client.id'))
                ->whereNotIn('id', $Cur_MCType_Ids)
                ->get();
        foreach ($localTypes as $localType) {
            $retval[] = [
                "id" => $localType->id,
                "name" => $localType->name
            ];
        }
        $retval[] = [
            "id" => -1,
            "name" => 'List'
        ];
        return $retval;
    }

    public function delete() {
        // On cascade delete
        MetaData::where('meta_column_id', '=', $this->request->id)->delete();
        // Delete an item permanently
        MetaColumn::find($this->request->id)->delete();
    }

}
