<?php

namespace App\Observers;

use App\Model\PersonMethod;
use Illuminate\Support\Facades\Log;

class PersonMethodObserver
{
    /**
     * @param PersonMethod $personMethod
     * @return true
     */
    public function saving(PersonMethod $personMethod) {
        // Prune non-digits from phone number - US formatting
        if (in_array($personMethod->person_method_type_id, [1,2,4])) {
            $personMethod->method = preg_replace('/[^+\d]+/', '', $personMethod->method);
        }
        return true;
    }
}