<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QueueMessage extends infoChiModel {

    //put your code here
    protected $fillable = [
        'log_id'
    ];

    public function Person() {
        return $this->belongsTo('App\Model\Person');
    }

    public function Client() {
        return $this->belongsTo('App\Model\Client');
    }

    public function Internal() {
        return $this->hasMany('App\Model\Internal');
    }

    public function Short() {
        return $this->belongsTo('App\Model\Short');
    }

}
