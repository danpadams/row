<?php

namespace App\Model;

use App\Model\Person;
use App\Model\Response;
use App\Model\infoChiModel;

class Invite extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];

    public function Person() {
        return $this->belongsTo(Person::class);
    }

    public function Response() {
        return $this->belongsTo(Response::class);
    }

}
