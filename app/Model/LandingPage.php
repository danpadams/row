<?php

namespace App\Model;

use App\Model\infoChiModel;

class LandingPage extends infoChiModel
{
    /**
     * @param $request
     * @return mixed
     */
    static public function getCurrent($request)
    {
        $LandingPage = LandingPage::where('domain','=', $request->domain)
            ->first();

        if ($LandingPage) {
            $client_id = $LandingPage['client_id'];
            $checkval = 1;
        } else {
            $client_id = $request->session()->get('client.id');
            $checkval = 0;
        }

        return array($client_id, $checkval);
    }

}
