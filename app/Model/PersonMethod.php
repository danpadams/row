<?php

namespace App\Model;

use App\Model\infoChiModel;
use Illuminate\Support\Facades\Log;

class PersonMethod extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];

    /**
     * @param string $method
     * @param int $client_id
     * @param bool $first
     * return array
     */
    public static function getPersonsFromMethod($method, $client_id, $first = false) {
        $method_short = substr($method, -10);
        $Objs = self::with('Person')
                ->where('method', '=', $method_short)
                ->get();

        $retval = [];
        foreach ($Objs as $Obj) {
            $addItem = true;
            // Must be in current client
            if ($Obj->person->client_id != $client_id) {
                continue;
            }
            // Must be non-deleted
            if ($Obj->person->deleted != 0) {
                continue;
            }
            // Actual Add/Prune of item

            if ($first) {
                return $Obj->toArray();
            }
            $retval[] = $Obj->toArray();
        }
        return $retval;
    }

    public static function createPersonFromMethod($method, $client_id) {
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r('', true));
        $method_short = substr($method, -10);

        $Person = new Person();
        $Person->client_id = $client_id;
        $Person->fname = $method_short;
        $Person->save();
        $person_id = $Person->id;

        $PersonMethod = new PersonMethod();
        $PersonMethod->person_id = $person_id;
        $PersonMethod->person_method_type_id = 1;
        $PersonMethod->method = $method_short;
        $PersonMethod->save();

        return [$PersonMethod->id, $Person->id];
    }

    // Joins
    public function Person() {
        return $this->belongsTo('App\Model\Person');
    }

    public function PersonMethodType() {
        return $this->belongsTo('App\Model\PersonMethodType');
    }

    public function find($method_text, $client_id) {
        $Objs = $this
                ->with('Person')
                ->where('method', '=', $method_text)
                ->where('person_method_type_id', '!=', 10)
                ->get();

        $retval = [];
        foreach ($Objs as $Obj) {
            $addItem = true;
            // First Time
            if ($addItem && $Obj->person->client_id == $client_id) {
                $addItem = true;
            } else {
                $addItem = false;
            }
            // Again        
            if ($addItem && $Obj->person->deleted == 0) {
                $addItem = true;
            } else {
                $addItem = false;
            }
            // Actual Add/Prune of item
            if ($addItem) {
                $retval[] = $Obj;
            }
        }
        return $retval;
    }

}
