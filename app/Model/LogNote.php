<?php

namespace App\Model;

use App\Model\infoChiModel;

class LogNote extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];

    public function Attachment() {
        return $this->hasMany('App\Model\Attachment','log_id', 'log_id');
    }

    public function Person() {
        return $this->belongsTo('App\Model\Person');
    }

    public function Log() {
        return $this->belongsTo('App\Model\Log');
    }


}
