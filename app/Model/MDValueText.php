<?php

namespace App\Model;

use App\Model\infoChiModel;

class MDValueText extends infoChiModel {

    protected $table = 'md_value_text';
    //put your code here
    protected $fillable = [
    ];

    public function MetaEnumValue() {
        return $this->belongsTo('App\Model\MetaEnumValue', 'value', 'id');
    }
}
