<?php

namespace App\Model;

use App\Model\infoChiModel;

class Number extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];

    /**
     * @param $Number
     * @return mixed
     */
    public static function getFromNumber($Number) {
        return self::where('number', '=', $Number)->first();
    }

    /**
     * @return mixed
     */
    public function PTree_Sms() {
        return $this->belongsTo('App\Model\PTree', 'p_tree_sms');
    }

    /**
     * @return mixed
     */
    public function client() {
        return $this->belongsTo('App\Model\Client');
    }

}
