<?php

namespace App\Model;

use App\Model\infoChiModel;

class EventAttendance extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];

    public function Person() {
        return $this->belongsTo(Person::class);
    }

    public function EventAttendanceResponse() {
        return $this->belongsTo(EventAttendanceResponse::class);
    }

}
