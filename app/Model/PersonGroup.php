<?php

namespace App\Model;

use App\Model\infoChiModel;

class PersonGroup extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];

    public function PersonGroupData() {
        return $this->hasMany('App\Model\PersonGroupData');
    }

}
