<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

class PersonMethodType extends Model {

    private $Types;
    //put your code here
    protected $fillable = [
    ];

    public function setupTypes() {
        if (!isset($this->Types)) {
            $Types = $this->get();
            foreach ($Types as $Type) {
                if ($Type) {
                    $this->Types[$Type->id] = $Type->method;
                }
            }
        }
    }

    public function lookup($id) {
        if ($id == 0) {
            return '';
        }
        $this->setupTypes();
        return $this->Types[$id];
    }

}
