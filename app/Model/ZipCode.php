<?php

namespace App\Model;

use App\Model\infoChiModel;
use Illuminate\Support\Facades\Log;

class ZipCode extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];

    public function checkZipCode($checkZipCode) {
        // Check to make sure that ZipCode is in System with Lat & Long
        // Get Data for Check
        $ZipCode = $this->find($checkZipCode);

        if ($ZipCode) {
            // Data is found - thus return
            $this->lat = $ZipCode->lat;
            $this->long = $ZipCode->long;
        } else {
            // Data is not found - add Zip Code, then check google for Lat/Long
            $this->lookupLatLongWithGoogleCurl($checkZipCode);
        }
    }

    private function lookupLatLongWithGoogleCurl($lookupZipCode) {
        // Retreive values of zips that don't have Lat/Long
        // list of zipcodes
        // Log::info(__METHOD__ . ' ' . $lookupZipCode);
        $serviceUrl = "http://maps.googleapis.com/maps/api/geocode/json?components=postal_code:%s&sensor=false";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, sprintf($serviceUrl, urlencode($lookupZipCode)));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        $data = json_decode(curl_exec($curl));
        $info = curl_getinfo($curl);
        if ($info['http_code'] != 200) {
//            echo "\tRequest Failed\n";// Request failed
        } else if ($data->status !== 'OK') {
//            echo "\tNo Results\n"; // Something happened, or there are no results
        } else {
            $this->id = $lookupZipCode;
            $this->lat = $data->results[0]->geometry->location->lat;
            $this->long = $data->results[0]->geometry->location->lng;
//            print_r($this->toArray());
            $this->save();
        }
    }

}
