<?php

namespace App\Model;

use App\Model\infoChiModel;

class MetaColumn extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];

    public function MCType() {
        return $this->belongsTo('App\Model\MCType', 'mc_type_id', 'id');
    }

}
