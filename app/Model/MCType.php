<?php

namespace App\Model;

use App\Model\infoChiModel;

class MCType extends infoChiModel {

    protected $table = 'mc_types';
    //put your code here
    protected $fillable = [
    ];

    public function MetaEnumValue() {
        // return $this->hasMany($related, $foreignKey, $localKey)
        return $this->hasMany('App\Model\MetaEnumValue', 'mc_type_id', 'id');
    }

}
