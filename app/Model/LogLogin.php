<?php

namespace App\Model;

use App\Model\infoChiModel;

class LogLogin extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];
    
    public function User() {
        return $this->belongsTo('App\Model\UserDetail');
    }

    public function Client() {
        return $this->belongsTo('App\Model\Client');
    }
}
