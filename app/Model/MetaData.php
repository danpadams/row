<?php

namespace App\Model;

use App\Model\Person;
use App\Model\infoChiModel;

class MetaData extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];

    public function Person() {
        return $this->belongsTo(Person::class);
    }

    public function MDValueText() {
        return $this->hasOne('App\Model\MDValueText');
    }

    public function MetaColumn() {
        return $this->belongsTo('App\Model\MetaColumn', 'meta_column_id', 'id');
    }
}
