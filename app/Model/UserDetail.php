<?php

namespace App\Model;

use App\Model\Person;
use App\Model\infoChiModel;

class UserDetail extends infoChiModel {

    protected $table = 'cake_users';
    //put your code here
    protected $fillable = [
    ];

    public function ClientUser() {
        return $this->hasMany('App\Model\ClientUser', 'user_id');
    }
    
    public function Person() {
        return $this->belongsTo('App\Model\Person');
    }
}
