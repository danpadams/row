<?php

namespace App\Model;

use App\Model\infoChiModel as Model;

class PBranch extends Model {

    //put your code here
    protected $fillable = [
    ];

    public function Tree() {
        return $this->belongsTo('App\Model\PTree', 'p_tree_id');
    }
    
    public function Type() {
        return $this->belongsTo('App\Model\PType', 'p_type_id');
    }

    public function NextBranch() {
        return $this->belongsTo('App\Model\PBranch', 'next_id');
    }
//                    ParentBranch
    public function ParentBranch() {
        return $this->belongsTo('App\Model\PBranch', 'parent_id');
    }
}
