<?php

namespace App\Model;

use App\Model\ClientFeature;
use Illuminate\Database\Eloquent\Model;

class Client extends Model {

    private $clients;

    //put your code here
    protected $fillable = [
    ];

    public function getFullList()
    {
        if (empty($this->clients)) {
            $retval = [];
            $Clients = Client::where('closed', '=', '0')->get();
            foreach ($Clients as $Client) {
                $retval[] = [
                    'id' => $Client->id,
                    'label' => $Client->name
                ];
            }

            $this->clients = $retval;
        }
        return $this->clients;
    }


    // Joins
    public function ClientFeature() {
        return $this->hasMany(ClientFeature::class);
    }

}
