<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ClientUserFeature extends Model {

    //put your code here
    protected $fillable = [
    ];

    public function Feature() {
        return $this->belongsTo('App\Model\Feature', 'feature_id');
    }

}
