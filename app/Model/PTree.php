<?php

namespace App\Model;

use App\Model\infoChiModel as Model;

class PTree extends Model {

    //put your code here
    protected $fillable = [
    ];

    public function PKind() {
        return $this->belongsTo('App\Model\PKind');
    }
    
    public function PBranch() {
        return $this->hasMany('App\Model\PBranch');
    }
}
