<?php

namespace App\Model;

use App\Model\infoChiModel;

class Log extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];

    public function Attachment() {
        return $this->hasMany('App\Model\Attachment');
    }

    public function LogNote() {
        return $this->hasMany('App\Model\LogNote');
    }

}
