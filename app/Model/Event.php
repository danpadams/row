<?php

namespace App\Model;

use App\Model\EventAttendance;
use App\Model\Invite;
use App\Model\infoChiModel;

class Event extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];

    public function Invite() {
        return $this->hasMany(Invite::class);
    }
    
    public function EventAttendance() {
        return $this->hasMany(EventAttendance::class);
    }
}
