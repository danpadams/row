<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;
use App\Model\Client;

class ClientUser extends Model
{
    //put your code here
    protected $fillable = [
    ];

    public function getAddList($myClients)
    {
        $ClientModel = new Client();
        $Clients = $ClientModel->getFullList();
        foreach ($Clients as $key => $client) {
            if (!empty($myClients[$client['id']])) {
                unset($Clients[$key]);
            }
        }
        $retval = [];
        foreach ($Clients as $client){
            $retval[] = $client;
        }
        return $retval;
    }

    /*
     * @todo: Refactor Join so that only id and name are returned
     */

    public function Client()
    {
        return $this->belongsTo('App\Model\Client', 'client_id');
    }

    public function User()
    {
        return $this->belongsTo('App\Model\UserDetail', 'user_id');
    }


}
