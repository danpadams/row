<?php

namespace App\Model;

use App\Model\infoChiModel;

class Person extends infoChiModel {

    protected $table = 'persons';
    //put your code here
    protected $fillable = [
        'fname', 'sname', 'address', 'city', 'state', 'zip'
    ];

    public function PersonMethod() {
        return $this->hasMany('App\Model\PersonMethod');
    }

    public function PersonEmail() {
        return $this->hasMany('App\Model\PersonMethod')->where('person_method_type_id', 3);
    }

    public function ZipCode() {
        return $this->hasOne('App\Model\ZipCode', 'id', 'zip');
    }

    public function MetaData() {
        return $this->hasMany('App\Model\MetaData');
    }

}
