<?php

namespace App\Model;

use App\Model\infoChiModel;

class Message extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];

    public function Person() {
        //        return $this->hasOne($related, $foreignKey, $localKey)
        return $this->hasOne('App\Model\Person', 'id', 'person_id');
    }

    public function Client() {
        return $this->belongsTo('App\Model\Client')->select(['id', 'name']);
    }

    public function UserMessage() {
        return $this->hasMany('App\Model\UserMessage');
    }

    public function MType() {
        return $this->belongsTo('App\Model\MType');
    }

    public function MStatus() {
        return $this->belongsTo('App\Model\MStatus');
    }

}
