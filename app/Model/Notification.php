<?php

namespace App\Model;

use App\Model\infoChiModel;

class Notification extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];
    
    public function UserMessage() {
        return $this->belongsTo('App\Model\UserMessage');
    }
    
    public function PersonMethod() {
        return $this->belongsTo('App\Model\PersonMethod');
    }

}
