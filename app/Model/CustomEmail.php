<?php

namespace App\Model;

use App\Model\infoChiModel;

class CustomEmail extends infoChiModel {

    //put your code here
    protected $fillable = [
        'name', 'settings'
    ];

    public function Domain() {
        return $this->belongsTo('App\Model\Domain');
    }
}
