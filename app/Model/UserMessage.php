<?php

namespace App\Model;

use App\Model\infoChiModel;

class UserMessage extends infoChiModel {

    //put your code here
    protected $fillable = [
    ];

    public function User() {
        return $this->belongsTo('App\Model\UserDetail');
    }

    public function Message() {
        //        return $this->hasOne($related, $foreignKey, $localKey)
        return $this->hasOne('App\Model\Message', 'id', 'message_id');
    }

    public function MStatus() {
        return $this->belongsTo('App\Model\MStatus');
    }

}
