<?php

namespace App\Model;

use App\Model\infoChiModel;

class PersonGroupData extends infoChiModel {

    protected $table = 'person_group_data';
    //put your code here
    protected $fillable = [
    ];

    public function Person() {
        return $this->belongsTo('App\Model\Person', 'person_id', 'id');
    }

    public function PersonGroup() {
        return $this->belongsTo('App\Model\PersonGroup', 'person_group_id', 'id');
    }

}
