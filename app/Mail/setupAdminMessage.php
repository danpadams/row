<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class setupAdminMessage extends Mailable
{

    use Queueable,
        SerializesModels;

    public $Data;
    public $Client;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data, $client)
    {
        $this->Data = $data;
        $this->Client = $client;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('system@reachoutwizard.com', 'ReachoutWizard System')
            ->subject('ReachoutWizard - New Client Setup')
            ->view('emails.setupAdmin');
    }

}
