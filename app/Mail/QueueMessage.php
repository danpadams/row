<?php

namespace App\Mail;

use App\Model\QueueMessage as QueueMessageModel;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class QueueMessage extends Mailable {

    use Queueable,
        SerializesModels;

    private $QueueMessage;
    private $Attachments;
    public $Body;
    public $Link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(QueueMessageModel $QueueMessage, $body, $link, $Attachments = []) {
        $this->QueueMessage = $QueueMessage;
        $this->Body = $body;
        $this->Link = $link;
        $this->Attachments = $Attachments;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build() {
        $Message = $this->from($this->QueueMessage->from, $this->QueueMessage->from_name)
            ->subject($this->QueueMessage->subject)
            ->view('emails.QueueMessage');
        foreach ($this->Attachments as $Attachment) {
            $Message = $Message->attach(public_path($Attachment['path']));
        }
        return $Message;
    }

}
