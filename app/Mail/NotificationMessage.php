<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;

class NotificationMessage extends Mailable
{

    use Queueable,
        SerializesModels;

    public $Note;
    public $Link;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($Note, $App)
    {
        $this->Note = $Note;
        $this->Link = $App . '/login?#/messages/' . $Note['user_message']['message']['id'];
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('system@reachoutwizard.com', 'ReachoutWizard System')
            ->subject('You have a new message')
            ->view('emails.Notification');
    }

}
