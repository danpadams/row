<?php

namespace App\Traits;

trait Lookup {

    private $Types;

    public function setupTypes() {
        if (!isset($this->Types)) {
            $Types = $this->get();
            foreach ($Types as $Type) {
                if (isset($Type->method)) {
                    $this->Types['method'][$Type->id] = $Type->method;
                }
                if (isset($Type->name)) {
                    $this->Types['name'][$Type->id] = $Type->name;
                }
                if (isset($Type->label)) {
                    $this->Types['label'][$Type->id] = $Type->label;
                }
            }
        }
    }

    public function lookup($id, $name = 'method') {
        $this->setupTypes();
        return $this->Types[$name][$id];
    }

}
