<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\NotificationController;
use Illuminate\Http\Request;

class checkNotifications extends Command {

    private $request;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:notifications';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Notification queue for Items to Send';

    /*
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(Request $request) {
        $this->request = $request;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $NCtl = new NotificationController($this->request);
        $NCtl->run();
        //
    }

}
