<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\PersonController;
use Illuminate\Http\Request;

class checkZips extends Command {

    private $request;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:zips';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check ZipCodes for Lat & Long';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $PersonCtrl = new PersonController($this->request);
        $PersonCtrl->checkZipCodes();
        echo "End\n";
    }

}
