<?php

namespace App\Console\Commands;

use App\Component\Usage;
use App\Model\CronRunner;
use Illuminate\Console\Command;
use App\Http\Controllers\QueueMessageController;
use Illuminate\Http\Request;

class checkUsage extends Command
{

    private $request;
    private $cron_name = 'usage';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:usage';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Queue for Items to Send';

    /*
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $Usage = new Usage();
        $CronRunner = CronRunner::where('cron_name', '=', $this->cron_name)->first();
        // Extra strotime item is necessary to check in comparrison to the start of the current hour
        $checkval =  strtotime(date('Y-m-d H:00:00'));

        $timestamp = strtotime($CronRunner->lastrun);

        while ($timestamp < $checkval) {
            $start_of_process = date('Y-m-d H:00:00', $timestamp);
            echo $start_of_process . "\n";

            $Usage->getTwilio($start_of_process);
            $timestamp = strtotime('+1 hour', strtotime($start_of_process));
        }

        $start_of_process = date('Y-m-d H:00:00', $timestamp);
        $CronRunner->lastrun = $start_of_process;
        $CronRunner->save();
        echo $start_of_process . "\n";
    }

}
