<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Http\Request;

class checkAll extends Command {

    private $request;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:all';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check All items for processing (Queue, Zips Codes, Notifications)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        // Outbound Actions
        echo "\n-- check:queue\n";
        $this->call('check:queue');
        // Wrap up with Notify
        echo "\n-- check:notifications\n";
        $this->call('check:notifications');
        // Check Zips
        echo "\n-- check:zips\n";
        $this->call('check:zips');
    }

}
