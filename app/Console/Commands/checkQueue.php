<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\QueueMessageController;
use Illuminate\Http\Request;

class checkQueue extends Command {

    private $request;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:queue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Queue for Items to Send';

    /*
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(Request $request) {
        $this->request = $request;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $QMCtl = new QueueMessageController($this->request);
        $QMCtl->run();
        //
    }

}
