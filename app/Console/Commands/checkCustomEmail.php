<?php

namespace App\Console\Commands;

use App\Component\CustomEmail;
use Illuminate\Console\Command;
use Illuminate\Http\Request;

class checkCustomEmail extends Command
{

    private $request;
    private $cron_name = 'custom_email';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'check:custom_email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Custom Emails to update';

    /*
     * Create a new command instance.
     *
     * @return void
     */

    public function __construct(Request $request)
    {
        $this->request = $request;
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        echo "Check: Custom_Email\n";
        $CustomEmail = new CustomEmail();
        $CustomEmail->process($this->cron_name);
    }

}
