<?php

namespace App\Component;

use Illuminate\Support\Facades\Log;

class BaseConvert {

    private $base = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

    public function toBase($num, $b = 62) {
        $base = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $r = $num % $b;
        $res = $this->base[$r];
        $q = floor($num / $b);
        while ($q) {
            $r = $q % $b;
            $q = floor($q / $b);
            $res = $this->base[$r] . $res;
        }
        return $res;
    }

    public function to10($num, $base = 62) {
        // Make $num be string of num base 10 for processing 
    // $orig = $num;
        if (is_int($num)) {
            $num = strval($num);
        }

        $limit = strlen($num);
        $res = strpos($this->base, $num[0]);
//        Log::info(__METHOD__ . ' num_:' . $num);
//        Log::info(__METHOD__ . ' num_:' . $num[0]);
//        Log::info(__METHOD__ . ' res:' . $res);
        for ($i = 1; $i < $limit; $i++) {
            $res = ($base * $res) + strpos($this->base, $num[$i]);
//            Log::info(__METHOD__ . ' num_:' . $num[1]);
//            Log::info(__METHOD__ . ' b   :' . $b);
//            Log::info(__METHOD__ . ' res:' . $res);
        }

        return $res;
    }

}
