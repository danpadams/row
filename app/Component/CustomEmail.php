<?php

namespace App\Component;

use App\Model\CustomEmailList;
use App\Model\QueueCustom;
use App\Model\CustomEmail as CustomEmailModel;
use App\Model\UserDetail;
use Illuminate\Support\Facades\Log;

class CustomEmail
{
    private $data = [];

    private $client_ids = [];

    public function process($queue_name)
    {
        // queue name = custom_email
        $QueueCustom = QueueCustom::where('queue', '=', $queue_name)
            ->where('processed', '=', 0)
            ->get();

        foreach ($QueueCustom as $queueCustom) {
            $client_id = $queueCustom->client_id;
            echo "\tClient_Id: " . $client_id . "\n";

            if (!in_array($client_id, $this->client_ids)) {
                $CustomEmails = CustomEmailModel::where('client_id', '=', $client_id)
                    ->with('Domain')->get();
                foreach ($CustomEmails as $CustomEmail) {
                    $settings = unserialize($CustomEmail->settings);
                    foreach ($settings['recipients'] as $recipient) {
                        $element['user_id'] = $recipient['user_id'];
                        $element['client_id'] = $client_id;
                        $element['email_name'] = $CustomEmail->name;
                        $element['email_address'] = $CustomEmail->username . '@' . $CustomEmail->domain->domain;
                        $this->checkBasics($client_id, $recipient['user_id']);
                        $this->addEmailToList($element);
                    }
                }
                $this->client_ids[] = $client_id;
            }
            $queueCustom->processed = 1;
            $queueCustom->save();
        }
    }

    private function addEmailToList($element = [])
    {
        $client_id = $element['client_id'];
        $user_id = $element['user_id'];
        if (empty($this->data[$client_id][$user_id])) {
            // Get Data
            $this->data[$client_id][$user_id] = CustomEmailList::where('client_id', '=', $client_id)
                ->where('user_id', '=', $user_id)
                ->orderBy('sort')
                ->get();
        }
        if (!empty($this->data[$client_id][$user_id])) {
            $found = false;
            foreach ($this->data[$client_id][$user_id] as $datum) {
                if ($element['email_address'] == $datum['email']) {
                    // Already in the list for this client_id/user_id
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                $element['sort'] = $this->getMaxSort($this->data[$client_id][$user_id]) + 10;
                $customEmailList = new CustomEmailList();
                $customEmailList->setFromArray('user_id', $element)
                    ->setFromArray('client_id', $element)
                    ->setFromArray('sort', $element);
                $customEmailList->email = $element['email_address'];
                $customEmailList->name = $element['email_name'];
                if (!empty($element['suffix'])) {
                    $customEmailList->suffix = $element['suffix'];
                }

                $customEmailList->save();
                $this->data[$client_id][$user_id] = CustomEmailList::where('client_id', '=', $client_id)
                    ->where('user_id', '=', $user_id)
                    ->orderBy('sort')
                    ->get()->toarray();

            }
        }
    }

    private function checkBasics($client_id, $user_id)
    {
        $User = UserDetail::find($user_id);

        $Element1['email_name'] = $User->email_name;
        $Element1['email_address'] = 'user.' . $User->id . '@' . config('row.domain.System');
//        $Element1['label'] = '' . $Element1['email_name'] . ' - ' . $Element1['email_address'] . '';
//        if (!empty($User->email_primary)) {
            $retval[] = $Element1;
//            unset($Element1);
//        }
//        $element ['email_name'] = $User->email_name;
//        $element ['email_address'] = $User->email_address;
////        $element ['label'] = '' . $element['email_name'] . ' - ' . $element['email_address'] . ' (External)';
//        $element['suffix'] = '(External)';
//        $retval[] = $element;
//        if (isset($Element1)) {
//            $retval[] = $Element1;
//            unset($Element1);
//        }
        foreach ($retval as $ret) {
            $ret['user_id'] = $user_id;
            $ret['client_id'] = $client_id;
            $this->addEmailToList($ret);
        }
    }

    private function getMaxSort($data)
    {
        $sort = 0;
        foreach ($data as $datum) {
            if ($datum['sort'] > $sort) {
                $sort = $datum['sort'];
            }
        }
        return $sort;
    }

}
