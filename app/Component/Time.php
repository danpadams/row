<?php

namespace App\Component;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Time {
    private $timezone = '';
    private $request;
    
    public $components = array('Session');
    
    private function _getTimezone() {
        if (!$this->timezone) {          
            $this->timezone = $this->request->session()->get('user.timezone');
        }

        return $this->timezone;
    }

    private function _toLocal($timestring, $format, $timezone) {
        date_default_timezone_set($timezone);
        $time = strtotime($timestring . ' UTC');
        $retval = date($format, $time);

        date_default_timezone_set('UTC');
        return $retval;
    }
    
    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function toLocal($timestring, $format = 'Y-m-d g:i A') {
        $timezone = $this->_getTimezone();
        $retval = $this->_toLocal($timestring, $format, $timezone);

        return $retval;
    }

    public function toLocalTS($timestring, $format = 'Y-m-d H:i:s') {
        $timezone = $this->_getTimezone();
        $retval = $this->_toLocal($timestring, $format, $timezone);

        return $retval;
    }

}
