<?php

namespace App\Component;

use App\Model\UserDetail as User;
use App\Model\PersonMethod;
use App\Model\Notification;

class Messaging {

    public static function notifyByEmail($user_id, $user_message_id) {
        // Add Notification
        // UserDetail used for offline reasons
        $User = User::find($user_id);
        $PersonMethod = PersonMethod::where('person_id', $User->person_id)
                ->where('person_method_type_id', '=', 3) // E-mail
                ->first();
        if ($PersonMethod) {
            $Notification = new Notification();
            $Notification->user_message_id = $user_message_id;
            $Notification->person_method_id = $PersonMethod->id;
            $Notification->save();
        }
    }
}
