<?php

namespace App\Component;

use App\Model\LogNote;
use App\Model\UsageTwilio;
use Illuminate\Support\Facades\Log;
use App\Model\Attachment as AttachmentModel;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Usage
{
    public function getTwilio($start_of_process)
    {
        $end_of_process = date('Y-m-d H:00:00', strtotime('+1 hour', strtotime($start_of_process)));

        $LogNotes = LogNote::with('Log')
            ->where('created_at', '>=', $start_of_process)
            ->where('created_at', '<', $end_of_process)
            ->where('twilio_sid', '!=', '')
            ->get()->toarray();

       if (!empty($LogNotes)) {
            $Data = [];
            foreach ($LogNotes as $LogNote) {
                $client_id = $LogNote['log']['client_id'];
                if (!empty($Data[$client_id])) {
                    $Data[$client_id]++;
                } else {
                    $Data[$client_id] = 1;
                }
            }
            foreach ($Data as $key => $Dat) {
                $UsageTwilio = new UsageTwilio();
                $UsageTwilio->client_id = $key;
                $UsageTwilio->start_time = $start_of_process;
                $UsageTwilio->usage_twilio_count = $Dat;
                echo "\t{$key} - \t{$start_of_process} - \t{$Dat}\n";
                $UsageTwilio->save();
            }
        }
    }
}

