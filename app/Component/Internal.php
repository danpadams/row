<?php

namespace App\Component;

use App\Model\Internal as InternalModel;
use App\Model\Log as LogModel;
use App\Model\QueueMessage;
use Illuminate\Support\Facades\Log;

class Internal {

    /**
     * 
     * @param array $Message
     * @param int $client_id
     * @param array $users,
     * @return int
     */
    public function create($Message, $client_id, $user_ids, $processtime, $From, $person_id) {
         // Creating QueueMessage-internal
        $Log = new LogModel();
        $Log->client_id = $client_id;
        $Log->action = $Message['action'];
        $Log->brief = $Message['brief'];
        $Log->save();
  
        $QM = new QueueMessage();
        $QM->body = $Message['body'];
        $QM->log_id = $Log->id;
        $QM->m_type_id = 4;
        $QM->client_id = $client_id;
        $QM->user_id = $user_ids[0];
        $QM->person_id = $person_id;
        $QM->from = $From['from'];
        $QM->from_name = $From['from_name'];
        $QM->subject = $Message['brief'];
        $QM->processtime = date('Y-m-d H:i:s', strtotime($processtime));
        $QM->save();

        $InternalModel = new InternalModel();
        $InternalModel->queue_message_id = $QM->id;
        $InternalModel->user_id = $user_ids[0];
        $InternalModel->save();

        return 1;
    }

}
