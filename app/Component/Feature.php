<?php

namespace App\Component;

use App\Model\ClientFeature;
use App\Model\ClientUserFeature;
use Illuminate\Support\Facades\Log;

class Feature
{

    public static function list($params)
    {
        $user_id = (empty($params['user_id'])) ? '' : $params['user_id'];
        $user_role = (empty($params['user_role'])) ? '' : $params['user_role'];
        $roleClient = $params['client_role'];
        $client_id = $params['client_id'];

        // Generate list of features relative to permissions
        if ($user_role == 'admin' || $roleClient == 'admin') {
            $ClientFeatureModel = new ClientFeature();
            $Features = $ClientFeatureModel
                ->with('Feature')
                ->where('client_id', '=', $client_id)
                ->get();
        } else {
            $ClientUserFeatureModel = new ClientUserFeature();
            $Features = $ClientUserFeatureModel
                ->with('Feature')
                ->where('client_id', '=', $client_id)
                ->where('user_id', '=', $user_id)
                ->get();
        }
         return $Features;
    }

}
