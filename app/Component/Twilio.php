<?php

namespace App\Component;

/*
 * Author: Dan Adams
 * Email Address: dan@infochi.com
 */

use App\Model\LogNote;
use Twilio\Rest\Client as Services_Twilio;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Twilio
{

    private $AccountSid;
    private $AuthToken;
    private $client;

    public function __construct()
    {
        $this->AccountSid = config('row.twilio.AccountSid');
        $this->AuthToken = config('row.twilio.AuthToken');


        $this->client = new Services_Twilio($this->AccountSid, $this->AuthToken);
    }

    public function createSubAcct($FriendlyName)
    {
        $account = $this->client->accounts->create(array(
            "FriendlyName" => $FriendlyName
        ));
        $retval = array(
            'sid' => $account->sid,
            'auth_token' => $account->auth_token,
            'status' => $account->status
        );
        return $retval;
    }

    private function checkData($data)
    {
        if ($data['auth_token'] && $data['TwilioId']) {
            $this->client = new Services_Twilio($data['TwilioId'], $data['auth_token']);
        }
    }

    public function subAcctClose($data)
    {
        $this->checkData($data);
        $this->log(__METHOD__ . print_r($data, true));
        $params = array('Status' => 'closed');
        $this->client->account->update($params);
    }

    // Helper function to send SMS
    public function sendMessage($data)
    {

        // Are we in the correct client
        $this->checkData($data);

        // Send the Message
        $Msg = [
            "From" => $data['From'],
            "Body" => $data["Body"]
        ];
        if (!empty($data['mediaUrl'])) {
            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($data['mediaUrl'], true));
            $Msg['mediaUrl'] = $data['mediaUrl'];
        }

        /**
         * @var Twilio\Rest\Api\V2010\Account\MessageList $this- >client->account->messages
         */
        $messages = $this->client->account->messages;

        /**
         * @var Twilio\Rest\Api\V2010\Account\MessageList $this- >client->account->messages
         */
        $message = $messages->create($data['To'], $Msg);

        if ($message->sid) {
            // Record a LogNote
            // Re-assign Variables
            $data['dest_addy'] = $data['To'];
            $data['body'] = $data['Body'];
            $LogNote = new LogNote();
            $LogNote->setFromArray('log_id', $data)
                ->setFromArray('person_id', $data)
                ->setFromArray('from_addy', $data)
                ->setFromArray('from_name', $data)
                ->setFromArray('dest_name', $data)
                ->setFromArray('dest_addy', $data)
                ->setFromArray('to', $data)
                ->setFromArray('user_id', $data)
                ->setFromArray('body', $data);
            $LogNote->twilio_sid = $message->sid;
            $LogNote->data = $message;
            $LogNote->version = 2;
            $LogNote->save();

            // Display a confirmation message on the screen
            echo "\t\tSent message {$message->sid}\n";
        }
        return $message->sid;
    }

    public function redirect($URL)
    {
        return "<Redirect>$URL</Redirect>";
    }

    public function wrapTwiml($twiml)
    {
        return "<Response>$twiml</Response>";
    }

    public function getMedia($url)
    {
        $parse = explode('/', $url);
        Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($parse, true));
        $media = $this->client->messages($parse[7])
            ->media($parse[9])
            ->fetch();

        $contents = file_get_contents($url);
//        $uploadedFile = new UploadedFile($url,$parse[9],$media->contentType);

        return ['test' => 'Test',
            'media' => $media,
            'url' => $url,
            'uploadedFile' => $contents,
            'image' => $media->contentType
        ];
    }
}
