<?php

namespace App\Component;

use App\Model\LogLogin;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class Login {

    private $request;

    public function __construct(Request $request) {
        $this->request = $request;
    }

    public function log() {
        $LogLogin = new LogLogin();
        $LogLogin->user_id = $this->request->session()->get('user.id');
        $LogLogin->client_id = $this->request->session()->get('client.id');
        $LogLogin->action_at = date('Y-m-d H:i:s');
        $LogLogin->save();
    }

}
