<?php

namespace App\Component;

use App\Model\Message;
use App\Model\Notification;
use App\Model\Number;
use App\Model\PersonMethod;
use App\Model\UserMessage;
use Illuminate\Support\Facades\Log;

class PhoneSystem
{

    //put your code here
    /**
     * Display Related
     */
    public function formatPhone($number)
    {
        $output = '';
        $type = '';
        if (strlen($number) == 12 && substr($number, 0, 2) == '+1') {
            $type = 'US';
        }
        if (strlen($number) == 10 && substr($number, 0, 2) != '+1') {
            $type = 'US';
        }
        $oNumber = $number;
        if ($type == 'US') {
            if (strlen($oNumber) == 12) {
                $number = substr($number, 2);
            }
            $output = '(' . substr($number, 0, 3) . ') ' . substr($number, 3, 3) . '-' . substr($number, 6, 4);
        }
        if (!$output) {
            $output = $number;
        }
        return $output;
    }

    /**
     * @param $user_id
     * @param $user_message_id
     */
    public function notifyByEmail($user_id, $user_message_id)
    {
        Messaging::notifyByEmail($user_id, $user_message_id);
    }

    /**
     * @param $post_data
     * @param $log_id
     * @return mixed
     */
    public function saveMessage($post_data, $log_id)
    {
        $Message = new Message();

        // Basic Data
        $Message->log_id = $log_id;
        $Message->from = $post_data['From'];
        $Message->to = $post_data['To'];
        $Message->brief = substr($post_data['Body'], 0, 99);
        $Message->body = !empty($post_data['Body']) ? $post_data['Body'] : '';
        if ($post_data['SmsSid']) {
            // SMS Message
            $Message->m_type_id = 1;
        } else {
            // Voice Message
            $Message->m_type_id = 2;
        }
        $Message->m_status_id = 1;

        // Find Company
        $Number = Number::getFromNumber($post_data['To']);
        $Message->client_id = $Number->client_id;
        // Find Person From Address
        $PersonMethod = PersonMethod::getPersonsFromMethod($post_data['From'], $Number->client_id, true);
        if (!empty($PersonMethod)) {
            $Message->person_method_id = $PersonMethod['id'];
            $Message->person_id = $PersonMethod['person']['id'];
        }

        $Message->save();
        $message_id = $Message->id;
        Log::info(__METHOD__ . ':' . __LINE__ . ' MessageID: ' . $message_id);

        return $message_id;
    }

    public function deliverMessage($settings, $msg_id)
    {
        foreach ($settings['recipients'] as $recip) {
            // Place Message
            $UserMessage = new UserMessage();
            $UserMessage->message_id = $msg_id;
            $UserMessage->m_status_id = 1; // Start Message as New
            $UserMessage->user_id = $recip['user_id']; // Assign Message to User
            $UserMessage->save();
            if (!empty($recip['person_method_id'])) {
                $recip['method_id'] = $recip['person_method_id'];
            }
            if ($recip['type_id'] == 3 && !empty($recip['method_id'])) {
                $Notification = new Notification();
                $Notification->user_message_id = $UserMessage->id;
                $Notification->person_method_id = $recip['method_id'];
                $Notification->save();
            }
        }
    }

    public function twimlGreet($Settings)
    {
//        $retval = print_r($Settings, true);
        $retval = '';
        if (isset($Settings['url']) && $Settings['url']) {

        } elseif (isset($Settings['library_id']) && $Settings['library_id']) {

        } else {
            $retval .= '<Say';
            if (!isset($Settings['robo_voice'])) {
                $Settings['robo_voice'] = 'man';
            }
            switch (($Settings['robo_voice'])) {
                case 'man':
                case 'woman':
                case 'alice':
                    break;
                default:
                    $Settings['robo_voice'] = man;
            }
            $retval .= ' voice="' . $Settings['robo_voice'] . '"';
            $retval .= '>' . $Settings['robo_text'] . '</Say>';
        }
        return $retval;
    }
}
