<?php

namespace App\Component;

use Illuminate\Support\Facades\Log;
use App\Model\Attachment as AttachmentModel;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class Attachment
{
    public static function getDetails($data)
    {
        // Filename
        if (empty($data['name'])) {
            $data['name'] = '1';
            $data['name'] = basename($data['path']);
        }

        // Is_Image
        switch ($data['mime_type']) {
            case 'image/gif':
            case 'image/jpeg':
            case 'image/jpg':
                $data['is_image'] = 1;
                break;
            default:
                $data['is_image'] = 0;
                break;
        }

        return $data;
    }

    public static function saveAttachment($file, $client_id, $log_id = 0)
    {
        if (!is_scalar($file)) {
            self::saveFile($file, $client_id, $log_id);
        }
    }

    public static function saveFile($file, $client_id, $log_id = 0)
    {
        $datum = [];

        //Display File Name
        $datum['File Name'] = $file->getClientOriginalName();

        //Display File Extension
        $datum['File Extension'] = $file->getClientOriginalExtension();

        //Display File Real Path
        $datum['File Real Path'] = $file->getRealPath();

        //Display File Size
        $datum['File Size'] = $file->getSize();

        //Display File Mime Type
        $datum['File Mime Type'] = $file->getMimeType();

        //Move Uploaded File
        $datum['dir_path'] = '/uploads/' . $client_id;
        $datum['date'] = date('Y-m-d_H-i-s_');
        $datum['file_name'] = $datum['date'] . $file->getClientOriginalName();
        $datum['file_name'] = str_replace(" ", "_", $datum['file_name']);
        $datum['path'] = $datum['dir_path'] . '/' . $datum['file_name'];
        $datum['dest_path'] = public_path('/uploads/' . $client_id);
        $datum['date'] = date('Y-m-d_H:i:s_');
        $datum['mime_type'] = $file->getMimeType();
        $uploaded = $file->move($datum['dest_path'], $datum['file_name']);
        if ($uploaded) {
            $Attachment = new \App\Model\Attachment();
//            $Attachment->size = $file->getSize();
            $Attachment->client_id = $client_id;
            $Attachment->path = $datum['path'];
            $Attachment->mime_type = $datum['mime_type'];
            $Attachment->log_id = $log_id;
            $Attachment->save();
        }

        return $Attachment;

    }

    public static function saveFromUrl($url, $client_id, $log_id)
    {
        {
            // parsed path
            $parse = parse_url($url);
            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($parse, true));
            $pathinfo = pathinfo($parse['path']);
            Log::info(__METHOD__ . ':' . __LINE__ . ' ' . print_r($pathinfo, true));

            // External URL
            $Attachment = new AttachmentModel();
            $Attachment->size = 0;
            $Attachment->client_id = $client_id;
            $Attachment->path = $url;
            $Attachment->mime_type = '';//$datum['mime_type'];
            $Attachment->log_id = $log_id;
            $Attachment->name = $pathinfo['filename'];
            $Attachment->save();
        }
    }
}

