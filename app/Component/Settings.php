<?php

namespace App\Component;

use Illuminate\Support\Facades\Log;

class Settings {

    private $data;

    public function __construct($settings) {
        // Check for Array
        if (is_array($settings)) {
            $this->data = $settings;
            return;
        }
        
        // Check for PHP Serialized
        $test = unserialize($settings);
        if (is_array($test)) {
            $this->data = unserialize($settings);
            return;
        }
        
        // Check for JSON
        /*
         * @todo: Create This Part
         */
    }

    public function getUsers($section) {
        $retval = [];
        $Item = $this->data[$section];
        for ($i = 0; $i < count($Item); $i++) {
            $retval[] = $Item[$i]['user_id'];
        }
        return $retval;
    }

    public function getClientId() {
        return $this->data['client_id'];
    }

}
