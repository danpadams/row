<?php

namespace App\Component;

use App\Model\MetaData;
use App\Model\Person;
use Illuminate\Support\Facades\Log;

class infoChi {

    public function replaceVariables($text, $person_id) {
        // Replace Person Variables - Takes Priority
        $personVariables = array('fname', 'sname', 'address', 'city', 'state', 'zip');
        $PersonModel = new Person();
        $Person = $PersonModel->findOrFail($person_id);
        for ($i = 0; $i < count($personVariables); $i++) {
            $var = $personVariables[$i];
            $text = str_replace('{{' . $var . '}}', $Person->$var, $text);
        }

         // Replace Additional Person Information Variables
        $MetaDataModel = new MetaData();
        $MetaData = $MetaDataModel
            ->where('person_id', '=', $person_id)
            ->with('Person')
            ->with('MetaColumn')
            ->with('MDValueText')
            ->with('MDValueText.MetaEnumValue')
            ->get();
        foreach ($MetaData as $MetaDat) {
            $MetaDatum = $MetaDat->toArray();

            if ($MetaDatum['meta_column']['vname']) {
                $Element['id'] = $MetaDatum['meta_column']['vname'];
                $Element['value'] = '';
                switch ($MetaDatum['meta_column']['mc_type_id']) {
                    case 1:
                        // Boolean Values ignored
                        break;
                    case 4:
                        // Enum Value
                        // Lookup Meta Enum Value Name for Option
                        $Element['value'] = $MetaDatum['m_d_value_text']['meta_enum_value']['name'];
                        break;
                    case 2:
                        // Short Text
                    case 3:
                        // Long Text
                    default:
                        // Display Option alone
                        $Element['value'] = $MetaDatum['m_d_value_text']['value'];
                        break;
                }
                if ($Element['id'] && $Element['value']) {
                    $text = str_replace('{{' . $Element['id'] . '}}', $Element['value'], $text);
                }
            }
        }

        // Clean up text of $$
        $text = preg_replace('/\s*\{\{[^}]*\}\}/', '', $text);
        return $text;
    }

}
