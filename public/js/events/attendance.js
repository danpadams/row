infoChi.controller('eventsAttendanceController', ['$scope', '$http', '$log', '$routeParams', '$uibModal', '$filter', 'infoChiSpinner', 'infoChiList',
    function ($scope, $http, $log, $routeParams, $uibModal, $filter, infoChiSpinner, infoChiList) {
        $scope.count = {"all": 0, "unknown": 0, "invited": 0, "no": 0, "maybe": 0, "yes": 0};
        $scope.filter = {"response_id": undefined};
        $scope.event_id = $routeParams.event_id;
        $scope.choice = {};
        $scope.new_invite = {};
        $scope.pageSize = 15;
        $scope.no_method = {"id": undefined, "label": "Choose Contact Method"};
        $scope.no_person = {"person_id": undefined, "label": "Choose Person"};
        $scope.person_function = {"person_id": -1, "label": "Add All People Visible"};
        $scope.no_status = {"id": undefined, "label": "Choose Invite Response"};

        getInvites = function () {
//            console.log('getInvites');
            // get invites list for current event
            // Check for multi adds waits and only gets list once
            infoChiSpinner.show();
            $http.get('/web/events/' + $scope.event_id + '/attendance').then(function (response) {
                // add "Choose Contact Method" to each invite
                for (key = 0; key < response.data.length; key++) {
                    if (response.data[key].PersonMethod) {
                        response.data[key].PersonMethod.unshift($scope.no_method);
                    } else {
                        response.data[key].PersonMethod = [$scope.no_method];
                    }
                }

                $scope.invites = response.data;
                // Count Responses
                $scope.count = {"all": 0, "unknown": 0, "invited": 0, "no": 0, "maybe": 0, "yes": 0};
                for (i = 0; i < $scope.invites.length; i++) {
                    $scope.count.all++;
                    switch (parseInt($scope.invites[i].response_id)) {
                        case 1:
                            $scope.count.unknown++;
                            break;
                        case 2:
                            $scope.count.no++;
                            break;
                        case 3:
                            $scope.count.maybe++;
                            break;
                        case 4:
                            $scope.count.yes++;
                            break;
                        case 5:
                            $scope.count.invited++;
                            break;
                    }
                }
                // Set Response Values
                if ($scope.responses) {
                    proInvites();
                } else {
                    $scope.$watch('responses', function () {
                        proInvites();
                    });
                }
                infoChiSpinner.hide();
            });
        };
        proInvites = function () {
            // Show response values for each invite
            for (key = 0; key < $scope.invites.length; key++) {
                $scope.invites[key].person = [];
                $scope.invites[key].person[$scope.invites[key].id] = {"id": $scope.invites[key].response_id, "name": $scope.invites[key].response_name};
            }
        };
        $scope.setFilter = function (type, value_id) {
            if (type == 'response_id') {
                $scope.filter.response_id = value_id;
            }
        };
        $scope.runFilter = function (item) {
            if ($scope.filter.response_id == undefined) {
                return true;
            }
            if ($scope.filter.response_id == parseInt(item.response_id))
                return true;
            return false;
        };
        $scope.dropdownChoice = function (item) {
            $scope.addInvite();
        };
        // Generate URL for sendComm
        $scope.sendComm = function () {
            methods = '';
            count = 0;
            // foreach is used for Data Structure
            angular.forEach($scope.choice, function (value, key) {
                if (value.id != undefined) {
                    if (count)
                        methods += ',';
                    methods += value.id;
                    count++;
                }
            });
            if (count > 0) {
                window.location.href = "#/queue_messages/create/pm_ids/" + methods + '/event_id/' + $scope.event_id;
            }
        };

        getEvent = function () {
            // Retreive General Data
            infoChiSpinner.show();
            // $scope.event = {};
            $http.get('/web/events/view/' + $scope.event_id).then(function (response) {
                $scope.event = response.data;
                infoChiSpinner.hide();
            });
        };
        $scope.setMethod = function (type_id) {
            infoChiList.setMethod(type_id, $scope.invites, $scope.choice, $scope.runFilter);
            //
        };

        $scope.getPeople = function () {
            infoChiSpinner.show();
            var valNum = $scope.new_invite.event.id;
            $http.get('/web/events/' + valNum + '/people_list').then(function (data) {
                // Sort data.data
                people = data.data;
//                console.log(people);
                people = $filter('orderBy')(data.data, 'sname', false);
//                console.log(people);
                if (people != undefined) {
                    people.unshift($scope.no_person);
                } else {
                    people = [$scope.no_person];
                }
                people.push($scope.person_function);
                $scope.people = people;
                infoChiSpinner.hide();
            });
        };
        $scope.getResponses = function () {
            infoChiSpinner.show();
            $http.get('/web/attendance/responses').then(function (data) {
                $scope.responses = data.data;
                infoChiSpinner.hide();
            });
        };
        $scope.addInvite = function () {
            console.log('$scope.addInvite()');
            response = $scope.new_invite.response;
            person = $scope.new_invite.person;
            console.log(person.person_id);
            if (person != undefined && person.person_id != undefined && response != undefined && response.id != undefined) {
                switch (person.person_id) {
                    case - 1:
                        // Add all remaining people
                        $scope.addItems = 0;
                        console.log('Add Many');
                        for (var i = 0; i < $scope.people.length; i++) {
                            if ($scope.filterPeople($scope.people[i])) {
                                if ($scope.people[i].person_id > 0) {
                                    $scope.addItems++;
                                    data = {
                                        "person_id": $scope.people[i].person_id,
                                        "event_attendance_response_id": response.id,
                                        "event_id": $scope.event_id
                                    };
                                    infoChiSpinner.show();
                                    $http.post('/web/attendance/create', data).then(function (data) {
                                        // People are filtered based on attendance/invites that are shown so no reload is necessary
                                        // Restrict to once for the outer loop
                                        // running inside the promise
                                        $scope.addItems--;
                                        if ($scope.addItems == 0) {
                                            getInvites();
                                        }
                                        $scope.new_invite.person = $scope.no_person;
                                        infoChiSpinner.hide();
                                    });
                                }
                            }
                        }
                        break;
                    default:
                        data = {
                            "person_id": person.person_id,
                            "event_attendance_response_id": response.id,
                            "event_id": $scope.event_id
                        };
                        infoChiSpinner.show();
                        $http.post('/web/attendance/create', data).then(function (data) {
                            getInvites();
                            $scope.new_invite.person = $scope.no_person;
                            infoChiSpinner.hide();
                        });
                        break;
                }
                // Just Reset it to the beginning
                $scope.new_invite.person = $scope.no_person;
            } else {
                // Nothing to Do                
            }
        };
        $scope.updResponse = function (invite_id, response_id) {
            infoChiSpinner.show();
            data = {
                "event_attendance_response_id": response_id,
                "id": invite_id
            };
            $http.post('/web/attendance/update', data).then(function (response) {
                getInvites();
                infoChiSpinner.hide();
            });
        };
        $scope.removeItem = function (invite) {
            infoChiSpinner.show();
            data = {
                "id": invite.invite_id
            };
            $http.post('/web/attendance/delete', data).then(function (data) {
                getInvites();
                infoChiSpinner.hide();
            });
        };

        // Setup Events for previous use
        $scope.no_event = {"id": 0, "label": "All People"};
        $scope.new_invite.event = $scope.no_event;
        $scope.new_invite.person = $scope.no_person;
        $scope.events = [];
        getEvents = function () {
            infoChiSpinner.show();
            $http.get('/web/events/groups').then(function (data) {
                $scope.events = data.data;
                $scope.events.unshift($scope.no_event);
                infoChiSpinner.hide();
            });
        }
        // Filter though people if in the same group
        $scope.filterPeople = function (item) {
            // Logic is to avoid items already in the list.
            return infoChiList.filter(item.person_id, $scope.invites, 'id');
        };
        // --

        // Run functions to get things started.
        getEvents();
        getEvent();
        getInvites();
        $scope.getPeople();
        $scope.getResponses();
        // Modal Stuff
        $scope.open = function (size) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/html/events/edit-event.html',
                controller: 'editEventController',
                size: size,
                resolve: {
                    modal: function () {
                        return {"title": "Edit Event Details"};
                    },
                    data: function () {
                        console.log($scope.event);
                        return $scope.event;
                    }
                }
            });

            modalInstance.result.then(function (data) {
                $http.post('/web/events/update', data).then(function (data) {
//                    $scope.event = data.data;
                });
                $log.info('Modal dismissed with "Close" at: ' + new Date());
            }, function () {
                $log.info('Modal dismissed with "Cancel" at: ' + new Date());
            });
        };
    }]);

//// Please note that $uibModalInstance represents a modal window (instance) dependency.
//// It is not the same as the $uibModal service used above.
//
//infoChi.controller('editEventController', function ($scope, $uibModalInstance, modal, data) {
//    $scope.modal = modal;
//    $scope.data = data;
//
//    $scope.ok = function () {
//        $uibModalInstance.close($scope.data);
//    };
//
//    $scope.cancel = function () {
//        $uibModalInstance.dismiss('cancel');
//    };
//});
