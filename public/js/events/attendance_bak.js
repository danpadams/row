infoChi.controller('eventsAttendanceController', ['$scope', '$http', '$log',
    '$location', '$uibModal', '$filter', 'infoChiSpinner',
    function ($scope, $http, $log, $location, $uibModal, $filter, infoChiSpinner) {
        $scope.event_id = $location.search().event_id;
        $scope.filter = {"event_attendance_response_id": undefined};
        $scope.choice = {};
        $scope.new_invite = {};
        $scope.pageSize = 15;
        $scope.no_method = {"id": undefined, "label": "Choose Contact Method"};
        $scope.no_person = {"id": undefined, "label": "Choose Person"};
        $scope.person_function = {"id": -1, "label": "Add All People Visible"};
        $scope.no_status = {"id": undefined, "label": "Choose Invite Response"};
        $scope.invites = [];

        // Get Attendee list for current event
        $scope.getInvites = function () {
            if ($scope.addItems == 0) {
                infoChiSpinner.show();
                $http.get('/events/json_attendance/' + $location.search().event_id).then(function (data) {
                    // Set Methods for Contact
                    for (key = 0; key < data.data.length; key++) {
                        if (data.data[key].person_methods) {
                            data.data[key].person_methods.unshift($scope.no_method);
                        } else {
                            data.data[key].person_methods = [$scope.no_method];
                        }
                    }
                    people = $filter('orderBy')(data.data, 'fname', false);
                    people = $filter('orderBy')(people, 'sname', false);

                    $scope.invites = data.data;

                    // Count Responses
                    $scope.count = {"all": 0, "present": 0, "excused": 0, "absent": 0};
                    for (i = 0; i < $scope.invites.length; i++) {
                        $scope.count.all++;
                        switch (parseInt($scope.invites[i].event_attendance_response_id)) {
                            case 0:
                                $scope.count.absent++;
                                break;
                            case 1:
                                $scope.count.present++;
                                break;
                            case 2:
                                $scope.count.excused++;
                                break;
                        }
                    }
                    // Set Response Values
                    if ($scope.responses != undefined) {
                        proInvites();
                    } else {
                        $scope.$watch('responses', function () {
                            proInvites();
                        });
                    }
                    infoChiSpinner.hide('mainSpinner');
                });
            }
        };
        $scope.proInvites = function () {
            for (i = 0; i < $scope.invites.length; i++) {
                $scope.invites[i].person = [];
                $scope.invites[i].person[$scope.invites[i].person_id] = {"id": $scope.invites[i].response_id, "response_name": $scope.invites[i].response_name};
            }
        };
        proInvites = function () {
            $scope.proInvites();
        }
        $scope.setFilter = function (type, value_id) {
            if (type == 'event_attendance_response_id') {
                $scope.filter.event_attendance_response_id = value_id;
            }
            if (type == 'gender') {
                $scope.filter.gender = value_id;
            }
        }
        $scope.runFilterResponse = function (item) {
            if ($scope.filter.event_attendance_response_id == undefined)
                return true;
            if ($scope.filter.event_attendance_response_id == parseInt(item.event_attendance_response_id))
                return true;
            return false;
        }
        $scope.runFilterGender = function (item) {
            if ($scope.filter.gender == undefined)
                return true;
            if ($scope.filter.gender == parseInt(item.person_gender))
                return true;
            return false;
        }
        $scope.dropdownResponse = function (item) {
            // No Code Here Yet
        }
        $scope.dropdownPerson = function (item) {
            $scope.addInvite();
        }
        $scope.sendComm = function () {
            methods = '';
            count = 0;
            // foreach is used for Data Structure
            angular.forEach($scope.choice, function (value, key) {
                if (value.id != undefined) {
                    if (count)
                        methods += ',';
                    methods += value.id;
                    count++;
                }
            });
            if (count > 0) {
                window.location.href = "/queue_messages/create#/?person_method_id=" + methods;
            }
        }
        $scope.getData = function () {
            infoChiSpinner.show();
            $scope.event = {};
            $http.get('/events/json_edit/' + $location.search().event_id).then(function (data) {
                $scope.event = data.data;
                $scope.modal = {
                    "title": "Edit Event Details"
                };
                infoChiSpinner.hide('mainSpinner');
            });
        };
        // Set Method Buttons - Loop through all contacts
        $scope.setMethod = function (type_id) {
            for (i = 0; i < $scope.invites.length; i++) {
                if ($scope.runFilterResponse($scope.invites[i]) && $scope.runFilterGender($scope.invites[i])) {
                    if (type_id) {
                        for (j = 0; j < $scope.invites[i].person_methods.length; j++) {
                            if ($scope.invites[i].person_methods[j].person_method_type_id == type_id) {
                                $scope.choice[$scope.invites[i].person_methods[j].person_id] = $scope.invites[i].person_methods[j];
                            }
                        }
                    } else {
                        $scope.choice[$scope.invites[i].person_id] = $scope.no_method;
                        // Set NO Contact
                    }
                }
            }
        };

        $scope.getPeople = function () {
            infoChiSpinner.show();
            if ($scope.new_invite.event.Type) {
                var valType = $scope.new_invite.event.Type;
            } else {
                var valType = 'EventAttendance';
            }
            var valNum = $scope.new_invite.event.id;
            $http.get('/events/json_people/' + $location.search().event_id + '/EventAttendance/' + valNum + '/' + valType).then(function (data) {
                // Sort data.data
                people = $filter('orderBy')(data.data, 'fname', false);
                people = $filter('orderBy')(people, 'sname', false);
                if (people != undefined) {
                    people.unshift($scope.no_person);
                } else {
                    people = [$scope.no_person];
                }
                people.push($scope.person_function);
                $scope.people = people;
                infoChiSpinner.hide('mainSpinner');
            });
        };
        $scope.getResponses = function () {
            infoChiSpinner.show();
            $http.get('/events/json_statuses/' + $location.search().event_id + '/EventAttendanceResponses').then(function (data) {
                $scope.responses = data.data;
                infoChiSpinner.hide('mainSpinner');
            });
        };
        $scope.addInvite = function () {
            event_id = $location.search().event_id;
            person = $scope.new_invite.person;
            response = $scope.new_invite.response;
            if (person.id != undefined && response != undefined && response.id != undefined) {
                switch (person.id) {
                    case - 1:
                        // Add all remaining people
                        for (var i = 0; i < $scope.people.length; i++) {
                            if ($scope.people[i].id > 0) {
                                $scope.addItems++;
                                data = {
                                    "person_id": $scope.people[i].id,
                                    "event_attendance_response_id": response.id,
                                    "event_id": event_id
                                };
                                infoChiSpinner.show();
                                $http.post('/events/json_add_attendance', JSON.stringify(data)).then(function (data) {
                                    // People are filtered based on attendance/invites that are shown so no reload is necessary
                                    $scope.addItems--;
                                    $scope.getInvites();
                                    infoChiSpinner.hide();
                                });
                            }
                        }
                        $scope.new_invite.person = $scope.no_person;
                        break;
                    default:
                        // Add of a single person
                        $scope.addItems = 0;
                        data = {
                            "person_id": person.id,
                            "event_attendance_response_id": response.id,
                            "event_id": event_id
                        };
                        infoChiSpinner.show();
                        $http.post('/events/json_add_attendance', JSON.stringify(data)).then(function (data) {
                            // People are filtered based on attendance/invites that are shown so no reload is necessary
                            $scope.getInvites();
                            $scope.new_invite.person = $scope.no_person;
                            infoChiSpinner.hide('mainSpinner');
                        });
                        break;
                }
                // Just Reset it to the beginning
                $scope.new_invite.person = $scope.no_person;
            } else {
                // Nothing to Do
            }
        }
        $scope.updResponse = function (invite_id, response_id) {
            infoChiSpinner.show('mainSpinner');
            data = {
                "event_attendance_response_id": response_id,
                "id": invite_id
            }
            $http.post('/events/json_update/EventAttendance', JSON.stringify(data)).then(function (data) {
                $scope.getInvites();
                infoChiSpinner.hide('mainSpinner');
            });
        }

        $scope.no_event = {"id": 0, "label": "All People"};
        $scope.new_invite.event = $scope.no_event;
        $scope.getEvents = function () {
            $http.get('/events/json_events/' + $location.search().event_id).then(function (data) {
                $scope.events = data.data;
                $scope.events.unshift($scope.no_event);
                infoChiSpinner.hide('mainSpinner');
            });
        }

        // Filter though people if in the same group
        $scope.filterPeople = function (item) {
            // Logic is to avoid items already in the list.
            for (var i = 0; i < $scope.invites.length; i++) {
                if (item.id == $scope.invites[i].person_id)
                    return false;
            }
            return true;
        }
        $scope.removeItem = function (attendance) {
            infoChiSpinner.show();
            data = {
                "id": attendance.id
            };
            $http.post('/event_attendances/json_remove', JSON.stringify(data)).then(function (data) {
                $scope.getInvites();
                infoChiSpinner.hide('mainSpinner');
            });
        };

        // Run functions to get things started.
        $scope.addItems = 0;
        $scope.getData();
        $scope.getInvites();
        $scope.getPeople();
        $scope.getResponses();
        $scope.getEvents();

        // Modal Stuff
        $scope.open = function (size) {

            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/template/editEvent.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                resolve: {
                    modal: function () {
                        return $scope.modal;
                    },
                    data: function () {
                        return $scope.event;
                    }
                }
            });

            modalInstance.result.then(function (data) {
                $http.post('/events/json_edit/' + $location.search().event_id, JSON.stringify(data)).then(function (data) {
                    $scope.event = data.data;
                });
                infoChiSpinner.hide('mainSpinner');
            }, function () {

                infoChiSpinner.hide('mainSpinner');
            });
        };
    }]);

// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

infoChi.controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, modal, data) {
    $scope.modal = modal;
    $scope.data = data;

    $scope.ok = function () {
        $uibModalInstance.close($scope.data);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
