infoChi.controller('eventsIndexController', ['$scope', '$http', '$log', 
    '$uibModal', 'infoChiSpinner',
    function ($scope, $http, $log, $uibModal, infoChiSpinner) {
        $scope.deleteItem = function (Event) {
            if (confirm("Are You Sure you wish to remove event '" + Event.name + "'?")) {
                infoChiSpinner.show();
                data = {
                    'id': Event.id
                };
                $http.post('/web/events/delete', JSON.stringify(data))
                        .then(function (response) {
                            getEvents();
                            infoChiSpinner.hide();
                        }, function (response) {
                            infoChiSpinner.hide();
                        });
            }
        };
        getEvents = function () {
            infoChiSpinner.show();
            $http.get('/web/events').then(function (data) {
                $scope.events = data.data;
                infoChiSpinner.hide();
            });
        }
        // --

        $scope.pageSize = 15;
        getEvents();
    }
]);
