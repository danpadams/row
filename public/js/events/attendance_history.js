infoChi.controller('eventsAttendanceHistoryController', ['$scope', '$http', '$log',
    '$routeParams', '$uibModal', '$filter', 'infoChiSpinner',
    function ($scope, $http, $log, $routeParams, $uibModal, $filter, infoChiSpinner) {
        $scope.getInvites = function () {
            if ($scope.addItems == 0) {
                infoChiSpinner.show();
                $http.get('/events/json_attendance/' + $routeParams.event_id).then(function (response) {
                    for (i = 0; i < response.data.length; i++) {
                        if (response.data[i].person_methods) {
                            response.data[i].person_methods.unshift($scope.no_method);
                        } else {
                            response.data[i].person_methods = [$scope.no_method];
                        }
                    }
                    people = $filter('orderBy')(response.data, 'fname', false);
                    people = $filter('orderBy')(people, 'sname', false);

                    $scope.invites = response.data;

                    // Set Response Values
                    if ($scope.responses) {
                        proInvites();
                    } else {
                        $scope.$watch('responses', function () {
                            proInvites();
                        });
                    }
                    proHistory();
                    infoChiSpinner.hide();
                });
            }
        };
        proHistory = function () {
            for (i = 0; i < $scope.invites.length; i++) {
                if (true/*$scope.invites[i].person_id == 144*/) {
                    infoChiSpinner.show();
                    $http.get('/event_attendances/json_history/' + $routeParams.event_id + '/' + $scope.invites[i].person_id + '/' + i).then(function (response) {
                        i = response.data.General.location;
                        $scope.invites[i].history = response.data.History;
                        infoChiSpinner.hide();
                    });
                }
            }
        };
        $scope.proInvites = function () {
            for (i = 0; i < $scope.invites.length; i++) {
                value = $scope.invites[i];
                $scope.invites[i].person = [];
                $scope.invites[i].person[value.person_id] = {
                    "id": value.response_id,
                    "response_name": value.response_name
                };
            }
        };
        proInvites = function () {
            $scope.proInvites();
        };
        $scope.setFilter = function (type, value_id) {
            $scope.filter[type] = value_id;
        };
        $scope.runFilterNumRecords = function (item) {
            if (item.id == $scope.filter.numRecords) {
                return true;
            }
            return false;
        };
        $scope.runFilterNumPercent = function (item) {
            if (item.history) {
                if (item.history[$scope.filter.numRecords - 1]) {
                    if (item.history[$scope.filter.numRecords - 1].value <= $scope.filter.numPercent) {
                        return true;
                    }
                }
            }
            return false;
        };
        $scope.sendComm = function () {
            methods = '';
            count = 0;
            // foreach is used for Data Structure
            angular.forEach($scope.choice, function (value, key) {
                if (value.id != undefined) {
                    if (count)
                        methods += ',';
                    methods += value.id;
                    count++;
                }
            });
            if (count > 0) {
                window.location.href = "/queue_messages/create#/?person_method_id=" + methods;
            }
        };
        $scope.getData = function () {
            infoChiSpinner.show();
            $scope.event = {};
            $http.get('/web/events/view/' + $scope.event_id).then(function (data) {
                $scope.event = data.data;
                $scope.modal = {
                    "title": "Edit Event Details"
                }
                infoChiSpinner.hide('mainSpinner');
            });
        };
        $scope.setMethod = function (type_id) {
            for (i = 0; i < $scope.invites.length; i++) {
                value = $scope.invites[i];
                if ($scope.runFilterNumPercent(value)) {
                    if (type_id) {
                        for (j = 0; j < value.person_methods.length; j++) {
                            svalue = value.person_methods[j];
                            if (svalue.person_method_type_id == type_id) {
                                $scope.choice[svalue.person_id] = svalue;
                            }
                        }
                    } else {
                        $scope.choice[value.person_id] = $scope.no_method;
                        // Set NO Contact
                    }
                }
            }
        };

        $scope.getPeople = function () {
            infoChiSpinner.show();
//            if ($scope.new_invite.event.Type) {
//                var valType = $scope.new_invite.event.Type;
//            } else {
            var valType = 'EventAttendance';
//            }
            var valNum = 0;
            ////$scope.new_invite.event.id;
            $http.get('/events/json_people/' + $routeParams.event_id + '/EventAttendance/' + valNum + '/' + valType).then(function (data) {
                // Sort data.data
                people = $filter('orderBy')(data.data, 'fname', false);
                people = $filter('orderBy')(people, 'sname', false);
                if (people != undefined) {
                    people.unshift($scope.no_person);
                } else {
                    people = [$scope.no_person];
                }
                people.push($scope.person_function);
                $scope.people = people;
                infoChiSpinner.hide('mainSpinner');
            });
        };

        $scope.no_event = {"id": 0, "label": "All People"};
//        $scope.new_invite.event = $scope.no_event;
        $scope.getEvents = function () {
            infoChiSpinner.show();
            $http.get('/web/events/groups').then(function (data) {
                $scope.events = data.data;
                $scope.events.unshift($scope.no_event);
                infoChiSpinner.hide();
            });
        };

        // Filter though people if in the same group
        $scope.filterPeople = function (item) {
            // Logic is to avoid items already in the list.
            for (var i = 0; i < $scope.invites.length; i++) {
                if (item.id == $scope.invites[i].person_id)
                    return false;
            }
            return true;
        };

        startData = function () {
            $scope.filter = {numRecords: 4, numPercent: 1};
            $scope.addItems = 0;
            $scope.event_id = $routeParams.event_id;
            $scope.choice = [];
            $scope.pageSize = 15;
            $scope.no_method = {"id": undefined, "label": "Choose Contact Method"};
            $scope.invites = [];
        };
        // --
        // Run functions to get things started.
        startData();
        $scope.getData();
        $scope.getInvites();
        $scope.getPeople();
        $scope.getEvents();

        // Modal Stuff
        $scope.open = function (size) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/template/editEvent.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                resolve: {
                    modal: function () {
                        return $scope.modal;
                    },
                    data: function () {
                        return $scope.event;
                    }
                }
            });

            modalInstance.result.then(function (data) {
                $http.post('/events/json_edit/' + $routeParams.event_id, JSON.stringify(data)).then(function (data) {
                    $scope.event = data.data;
                });
                infoChiSpinner.hide('mainSpinner');
            }, function () {

                infoChiSpinner.hide('mainSpinner');
            });
        };
    }]);

// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

infoChi.controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, modal, data) {
    $scope.modal = modal;
    $scope.data = data;

    $scope.ok = function () {
        $uibModalInstance.close($scope.data);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };
});
