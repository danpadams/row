infoChi.controller('eventsCreateController', ['$scope', '$http', '$log',
    '$uibModal', 'infoChiSpinner',
    function ($scope, $http, $log, $uibModal, infoChiSpinner) {
        getBlank = function () {
            return data = {
                id: 0,
                name: '',
                startdate: '',
                msg_general: ''
            };
        };
        getOptions = function ($section) {
            if ($section == 'next_page') {
                return  [{
                        id: 1,
                        name: 'Invite'
                    }, {
                        id: 2,
                        name: 'Attendance'
                    }];
            }
        };
        setBlank = function () {
            $scope.Data = getBlank();
        };
        setOptions = function () {
            $scope.Options = {};
            $scope.Options.next_page = getOptions('next_page');
            for (var i = 0; i < $scope.Options.next_page.length; i++) {
                if ($scope.Options.next_page[i].id == 1) {
                    $scope.selOpt('next_page', $scope.Options.next_page[i]);
                    $scope.selData.next_page = $scope.Options.next_page[i];
                }
            }
            // Get Numbers for Message
        };
        $scope.selOpt = function (spot, item) {
            $scope.Data[spot] = item.id;
        }

        $scope.saveData = function () {
            data = $scope.Data;
            $http.post('/web/events/update', data).then(function (response) {
                switch ($scope.Data.next_page) {
                    case 1:
                        window.location.href = "#/events/" + response.data.id + "/invite";
                        break;
                    case 2:
                        window.location.href = "#/events/" + response.data.id + "/attendance";
                        break;
                }
            });
        }
        // --

        $scope.selData = {};
        setBlank();
        setOptions();
    }
]);
