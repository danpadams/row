infoChi.controller('clientsEditCtrl', ['$scope', '$http', 'infoChiSpinner', '$routeParams', 'infoChiList',
    function ($scope, $http, infoChiSpinner, $routeParams, infoChiList) {
        $scope.$on('getData', function () {
            getData();
        });
        getData = function () {
            infoChiSpinner.show();
            client_id = $scope.$parent.Current.Client.id;
            $http.get('/web/clients/' + client_id + '/view').then(function (response) {
                setData(response);
                console.log(response.data);
                infoChiSpinner.hide();
            });
        };
        getFeatures = function () {
            infoChiSpinner.show();
            $http.get('/web/lists/features').then(function (response) {
                console.log(response.data);
                $scope.newFeatures = response.data;
                infoChiSpinner.hide();
            });
        };
        $scope.removeFeature = function (id) {
            infoChiSpinner.show();
            data = {
                "id": id,
                "client_id": $scope.$parent.Current.Client.id
            }
            $http.post('/web/clients/remove_feature', data).then(function (response) {
                setData(response);
                getFeatures();
                infoChiSpinner.hide();
            })
        }
        $scope.defaultFeatures = function (id) {
            infoChiSpinner.show();
            data = {};
            $http.post('/web/clients/give_default_features', data).then(function (response) {
                getData();
                infoChiSpinner.hide();
            })
        }
        setData = function (response) {
            $scope.data = response.data.Client;
            $scope.features = response.data.Features;

        }
        $scope.addFeature = function (item) {
            infoChiSpinner.show();
            data = {"feature_id": item.id};
            $http.post('/web/clients/add_feature', data).then(function (response) {
                getData();
                getFeatures();
                infoChiSpinner.hide();
            })

        }
        // --

        // call getData through Parent Function
        $scope.valYesNo = [{"label":"No"},{"label":"Yes"}];
        $scope.$parent.getCurrent('getData')
        getFeatures();
    }
]);

