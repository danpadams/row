infoChi.controller('clientsIndexCtrl', ['$scope', '$http', 'infoChiSpinner',
    function ($scope, $http, infoChiSpinner) {
        // Get Admin Level
        $scope.$on('getLevels', function () {
            getLevels();
        });
        getLevels = function () {
            $scope.user_admin = false;
            if ($scope.$parent.Current.User.role == "admin") {
                $scope.user_admin = true;
            }
        }

        $scope.getClients = function () {
            infoChiSpinner.show();
            $http.get('/web/clients').then(function (response) {
                $scope.clients = response.data;
                infoChiSpinner.hide();
            });
        };
        $scope.goSwitch = function (id) {
            $scope.$parent.changeClient(id);
        };
        $scope.goManage = function (id) {
            infoChiSpinner.show();
            $http.get('/web/client/change/' + id).then(function (response) {
                console.log('Client Changed');
//                $scope.$parent.getCurrent();
                window.location.href = "#/clients/edit/";
                infoChiSpinner.hide();
            });
        };

        $scope.pageSize = 15;
        $scope.predicate = 'name';
        $scope.getClients();
        $scope.$parent.getCurrent('getLevels')
    }
]);
