infoChi.controller('outgoingMessagesController', ['$scope', '$http', 'infoChiSpinner',
    function ($scope, $http, infoChiSpinner) {
        infoChiSpinner.show();
        $http.get('/web/outgoing_messages').then(function (data) {
            $scope.messages = data.data;
            infoChiSpinner.hide();
        });
    }
]);
