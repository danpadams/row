infoChi.controller('outgoingMessagesViewController', ['$scope', '$http', '$routeParams',
    'infoChiSpinner', 'infoChiFlash',
    function ($scope, $http, $routeParams, infoChiSpinner, infoChiFlash) {
        getInfo = function () {
            if (outgoing_message_id == 0) {
                setBlank();
            } else {
                // Retreive the data for the Short
                infoChiSpinner.show();
                $http.get('/web/outgoing_messages/' + outgoing_message_id).then(function (response) {
                    $scope.Data = response.data;
                    setShortLink();
                    infoChiSpinner.hide();
                });
            }
        };
        $scope.saveData = function () {
            data = $scope.Data;
            infoChiSpinner.show();
            $http.post('/web/outgoing_messages/save', data).then(function (response) {
                infoChiFlash.show(response.data.message, 'success');
                $scope.Data = response.data;
                setShortLink();
                infoChiSpinner.hide();
            });
        };
        $scope.saveDelete = function () {
            data = {id: $scope.Data.id};
            infoChiSpinner.show();
            $http.post('/web/outgoing_messages/delete', data).then(function (response) {
                infoChiFlash.show(response.data.message, 'success');
                window.location.href = "#/outgoing_messages";
                infoChiSpinner.hide();
            });

        }
        getShorts = function () {
            infoChiSpinner.show();
            $http.get('/web/shorts/0').then(function (response) {
                $scope.Shorts = response.data;
                $scope.Shorts.unshift($scope.emptyShort);
                infoChiSpinner.hide();
            });

        };
        $scope.chooseShort = function (item) {
            $scope.Data.short_id = item.id;
        };

        $scope.getAction = function () {
            if ($scope.Data) {
                if ($scope.Data.id == 0) {
                    return 'Create';
                } else {
                    return 'View / Edit';
                }
            }
        }
        getBlank = function () {
            return data = {
                "id": 0,
                "title": '',
                "body": '',
            }
        };
        setBlank = function () {
            $scope.Data = getBlank();
        };
        setShortLink = function () {
            if ($scope.Data.short_id > 0) {
                $scope.Data.Short = $scope.Data.short;
            } else {
                $scope.Data.Short = $scope.emptyShort;
            }
        };
        // --

        outgoing_message_id = $routeParams.outgoing_message_id;
        $scope.emptyShort = {id: 0, name: 'Choose Optional Short URL'};
        $scope.selected = {};
        getInfo();
        getShorts();
    }
]);
