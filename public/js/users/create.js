infoChi.controller('usersCreateCtrl', ['$scope', '$http', '$routeParams', 'infoChiSpinner', 'infoChiArray', 'infoChiFlash',
    function ($scope, $http, $routeParams, infoChiSpinner, infoChiArray, infoChiFlash) {
        console.log('usersCreateCtrl');
        $scope.submit = function () {
            console.log($scope.data);
            infoChiSpinner.show();
            $http.post('/web/users/create', $scope.data).then(function (response) {

                if (response.data.client_id) {
                    $scope.data = {};
                } else {
                    msg = response.data.msg;
                    console.log(msg);
                    alert(msg);
                }
                infoChiSpinner.hide();
            });
        };

        $scope.data = {contact: {}};
    }
]);
