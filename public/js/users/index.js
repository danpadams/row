infoChi.controller('usersIndexCtrl', ['$scope', '$http', 'infoChiSpinner', 'infoChiList', '$localStorage', '$routeParams',
    function ($scope, $http, infoChiSpinner, infoChiList, $localStorage, $routeParams) {
        $scope.getData = function () {
            getData();
        };
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/users').then(function (response) {
                $scope.data = response.data;
                // console.log('getData()');
                // console.log(response.data);
                setClients();
                infoChiSpinner.hide();
            });
        };
        setClients = function () {
            $scope.Clients = [];
            $scope.ClientIds = [];
            for (var i = 0; i < $scope.data.length; i++) {
                for (var j = 0; j < $scope.data[i].Clients.length; j++) {
                    client = $scope.data[i].Clients[j];
                    if ($scope.ClientIds.indexOf(client.id) == -1) {
                        $scope.Clients.push({'id': client.id, 'name': client.name});
                        $scope.ClientIds.push(client.id);
                    }
                }
            }
        };
        $scope.setFilter = function (type, value_id) {
            if (type == 'client_id') {
                $scope.filter_id = value_id;
                $scope.$storage.usersClientId = value_id;
                $scope.client_id = value_id;
            }
        };
        $scope.runFilter = function (item) {
            // $scope.Filter.client_id
            if ($scope.$storage.usersClientId == 0) {
                return true;
            }
            // retval = infoChiList.exists($scope.Filter.client_id, item.Clients, 'id');
            retval = infoChiList.exists($scope.$storage.usersClientId, item.Clients, 'id');
            return retval;
        };
        // ==

        $scope.$storage = $localStorage.$default({
            usersClientId: 0
        });
        $scope.Filter = {};
        client_id = $routeParams.client_id;
        if ($routeParams.client_id == undefined) {
            // client_id = $scope.$parent.Current.Client.id;
            $scope.$parent.getCurrent('setClientID')
        }
        getData();
        $scope.setFilter('client_id', 0);

        $scope.$on('setClientID', function () {
            setClientID();
        });
        setClientID = function () {
            client_id = $scope.$parent.Current.Client.id;
            $scope.setFilter('client_id', client_id);
        }
        $scope.deleteFromClient = function (id) {
            console.log('deleteFromClient(' + id + ')');
            data = {
                'id': id
            };
            infoChiSpinner.show();
            $http.post('/web/users/delete_from_client', data).then(function (response) {
                console.log(response.data);
                getData();
                infoChiSpinner.hide();
            });

        }
        $scope.addClient = function (user_id, item) {
            console.log('addClient(user_id: ' + user_id + ', item.id: ' + item.id + ')');
            data = {
                'user_id': user_id,
                'client_id': item.id
            };
            infoChiSpinner.show();
            $http.post('/web/users/add_to_client', data).then(function (response) {
                console.log(response.data);
                getData();
                infoChiSpinner.hide();
            });

        }
    }
]);
