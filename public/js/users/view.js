infoChi.controller('usersViewCtrl', ['$scope', '$http', '$routeParams', 'infoChiSpinner', 'infoChiArray', 'infoChiFlash', '$filter',
    function ($scope, $http, $routeParams, infoChiSpinner, infoChiArray, infoChiFlash, $filter) {
        // -- Level 1 
        getInfo = function () {
            // Retreive the data
            infoChiSpinner.show();
            $http.get('/web/users/' + dat_id).then(function (response) {
                $scope.Data = response.data;
                getClients();
                getPeople();
                getYesNo();
//                console.log($scope.Data);
                infoChiSpinner.hide();
            });
        };
        $scope.updateData = function () {
            data = {
                'id': $scope.Data.id,
                'primary_client_id': $scope.Data.primary_client_id,
                'person_id': $scope.Data.person_id,
                'email_name': $scope.Data.email_name,
                'email_primary': $scope.Data.email_primary,
                'email_address': $scope.Data.email_address,
            };
            infoChiSpinner.show();
            $http.post('/web/users/update', data).then(function (response) {
                if (response.data.message) {
                    infoChiFlash.show(response.data.message, response.data.status, 5000, {container: 'updUser'});
                }
                infoChiSpinner.hide();
            });
        };
        $scope.updateChoice = function (type) {
            switch (type) {
                case 'primary_client_id':
                case 'person_id':
                case 'email_primary':
                    $scope.Data[type] = $scope.Selected[type].id;
                    break;
            }
        };
        getDisplay = function () {
            $scope.Choices = {};
            $scope.Choices.yesno = [
                {
                    id: 0,
                    label: 'No'
                }, {
                    id: 1,
                    label: 'Yes'
                }
            ];
        };
        getClients = function () {
            // Get Data and set Dropdown
            infoChiSpinner.show();
            $http.get('/web/users/clients/' + dat_id).then(function (response) {
                $scope.Choices.clients = response.data;
                $scope.Selected.primary_client_id = infoChiArray.setFromList($scope.Data.primary_client_id, $scope.Choices.clients);
//                console.log($scope.Choices.clients);
                infoChiSpinner.hide();
            });
        };
        getPeople = function () {
            // Get Data and set Dropdown
            infoChiSpinner.show();
            $http.get('/web/users/people/' + $scope.Data.primary_client_id).then(function (response) {
                $scope.Choices.people = response.data;
                $scope.Selected.person_id = infoChiArray.setFromList($scope.Data.person_id, $scope.Choices.people);
                infoChiSpinner.hide();
            });
        };
        getYesNo = function () {
            // Select email_primary using a local set of data
            $scope.Selected.email_primary = infoChiArray.setFromList($scope.Data.email_primary, $scope.Choices.yesno);

        };
        $scope.updatePw = function () {
            data = {
                "email": $scope.Data.username,
                "old_password": $scope.Data.passwd_current,
                "new_password": $scope.Data.passwd,
                "new_password2": $scope.Data.passwd2
            };
            $http.post('/web/users/upd_password', data).then(function (response) {
                if (response.data.message) {
                    infoChiFlash.show(response.data.message, response.data.status, 5000, {container: 'updPasswd'});
                }
            });
        };
        /**
         * Runs the Custom Email List Section
         */
        getCustomEmailList = function () {
            infoChiSpinner.show();
            $http.get('/web/custom_email_list/' + dat_id).then(function (response) {
                $scope.CustomEmailList = response.data.CustomEmailList;
                infoChiSpinner.hide();
            });
        }
        $scope.saveCustomEmailList = function () {
            infoChiSpinner.show();
            data = {
                "data": $scope.CustomEmailList
            };
            $http.post('/web/custom_email_list/save', data).then(function (response) {
                console.log(response);
                $scope.CustomEmailList = response.data.CustomEmailList;
                // Add Container Flash
                if (response.data.message) {
                    infoChiFlash.show(response.data.message, response.data.status, 5000, {container: 'updEmailList'});
                }

                infoChiSpinner.hide();
            });
        }
        $scope.moveUpCEL = function (id) {
            console.log('moveUpCEL()');
            result = getSortInfo($scope.CustomEmailList, id)

            if (result.index_id === 0) {
                console.log('Already at Top');
            } else {
                // Calculare new sort value
                new_sort = 0;
                if (result.sort_info.previous === 0) {
                    new_sort = result.sort_info.before / 2;
                } else {
                    new_sort = (result.sort_info.previous + result.sort_info.before) / 2;
                }

                // Put in place
                output_data = $scope.CustomEmailList
                output_data[result.index_id].sort = new_sort;
                output_data = $filter('orderBy')($scope.CustomEmailList, 'sort')
                $scope.CustomEmailList = nextTen(output_data);
            }
        }
        $scope.moveDownCEL = function (id) {
            console.log('moveDownCEL()');
            result = getSortInfo($scope.CustomEmailList, id)

            if (result.index_id === $scope.CustomEmailList.length - 1) {
                console.log('Already at Bottom');
            } else {
                // Calculate new sort value
                new_sort = 0;
                if (result.sort_info.after === 0) {
                    new_sort = result.sort_info.next + 10;
                } else {
                    new_sort = (result.sort_info.next + result.sort_info.after) / 2;
                }

                // Put in place
                output_data = $scope.CustomEmailList
                output_data[result.index_id].sort = new_sort;
                output_data = $filter('orderBy')($scope.CustomEmailList, 'sort')
                $scope.CustomEmailList = nextTen(output_data);
            }
        }
        getSortInfo = function (data, id) {
            index_id = -1;
            sort_info = {"before": 0, "previous": 0, "current": 0, "next": 0, "after": 0, current_found: 0};

            for (i = 0; i < data.length; i++) {
                // 1 - Previous
                if ((data[i].id !== id) && (sort_info.current_found === 0) && (sort_info.previous === 0) && (sort_info.before !== 0)) {
                    sort_info.previous = data[i].sort;
                }
                // 0 - Before
                if ((data[i].id !== id) && (sort_info.current_found === 0) && (sort_info.before === 0)) {
                    sort_info.before = data[i].sort;
                }
                // 4 - After
                if ((sort_info.current_found === 1) && (sort_info.next !== 0) && (sort_info.after === 0)) {
                    sort_info.after = data[i].sort;
                }
                // 3 - Next
                if ((sort_info.current_found === 1) && (sort_info.next === 0)) {
                    sort_info.next = data[i].sort;
                }
                // 2 - Current
                if (data[i].id === id) {
                    sort_info.current_found = 1;
                    sort_info.current = data[i].sort;
                    index_id = i;
                }
            }

            return {
                index_id: index_id,
                sort_info: sort_info
            };
        }
        nextTen = function (data) {
            for (i = 0; i < data.length; i++) {
                new_sort = (i + 1) * 10;
                data[i].sort = new_sort;
            }
            return data;
        }
        /**
         * Runs the Feature List Section
         */
        getFeaturesList = function () {
            infoChiSpinner.show();
            $http.get('/web/users/features_list').then(function (response) {
                $scope.newFeatures = response.data;
                infoChiSpinner.hide();
            });
        };
        getFeaturesUser = function () {
            infoChiSpinner.show();
            $scope.Features = [];
            $http.get('/web/users/features_user/' + dat_id).then(function (response) {
                $scope.Features = response.data;
                getFeatureIds();
                infoChiSpinner.hide();
            });
        }
        $scope.filterFeatureList = function (item) {
            if (FeatureIds.indexOf(item.id) != -1) {
                return false;
            }
            return true;
        };
        $scope.removeFeature = function (id) {
            infoChiSpinner.show();
            data = {
                "user_id": dat_id,
                "item_id": id
            }
            $http.post('/web/users/feature_remove', data).then(function (response) {
                $scope.Features = response.data;
                infoChiSpinner.hide();
            })
        }
        $scope.addFeature = function (item) {
            infoChiSpinner.show();
            data = {
                "user_id": dat_id,
                "feature_id": item.id
            };
            $http.post('/web/users/feature_add', data).then(function (response) {
                $scope.Features = response.data;
                infoChiSpinner.hide();
            })
        }
        getFeatureIds = function () {
            FeatureIds = [];
            // console.log($scope.Features);
            for (i = 0; i < $scope.Features.length; i++) {
                FeatureIds.push($scope.Features[i].feature_id);
            }
        }
        setupFeatures = function () {
            // $scope.showSectionFeatures = 0;
            $scope.$parent.getFeatures('setupFeatures_1');
            FeatureIds = [];
        }
        $scope.$on('setupFeatures_1', function () {
            $scope.$parent.getCurrent('setupFeatures_2');
        });
        $scope.$on('setupFeatures_2', function () {
            setupFeatures_2();
        });
        setupFeatures_2 = function () {
            console.log(dat_id);
            console.log($scope.$parent.Current);
            console.log($scope.$parent.Features);
            $scope.Features = [];
            if ($scope.$parent.Current.User.role === 'admin') {
                console.log('Access granted: admin');
                setupFeatures_3();
            } else if ((dat_id !== $scope.$parent.Current.User.id) &&
                ($scope.$parent.Features.indexOf(10) !== -1)) {
                // setupFeatures_3();
                console.log('access granted: user ');
                console.log($scope.Features);
            }
        }
        setupFeatures_3 = function () {
            console.log('setupFeature_3()');
            $scope.showSectionFeatures = 1;
            getCustomEmailList();
            getFeaturesList();
            getFeaturesUser();
        }

        // -- Level 1
        infoChiFlash.clear();
        $scope.Choices = {};
        // For Level 1
        setDefault = function () {
            dat_id = $routeParams.dat_id;
            $scope.Selected = {};
            $scope.Data = {};
        };
        // FeatureIds = [];
        setDefault();        // --
        getDisplay();
        getInfo();
        setupFeatures();
    }
]);
