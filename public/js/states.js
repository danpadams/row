infoChi.config(['$stateProvider', '$ocLazyLoad', function ($stateProvider, $ocLazyLoad) {

//        $urlRouterProvider.otherwise('/home');
//    $locationProvider.hashPrefix('!');

        $stateProvider.state('index', {
            url: '/',
            views: {
                "lazyLoadView": {
                    controller: 'defaultHomeCtrl', // This view will use AppCtrl loaded below in the resolve
                    templateUrl: '/html/home.html',
                    resolve: {// Any property in resolve should return a promise and is executed before the view is loaded
                        loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                                // you can lazy load files for an existing module
                                return $ocLazyLoad.load('/js/home.js');
                            }]
                    }
                }
            },
//
//        templateUrl: '/html/home.html',
//        controller: "defaultHomeCtrl"
        });
//    $ocLazyLoadProviderProvider.state('home', {
//        url: '/home',
//        templateUrl: '/html/home.html',
//        controller: "defaultHomeCtrl"
//    });
//    $ocLazyLoadProviderProvider.state('queue_messages.create', {
//        url: '/queue_messages/create',
//        templateUrl: "/html/queue_messages/create.html",
//        controller: "createQueueMessagesCtrl"
//    })
//
//    $ocLazyLoadProviderProvider.state('jamie', {
//        url: '/',
//        templateUrl: 'html/index.html',
//        controller: "defaultIndexCtrl"
//    });
    }]);
