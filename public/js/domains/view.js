infoChi.controller("domainsViewCtrl", ['$scope', '$http', 'infoChiSpinner', '$routeParams',
    function ($scope, $http, infoChiSpinner, $routeParams) {
        $scope.getData = function () {
            getData();
        };
        getData = function () {
            if (item_id == 0) {
                setBlank();
                pickCat();
            } else {
                infoChiSpinner.show();
                $http.get('/web/items/' + item_id).then(function (response) {
                    $scope.data = response.data;
                    pickCat();
                    infoChiSpinner.hide();
                });
            }
        };
        $scope.saveData = function () {
            data = $scope.data;
            infoChiSpinner.show();
            $http.post('/web/items/save', data).then(function (response) {
                $scope.data = response.data;
                pickCat();
                infoChiSpinner.hide();
            });
        };
        getBlank = function () {
            return data = {
                "category_id": 0,
                "id": 0,
                "mrrp": 0,
                "pv": 0,
                "name": ""
            }
        };
        setBlank = function () {
            $scope.data = getBlank();
        };
        // --
        getCats = function () {
            infoChiSpinner.show();
            $scope.categories = [];
            $http.get('/web/cats').then(function (response) {
                $scope.categories = response.data;
                infoChiSpinner.hide();
            });

        };
        $scope.setCat = function (item) {
            $scope.data.category_id = item.id;
        };
        pickCat = function () {
            // Wrap Calls to this in safty check to make sure the value exist.            
            if ($scope.data.category_id) {
                $scope.data.Category = $scope.data.category;
            } else {
                $scope.data.Category = emptyCat;
            }
        };
        emptyCat = {
            id: 0,
            name: "Choose Category"
        };
        // Detail --
        getDetail = function () {
            infoChiSpinner.show();
            $scope.detail = [];
            $http.get('/web/items/data/' + item_id).then(function (response) {
                $scope.detail = response.data;
                infoChiSpinner.hide();
//                console.log($scope.detail);
            });
        }
        // --
        item_id = $routeParams.item_id;
        $scope.pageSize = 10;
        getData();
        getCats();
        getDetail();
    }
]);