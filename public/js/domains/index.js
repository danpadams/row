infoChi.controller('domainsIndexCtrl', ['$scope', '$http', 'infoChiSpinner',
    function ($scope, $http, infoChiSpinner) {
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/domains/admin').then(function (response) {
                $scope.data = response.data;
                console.log($scope.data);
                infoChiSpinner.hide();
            });
        };
        // ==

        getData();
    }
]);
