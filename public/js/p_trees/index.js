infoChi.controller('pTreesIndexCtrl', ['$scope', '$http', '$localStorage',
    'infoChiSpinner',
    function ($scope, $http, $localStorage, infoChiSpinner) {
        $scope.itemClose = function (p_tree_id) {
            infoChiSpinner.show();
            data = {
                'id': p_tree_id
            };
            $http.post('/web/p_trees/change_state', data).then(function (response) {
                // Remove button for update
                getData();
                infoChiSpinner.hide();
            });
        };
        $scope.setDeletedList = function (deleted) {
            $scope.$storage.pTree_deleted = deleted;
            getData();
        };
        $scope.$storage = $localStorage.$default({
            pTree_deleted: 0
        });
        $scope.getStatusDeleted = function () {
            if ($scope.$storage.pTree_deleted) {
                return 'Deleted';
            } else {
                return 'Active';
            }
        }
        $scope.showChangeStatus = function (curClosed) {
            if (curClosed) {
                return 'Restore';
            } else {
                return 'Delete';
            }
        }
        

        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/p_trees/' + $scope.$storage.pTree_deleted).then(function (data) {
                $scope.pTrees = data.data;
                infoChiSpinner.hide();
            });
        };

        getData();
    }
]);