infoChi.controller('pTreesViewCtrl', ['$scope', '$http', '$routeParams', 'infoChiSpinner',
    function ($scope, $http, $routeParams, infoChiSpinner) {
        $scope.setType = function (type) {
            infoChiSpinner.show();
            // Create Branch of the desired type to use as the next branch in the tree
            var valUrl = '/web/p_branches/generate/' + type.id + '/p_tree/' + p_tree_id;
            $http.get(valUrl).then(function (response) {
                $scope.pBranch = {
                    id: response.data.id,
                    name: response.data.name
                };
                infoChiSpinner.hide();
            });
        }
        $scope.pType = {selected: {}};
        $scope.update = function () {
            // Update basic values for Tree
            infoChiSpinner.show();
            data = {
                'id': p_tree_id,
                'name': $scope.pTree.name
            };
            $http.post('/web/p_trees/update', data).then(function (response) {
                // Remove button for update
                $scope.pTree.old_name = response.data.name;
                infoChiSpinner.hide();
            });
        };

        // Sub-function to find name of first branch
        getName = function () {
            for (i = 0; i < $scope.pTree.p_branch.length; i++) {
                if ($scope.pTree.p_branch[i].id == $scope.pTree.p_branch_id) {
                    $scope.pBranch = $scope.pTree.p_branch[i];
                }
            }
        };
        getTypes = function () {
            // Get list of Kinds for First Module
            infoChiSpinner.show();
            $http.get('/web/p_types/kinds/' + $scope.pTree.p_kind_id).then(function (response) {
                $scope.pTypes = response.data;
                if ($scope.pTree.p_branch_id) {
                    for (i = 0; i < response.data.length; i++) {
                        if (response.data[i].id == $scope.pBranch.p_type_id) {
                            $scope.pType.selected = response.data[i];
                        }
                    }
                } else {
                    $scope.pType.selected = {name: "Select Type"};
                }
                infoChiSpinner.hide();
            });
        };
        // Initial Processing for create or update
        getData = function () {
            if (p_tree_id) {
                getExisting();
            } else {
                $scope.pTree = {
                    'name': 'To be named',
                    'id': 0,
                    'old_name': 'To be named.'
                };
                getTypes();
                $scope.labels.Type = 'Set';
            }
        };
        getExisting = function () {
            // Get Data for pTree
            infoChiSpinner.show();
            $http.get('/web/p_trees/view/' + p_tree_id).then(function (response) {
                response.data.old_name = response.data.name;
                $scope.pTree = response.data;
                // Set Label for setting of first branch
                if ($scope.pTree.p_branch_id == 0) {
                } else {
                    $scope.labels.Type = 'Change';
                    getName();
                }

                getTypes();
                infoChiSpinner.hide();
            });
        };
        // ---
        if ($routeParams.p_tree_id != undefined) {
            p_tree_id = $routeParams.p_tree_id;
        } else {
            p_tree_id = 0;
        }
        $scope.labels = {};
        $scope.pBranch = {};
        getData();
    }
]);
