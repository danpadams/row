infoChi.controller('logLoginsIndexCtrl', ['$scope', '$http', '$localStorage',
    'infoChiSpinner',
    function ($scope, $http, $localStorage, infoChiSpinner) {
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/log_logins/' + offset).then(function (response) {
                $scope.data = $scope.data.concat(response.data);
                infoChiSpinner.hide();
                offset = offset+response.data.length;
            });
        };
        $scope.getData = function () {
            getData();
        };
        $scope.filterData = function (item) {
            return true;
        };
        $scope.startOver = function () {
            offset = 0;
            $scope.data = [];
            getData();
        };

        // --
        $scope.data = [];
        $scope.pageSize = 10;
        offset = 0;
        getData();
    }
]);