infoChi.controller("defaultLayoutCtrl", ['$scope', '$http', '$localStorage', 'infoChiSpinner', 'infoChiFlash',
    function ($scope, $http, $localStorage, infoChiSpinner, infoChiFlash) {
        // add public variant
        $scope.getCurrent = function (eventName) {
            if ($scope.$storage.Current && $scope.$storage.Current.Client) {
                console.log('$scope.getCurrent(): Data Found')
                $scope.Current = $scope.$storage.Current
                if (eventName != undefined && eventName != '') {
                    $scope.$broadcast(eventName);
                }
            } else {
                console.log('$scope.getCurrent(): Running getCurrent()');
                getCurrent(eventName);
            }

            return $scope.Current;
        };
        getCurrent = function (eventName) {
            $scope.Current = [];
            infoChiSpinner.show();
            $http.get('/web/home/current/').then(function (response) {
                $scope.Current = response.data;
                $scope.$storage.Current = response.data
                if (eventName != undefined && eventName != '') {
                    $scope.$broadcast(eventName);
                }
                infoChiSpinner.hide();
            }, function myError(response) {
                window.location.href = "/login";
                infoChiSpinner.hide();
            });
        };

        // add public variant
        $scope.getFeatures = function (eventName) {
            if ($scope.$storage.Features && $scope.$storage.Features.length) {
                console.log('$scope.getFeatures(): Data Found')
                $scope.Features = $scope.$storage.Features
                if (eventName != undefined && eventName != '') {
                    $scope.$broadcast(eventName);
                }
            } else {
                console.log('$scope.getFeatures(): Running getFeatures()')
                getFeatures(eventName);
            }

            return $scope.Features;
        };
        getFeatures = function (eventName) {
            $scope.Current = [];
            infoChiSpinner.show();
            $http.get('/web/home/get_features/').then(function (response) {
                console.log('getFeatures() - Data Fetch');
                $scope.Features = response.data;
                $scope.$storage.Features = response.data;
                if (eventName != undefined && eventName != '') {
                    $scope.$broadcast(eventName);
                }
                infoChiSpinner.hide();
            }, function myError(response) {
                infoChiSpinner.hide();
            });
        };

        $scope.changeClient = function (id, eventName) {
            infoChiSpinner.show();
            if (eventName == undefined || eventName=='') {
                eventName = 'changedClient';
            }
            $http.get('/web/client/change/' + id).then(function (response) {
                infoChiFlash.show('Changed Client', 'success', 5000);
                getCurrent(eventName);
                getFeatures();
                infoChiSpinner.hide();
            });
        };

        $scope.proLogout = function () {
            infoChiSpinner.show();
            $scope.$storage = {};
            localStorage.clear()
            $http.post('/logout', {}).then(function (result) {
                window.location.href = '/';
            });
        };

        startLocalStorage = function () {
            $scope.$storage = $localStorage.$default({
                Current: {},
                Features: {}
            });
        }

        // --
        startLocalStorage();
    }]);
