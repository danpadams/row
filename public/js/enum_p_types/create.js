infoChi.controller('pTypesCreateCtrl', ['$scope', '$http', 'infoChiSpinner',
    function ($scope, $http, infoChiSpinner) {
        $scope.updateData = function () {
            // Create the date for the Property
            data = {
                'name': $scope.Data.name,
                'p_kind_id': $scope.Data.p_kind_id,
                'filename_view': $scope.Data.filename_view,
                'function_twiml': $scope.Data.function_twiml
            };
            console.log(data);
            infoChiSpinner.show();
            $http.post('/web/p_types/create', data).then(function (response) {
                console.log(response.data);
                window.location.href = "#/p_types/" + response.data.id;
                infoChiSpinner.hide();
            });
        };
        $scope.updateChoice = function (type) {
            if (type == 'kind') {
                $scope.Data.p_kind_id = $scope.Selected.kind.id;
            }
        };


        // --
        getKinds = function () {
            $scope.Choices = {};
            $scope.Choices.kind = [
                {
                    id: 1,
                    label: 'SMS'
                }, {
                    id: 2,
                    label: 'Voice'
                }
            ];
        };
        getKinds();
        noKind = {id: 0, label: 'Choose One'};
        $scope.Selected = {};
        $scope.Selected.kind = noKind;
    }
]);

