infoChi.controller('pTypesIndexCtrl', ['$scope', '$http', 'infoChiSpinner',
    function ($scope, $http, infoChiSpinner) {
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/p_types').then(function (response) {
                $scope.data = response.data;
                infoChiSpinner.hide();
            });
        };
        // ==

        getData();
    }
]);
