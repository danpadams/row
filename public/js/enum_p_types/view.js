infoChi.controller('pTypesViewCtrl', ['$scope', '$http', '$routeParams', 'infoChiSpinner', 'infoChiArray',
    function ($scope, $http, $routeParams, infoChiSpinner, infoChiArray) {
        getInfo = function () {
            // Retreive the data for the Short
            infoChiSpinner.show();
            $http.get('/web/p_types/' + dat_id).then(function (response) {
                $scope.Data = response.data;
                $scope.Selected.kind = infoChiArray.setFromList(response.data.p_kind_id, $scope.Choices.kind);
                infoChiSpinner.hide();
            });
        };
        $scope.updateData = function () {
            // Update the date for the Short
            data = {
                'id': $scope.Data.id,
                'name': $scope.Data.name,
                'p_kind_id': $scope.Data.p_kind_id,
                'filename_view': $scope.Data.filename_view,
                'function_twiml': $scope.Data.function_twiml,
                'show_next_module': $scope.Data.show_next_module,
                'active': $scope.Data.active
            };
            console.log(data);
            infoChiSpinner.show();
            $http.post('/web/p_types/update', data).then(function (response) {
                $scope.Data = response.data;
                infoChiSpinner.hide();
            });
        };
        $scope.updateChoice = function (type) {
            if (type == 'kind') {
                $scope.Data.p_kind_id = $scope.Selected.kind.id;
            }
        };

        // --
        getKinds = function () {
            $scope.Choices = {};
            $scope.Choices.kind = [
                {
                    id: 1,
                    label: 'SMS'
                }, {
                    id: 2,
                    label: 'Voice'
                }
            ];
        };

        setDefault = function () {
            dat_id = $routeParams.dat_id;
            $scope.Selected = {};
            $scope.Data = {};
        };
        setDefault();
        getKinds();
        getInfo();
    }
]);
