infoChi.controller("createQueueMessagesCtrl", ['$scope', '$http', '$routeParams', 'infoChiSpinner', 'infoChiTooltip', '$filter', 'fileUpload', 'fileService', 'infoChiFlash',
    function ($scope, $http, $routeParams, infoChiSpinner, infoChiTooltip, $filter, fileUpload, fileService, infoChiFlash) {        // Tooltips

        let getResponses = function () {
            if ($scope.event_id) {
                infoChiSpinner.show();
                $http.get('/web/invites/responses').then(function (response) {
                    $scope.responses = response.data;
                    infoChiSpinner.hide();
                });
            }
        }
        // Common Stuff
        $scope.tooltips = {};
        $scope.tooltip = function (id) {
            infoChiTooltip.tooltip(id).then(function (response) {
                $scope.tooltips[id] = response;
            });
        };
        // Get Item
//        $scope.tooltip(2);
//        $scope.tooltip(3);
//        $scope.tooltip(4);
        $scope.tooltip(6);
        $scope.tooltip(7);


        $scope.dropdownChoice = {};
        $scope.startDateBeforeRender = function ($dates) {
            const todaySinceMidnight = new Date(Date.now() - 86400000);
            todaySinceMidnight.setUTCHours(0, 0, 0, 0);
            $dates.filter(function (date) {
                return date.utcDateValue < todaySinceMidnight;
            }).forEach(function (date) {
                date.selectable = false;
            });
        };

        // --
        $scope.dropdownChoice.email = function (item) {
            $scope.data.from_email = item.email_address;
            $scope.data.from_name = item.email_name;
        };
        $scope.dropdownChoice.phone = function (item) {
            $scope.data.from_phone = item.number;
            // $('#OutgoingMessageFromPhone').val(item.number);
        };
        $scope.dropdownChoice.contact = function (item) {
            if ($scope.recipients != undefined && $scope.recipients.length) {
                for (let i = 0; i < $scope.recipients.length; i++) {
                    if (item.person_method_id == $scope.recipients[i].method_id) {
                        return false;
                    }
                }
            }
            let element = {
                "method_id": item.person_method_id,
                "method": item.method,
                "type_id": item.type_id,
                "user_id": item.user_id,
                "person_id": item.person_id,
                "label": item.label
            };
            $scope.recipients.push(element);

        };
        $scope.dropdownChoice.outgoing_message = function (item) {
            $scope.data.subject = item.title;
            $scope.data.body = item.body;
            if (item.short_id) {
                $scope.data.short_id = item.short.id;
                $scope.choice.selectedShort = item.short;
            }
        };
        $scope.dropdownChoice.short = function (item) {
            $scope.data.short_id = item.id;
        };

        $scope.remove = function (item) {
            $scope.recipients.splice(item, 1);
        };

        $scope.filterUsed = function (item) {
            if ($scope.recipients != undefined && $scope.recipients.length) {
                for (let i = 0; i < $scope.recipients.length; i++) {
                    if (item.person_method_id == $scope.recipients[i].method_id) {
                        return false;
                    }
                }
            }
            return true;
        };
        // Process "person_method_id"
        addByIds = function (person_method_ids) {
            let pm_ids = [];
            // Create person_method_ids as array
            if (Array.isArray(person_method_ids)) {
                pm_ids = person_method_ids;
            } else {
                pm_ids = person_method_ids.split(',');
            }
            // Make sure this array is of integers
            for (let i = 0; i < pm_ids.length; i++) {
                pm_ids[i] = parseInt(pm_ids[i], 10);
            }

            // Check list of contacts for "pm_id" and add to Chosen List
            for (let j = 0; j < $scope.contacts.length; j++) {
                if (pm_ids.indexOf($scope.contacts[j].person_method_id) != -1) {
                    $scope.dropdownChoice.contact($scope.contacts[j]);
                }
            }

            // Add Check for What client is the contact in
            if ($scope.recipients.length == 0) {
                infoChiSpinner.show();
                // Get info of new client and if change is needed
                $http.get('/web/person_method/check_client/' + pm_ids[0]).success(function (data) {
                    if (data.change_client) {
                        $scope.$parent.changeClient(data.client_id, 'initPage');
                    } else {
                        checkRecipients(person_method_ids)
                    }
                    infoChiSpinner.hide();
                });
            } else if ($scope.recipients < pm_ids.length) {
                checkRecipients(person_method_ids);
            }

        };
        /**
         * Method to determine the reason for not selecting a Recipient
         *
         * @param person_method_ids CSV List passed from URL
         */
        checkRecipients = function (person_method_ids) {
            infoChiSpinner.show();
            // Get info of new client and if change is needed
            $http.get('/web/person_method/check_recips/' + person_method_ids).success(function (data) {
                for (i = 0; data.messages && i < data.messages.length; i++) {
                    statusFlash = 'success';
                    if (data.messages[i].statusFlash) {
                        statusFlash = data.messages[i].statusFlash;
                    }
                    infoChiFlash.show(data.messages[i].message, statusFlash, 5000);
                }
                infoChiSpinner.hide();
            });
        }
        checkPMIds = function () {
            pm_ids = $routeParams.pm_ids;
            if (pm_ids) {
                addByIds(pm_ids);
            }
        }
        checkPreviousLog = function () {
            log_id = $routeParams.log_id;
            if (log_id) {
                infoChiSpinner.show();
                $http.get('/web/queue_message/resend/' + log_id).success(function (data) {
                    if (data.body) {
                        $scope.data.body = data.body;
                        $scope.data.subject = data.subject;
                        if (data.short_id) {
                            $scope.data.short_id = data.short_id;
                            $scope.choice.selectedShort = {'name': ''};
                            $scope.choice.selectedShort.name = data.short_name;
                        }
                        addByIds(data.method_ids);
                    }
                    infoChiSpinner.hide();
                });
            }
        }

        // Get lists of data
        $scope.choice = {};
        getListEmail = function () {
            infoChiSpinner.show();
            $http.get('/web/lists/email').success(function (data) {
                $scope.emails = data;
                $scope.dropdownChoice.email(data[0]);
                $scope.choice.selectedEmail = data[0];
                infoChiSpinner.hide();
            });
        };
        getListPhone = function () {
            infoChiSpinner.show();
            $http.get('/web/lists/phone').success(function (data) {
                $scope.numbers = data;
                if (data[0]) {
                    $scope.dropdownChoice.phone(data[0]);
                    $scope.choice.selectedPhone = data[0];
                }
                infoChiSpinner.hide();
            });
        };
        // Fill the contacts list
        getListContacts = function () {
            infoChiSpinner.show();
            $http.get('/web/lists/contact').success(function (data) {
                $scope.contacts = $filter('orderBy')(data, 'label', false);
                checkPMIds();
                checkPreviousLog();
                infoChiSpinner.hide();
            });
        };
        getListOutgoingMessages = function () {
            infoChiSpinner.show();
            $http.get('/web/lists/outgoing_message').success(function (data) {
                $scope.messages = data;
                infoChiSpinner.hide();
            });
        };
        getListShorts = function () {
            infoChiSpinner.show();
            $http.get('/web/lists/short').success(function (data) {
                $scope.shorts = data;
                $scope.shorts.unshift($scope.emptyShort);
                infoChiSpinner.hide();
            });
        };

        $scope.subData = function () {
            let data = $scope.data; // data to be updated locally
            data.recipients = $scope.recipients;
            data.fileData = $scope.fileData;
            if (data.recipients.length) {
                infoChiSpinner.show();
                $http.post('/web/queue_message/create', data).success(function (data) {
                    let message = 'The message you composed has been added to the queue!';
                    infoChiFlash.show(message, 'success', 5000);
                    refreshData();
                    $scope.data.dateDropDownInput = new Date();
                    $scope.data.body = '';
                    $scope.data.subject = '';
                    console.log($scope.hasAttachments);
                    infoChiSpinner.hide();
                });
            } else {
                console.log("Cannot Sumbit Message - No Recipients");
            }

        };

        // -- File Upload - Check Features
        startFileUpload = function () {
            $scope.hasAttachments = 0;
            $scope.$parent.getFeatures('create_getFeatures');              
        }
        $scope.$on('create_getFeatures', function () {
            create_getFeatures();
        });
        create_getFeatures = function () {
            if ($scope.$parent.Features.indexOf(13) > -1) {
                $scope.hasAttachments = 1;
            }
        }

        $scope.uploadFile = function () {
            file = fileService.pop();
            var uploadUrl = '/web/attachment/upload_file';
            infoChiSpinner.show();
            fileUpload.uploadFileToUrl(file, uploadUrl, afterUpload, afterUploadError);
        };
        // Callback function for success on fileUpload
        afterUpload = function (response) {
            infoChiSpinner.hide();
            $scope.fileData.push(response);
            console.log($scope.myFile);
            // $scope.myFile.reset();
            angular.forEach(
                angular.element("input[type='file']"),
                function (inputElem) {
                    angular.element(inputElem).val(null);
                });
            // What else do I do?
        }
        afterUploadError = function (response) {
            infoChiSpinner.hide();
            console.log('afterUpload_error');
        }
        $scope.remove_fileData = function (item) {
            $scope.fileData.splice(item, 1);
        };
        // File Upload & Management

        // ---
        refreshData = function () {
            $scope.emptyShort = {id: 0, name: 'Choose Optional Short URL'};
            $scope.selected = {};
            $scope.recipients = [];
            $scope.myFile = '';
            $scope.fileData = [];
        };

        initData = function () {
            refreshData();
            var setDate = moment();
            $scope.data = {
                "from_email": '',
                "from_name": '',
                "from_phone": '',
                "short_id": 0,
                "dateDropDownInput": setDate.format('YYYY/MM/DD h:mm:ss a'),
            };
        };

        // Listener Funtion
        $scope.$on('initPage', function () {
            initPage();
        });
        initPage = function () {
            initData();
            $scope.$parent.getCurrent();
            getListEmail();
            getListPhone();
            getListContacts();
            getListOutgoingMessages();
            getListShorts();
            startFileUpload();
        };

        $scope.event_id = $routeParams.event_id;
        if ($scope.event_id == undefined) {
            $scope.event_id = 0;
        } else {
            $scope.no_status = {"id": "0", "name": "Unknown"};
            getResponses();
        }

        // --
        initPage();
    }
]);