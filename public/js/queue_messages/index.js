infoChi.controller("queueMessagesIndexCtrl", ['$scope', '$http', '$routeParams',
    'infoChiSpinner', 'infoChiTooltip', '$filter', 'infoChiFlash',
    function ($scope, $http, $routeParams, infoChiSpinner, infoChiTooltip, $filter, infoChiFlash) {        // Tooltips
        $scope.getData = function () {
            console.log("getData");
            getData();
        }
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/queue_message/pending').then(function (response) {
                $scope.data = response.data;
                console.log($scope.data);
                infoChiSpinner.hide();
            });
        };


        $scope.getData();
    }
]);