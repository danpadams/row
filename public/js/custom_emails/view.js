infoChi.controller('customEmailsViewCtrl', ['$scope', '$http', 'infoChiSpinner', '$routeParams', 'infoChiList', 'infoChiFlash',
    function ($scope, $http, infoChiSpinner, $routeParams, infoChiList, infoChiFlash) {
        getData = function () {
            console.log('customEmailsViewCtrl:getData()')
            if (dat_id == 0) {
                // Creation of email
                console.log('Creation of email');
                $scope.Selected.domain = {domain: "Choose Domain Name"};
                $scope.data = {id: 0, Settings: {recipients: []}};
                // infoChiSpinner.show();
                $http.get('/web/domains').then(function (response) {
                    $scope.Domains = response.data;
                    console.log($scope.Domains);
                    // infoChiSpinner.hide();
                });
            } else {
                infoChiSpinner.show();
                // Retrieve email settings
                $scope.data = {};
                $http.get('/web/custom_emails/' + dat_id).then(function (response) {
                    $scope.data = response.data;
                    console.log($scope.data);
                    infoChiSpinner.hide();
                });
            }
        };

        // --

        $scope.updEmail = function () {
            data = {
                'id': $scope.data.id,
                'name': $scope.data.name,
                'username': $scope.data.username,
                'domain_id': $scope.data.domain_id
            };
            recipients = [];
            if (Array.isArray($scope.data.Settings.recipients)) {
                recipients = $scope.data.Settings.recipients;
            }
            data['settings'] = {
                'recipients': $scope.data.Settings.recipients
            };
            infoChiSpinner.show();
            $http.post('/web/custom_emails/save', data).success(function (response) {
                // Add New Data to Current
                if (dat_id == 0) {
                    window.location.href = '#/custom_emails/' + response.id;
                }
                infoChiFlash.show(response.message, 'success', 5000);
                $scope.data = response
                infoChiSpinner.hide();
            });
        };
        $scope.updateChoice = function (name) {
            console.log(name);
            console.log($scope.Selected[name])
            switch (name) {
                case 'domain':
                    $scope.data.domain = {};
                    $scope.data.domain = $scope.Selected[name];
                    $scope.data.domain_id = $scope.Selected[name].id;
                    break;
            }
            console.log($scope.data);
        }

        getRecipContacts = function () {
            infoChiSpinner.show();
            $http.get('/web/lists/contacts_email/').then(function (response) {
                $scope.recip_contacts = response.data;
                infoChiSpinner.hide();
            });
        };
        if ($scope.dropdownChoice == undefined) {
            $scope.dropdownChoice = {};
        }
        $scope.dropdownChoice.recip = function (item) {
            if ($scope.data.Settings == undefined) {
                // Start Fresh
                $scope.data.Settings = {};
            }
            if ($scope.data.Settings.recipients == undefined) {
                // Start Fresh
                $scope.data.Settings.recipients = [];
            }
            $scope.data.Settings.recipients.push(item);
        };
        if ($scope.customFilter == undefined) {
            $scope.customFilter = {};
        }
        $scope.customFilter.recip = function (item) {
            return infoChiList.filter(item.user_id, $scope.data.Settings.recipients, 'user_id')
        };
        if ($scope.remove == undefined) {
            $scope.remove = {};
        }
        $scope.remove.recip = function (key) {
            $scope.data.Settings.recipients.splice(key, 1);
        };

        dat_id = $routeParams.dat_id;
        $scope.Selected = {};
        getData();
        getRecipContacts();
    }
]);
