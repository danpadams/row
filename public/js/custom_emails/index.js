infoChi.controller('customEmailsIndexCtrl', ['$scope', '$http', 'infoChiSpinner',
    function ($scope, $http, infoChiSpinner) {
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/custom_emails').then(function (response) {
                $scope.data = response.data;
                console.log($scope.data);
                infoChiSpinner.hide();
            });
        };
        $scope.markDeleted = function (id) {
            console.log(id);
            data = {
                'id': id
            };
            infoChiSpinner.show();
            $http.post('/web/custom_emails/delete', data).success(function (response) {
                getData();
                infoChiSpinner.hide();
            });
        }
        // ==

        getData();
    }
]);
