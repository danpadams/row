infoChi.controller('personGroupIndexController', ['$scope', '$http',
    'infoChiSpinner',
    function ($scope, $http, infoChiSpinner) {
        $scope.deleteItem = function (PersonGroup) {
            if (confirm("Are You Sure you wish to remove group '" + PersonGroup.name + "'?")) {
                data = {
                    'id': PersonGroup.id
                };
                infoChiSpinner.show();
                $http.post('/web/person_groups/delete', data).then(function (response) {
                    getGroups();
                    infoChiSpinner.hide();
                }, function (response) {
                    infoChiSpinner.hide();
                });
            }
        };
        getGroups = function () {
            infoChiSpinner.show();
            $http.get('/web/person_groups').then(function (data) {
                $scope.groups = data.data;
                infoChiSpinner.hide();
            });
        };
        // --

        $scope.pageSize = 10;
        getGroups();
    }
]);
