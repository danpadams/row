infoChi.controller('personGroupCreateController', ['$scope', '$http', 'infoChiSpinner',
    function ($scope, $http, infoChiSpinner) {
        $scope.submit = function () {
            data = {
                "name": $scope.data.name
            };
            infoChiSpinner.show();
            $http.post('/web/person_groups/create', data).then(function (response) {
                window.location.href = "#/person_groups/view/" + response.data.id;
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
    }
]);
