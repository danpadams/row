infoChi.controller('personGroupViewController', ['$scope', '$http', '$routeParams', 'infoChiSpinner', 'infoChiList',
    function ($scope, $http, $routeParams, infoChiSpinner, infoChiList) {
        $scope.pageSize = 15;
        $scope.no_method = {"id": undefined, "label": "Choose Contact Method"};
        getGroup = function () {
            infoChiSpinner.show();
            $http.get('/web/person_groups/view/' + person_group_id).success(function (data) {
                $scope.summary = data;
                infoChiSpinner.hide();
            });
        };
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/person_groups/detail/' + person_group_id).then(function (response) {
                for (key = 0; key < response.data.length; key++) {
                    if (response.data[key].person_method) {
                        response.data[key].person_method.unshift($scope.no_method);
                    } else {
                        response.data[key].person_method = [$scope.no_method];
                    }
                }
                ;
                $scope.detail = response.data;
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
        $scope.removeFromGroup = function (pgd_id) {
            data = {
                "pgd_id": pgd_id
            };
            infoChiSpinner.show();
            $http.post('/web/person_groups/remove_from_group', data)
                    .then(function (response) {
                        getData();
//                        getGroup(); // Bottom
                        infoChiSpinner.hide();
                    }, function (response) {
                        infoChiSpinner.hide();
                    });
        };
        $scope.addToGroup = function (item) {
            data = {
                "person_id": item.person_id,
                "person_group_id": person_group_id
            };
            infoChiSpinner.show();
            $http.post('/web/person_groups/add_to_group', data).then(function (response) {
                getData(); // Get the new list
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
        $scope.setMethod = function (type_id) {
            for (i = 0; i < $scope.detail.length; i++) {
                if (type_id != 0) {
                    for (j = 0; j < $scope.detail[i].person_method.length; j++) {
                        if ($scope.detail[i].person_method[j].person_method_type_id == type_id) {
                            $scope.choice[$scope.detail[i].person_method[j].person_id] = $scope.detail[i].person_method[j];
                        }
                    }
                } else {
                    $scope.choice[$scope.detail[i].id] = $scope.no_method;
                    // Set NO Contact
                }
            }
        };
        $scope.sendComm = function () {
            methods = '';
            count = 0
            // foreach is used for Data Structure
            angular.forEach($scope.choice, function (value, key) {
                if (value.id != undefined) {
                    if (count)
                        methods += ',';
                    methods += value.id;
                    count++;
                }
            });
            if (count > 0) {
                window.location.href = "#/queue_messages/create/pm_ids/" + methods
            }
        };
        $scope.filterPeople = function (item) {
            return infoChiList.filter(item.person_id, $scope.detail, 'id');
        };
        $scope.filterTest = function () {
            // If Variables are present, run generic test to determine size of list
            if ($scope.people && $scope.detail) {
                return infoChiList.filterAll($scope.people, 'person_id', $scope.detail, 'id');
            } else {
                return 0;
            }
        };

        getPeople = function () {
            // Get the list of people to be added to the list
            infoChiSpinner.show();
            $http.get('/web/person_groups/add_list/' + person_group_id).success(function (data) {
                $scope.people = data;
                infoChiSpinner.hide();
            });
        };
        // --

        person_group_id = $routeParams.person_group_id;
        $scope.choice = [];
        getData();
        getGroup();
        getPeople();
        $scope.testData = $scope.filterTest();
    }
]);
