infoChi.controller('messagesIndexController', ['$scope', '$http', '$localStorage',
    'infoChiTooltips', 'infoChiSpinner',
    function ($scope, $http, $localStorage, infoChiTooltips, infoChiSpinner) {
        $scope.refresh = function () {
            // Function setup to receive refresh button
            $scope.msgsGet();
        }
        $scope.msgsGet = function () {
            infoChiSpinner.show();
            $http.get('/web/messages/' + $scope.$storage.msgStatus + '/' + $scope.$storage.msgClient).success(
                function (data) {
                    $scope.messages = data;
                    $scope.setSort('message.created', true, 1);
                    console.log($scope.messages);
                    infoChiSpinner.hide();
                });
        };
        $scope.setStatus = function (status_id) {
            $scope.$storage.msgStatus = status_id;
            $scope.msgsGet();
        };
        $scope.setClient = function (client_id) {
            $scope.$storage.msgClient = client_id;
            console.log('Client: ' + client_id);
            $scope.msgsGet();
        };
        $scope.setSort = function (sort_key, sort_order, id) {
            if (sort_order == undefined) {
                sort_order = false;
            }
            $scope.sortKey = sort_key;
            $scope.sortOrder = sort_order
            $scope.sortId = id;
            console.log(sort_key);
            console.log(sort_order);
        }
        $scope.getUserClients = function () {
            $scope.myClients = [];
//            infoChiSpinner.show();
            $http.get('/web/home/list_clients').success(function (data) {
                $scope.myClients = data;
//                infoChiSpinner.hide();
            });
        };
        $scope.chkStatus = function (status_id) {
            if (status_id == $scope.$storage.msgStatus) {
                return true;
            }
            return false;
        };
        $scope.chkClient = function (client_id) {
            if (client_id == $scope.$storage.msgClient) {
                return true;
            }
            return false;
        };
        $scope.chkSort = function (id) {
            if (id == $scope.sortId) {
                return true;
            }
            return false;
        };

        $scope.msgClose = function (user_message_id) {
            infoChiSpinner.show();
            data = {
                "user_message_id": user_message_id
            };
            $http.post('/web/messages/msg_close', JSON.stringify(data)).then(function (data) {
                $scope.msgsGet();
                infoChiSpinner.hide();
            });
        };

        // --

        $scope.statuses = [{
            "id": 1,
            "label": "New"
        }, {
            "id": 3,
            "label": "Open"
        }];
        $scope.orderChoices = [{
            id: 1,
            "key": "message.created",
            "order": "true",
            "label": "Message: Newest First"
        }, {
            id: 2,
            "key": "message.created",
            "order": "false",
            "label": "Message: Oldest First"
        }]
        $scope.$storage = $localStorage.$default({
            msgStatus: 3,
            msgClient: 0
        });
        $scope.pageSize = 10;
        $scope.msgsGet();
        $scope.getUserClients();

        infoChiTooltips.tooltip(5);
    }
]);


