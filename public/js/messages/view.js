infoChi.controller('messagesViewController', ['$scope', '$http', '$routeParams',
    'infoChiTooltip', 'infoChiSpinner', 'infoChiArray', 'infoChiList',
    function ($scope, $http, $routeParams, infoChiTooltip, infoChiSpinner, infoChiArray, infoChiList) {
        getMessage = function () {
            infoChiSpinner.show();
            $http.get('/web/messages/' + message_id).then(function (response) {
                $scope.message = response.data;
                console.log($scope.message);
                if ($scope.message.message.person_id == 0) {
                    $scope.valContactCreate = 0;
                    contactLookup();

//                    $scope.no_person = {"id": 0, "label": "Choose Person"};
//                    getPeople();
//                } else {
//                    $scope.getInvites();
                }
//

                getUsersWithCopy();
                getUsersToShare();
                getAttachments();
                infoChiSpinner.hide();
            });
        };
        // Event Invites
        $scope.getInvites = function () {
            if ($scope.message.Message.person_id) {
                person_id = $scope.message.Message.person_id;
                //infoChiSpinner.show();
                $scope.pageSize = 5;
                $http.get('/messages/json_invites/' + person_id).then(function (data) {
                    $scope.invites = data.data;
                    $scope.getResponses();
                    //infoChiSpinner.hide();
                });
            }
        };
        $scope.getResponses = function () {
            //infoChiSpinner.show();
            // Possible for Session Variable
            $http.get('/events/json_statuses').then(function (data) {
                $scope.responses = data.data;
                //infoChiSpinner.hide();
            });
        };
        $scope.updResponse = function (invite_id, response_id) {
            //infoChiSpinner.show();
            data = {
                "response_id": response_id,
                "id": invite_id
            };
            $http.post('/invites/json_update', JSON.stringify(data)).then(function (data) {
                $scope.getInvites();
                //infoChiSpinner.hide('mainSpinner');
            });
        };
        // Event Invites
        $scope.msgClose = function () {
            infoChiSpinner.show();
            console.log('msgClose()');
            data = {
                "user_message_id": $scope.message.id
            };
            $http.post('/web/messages/msg_close', JSON.stringify(data)).then(function (data) {
                getMessage();
                infoChiSpinner.hide();
            });
        };
        $scope.msgMarkSpam = function () {
            infoChiSpinner.show();
            console.log('msgMarkSpam()');
            data = {
                "user_message_id": $scope.message.id
            };
            $http.post('/web/messages/msg_mark_spam', JSON.stringify(data)).then(function (data) {
                getMessage();
                infoChiSpinner.hide();
            });
        };
        $scope.msgStatus = function (status_id) {
            // infoChiSpinner.show();
            console.log($scope.message);
            data = {
                "user_message_id": $scope.message.id,
                "status_id": status_id
            };
            $http.post('/web/messages/msg_status', JSON.stringify(data)).then(function (data) {
                getMessage();
                // infoChiSpinner.hide();
            });
        };        // Users with Copy of Message
        getUsersWithCopy = function () {
            infoChiSpinner.show();
            $http.get('/web/messages/' + message_id + '/users_with_copy').then(function (data) {
                $scope.usersWithCopy = data.data;
                $scope.share = {'User': shareUserEmpty};
                infoChiSpinner.hide();
            });
        };
        getUsersToShare = function () {
            // Look into using a usersWithCopysession variable
            infoChiSpinner.show();
            $http.get('/web/messages/share_users').then(function (data) {
                $scope.usersShare = data.data;
//                $scope.usersShare.unshift(shareUserEmpty);
                infoChiSpinner.hide();
            });
        };
        getAttachments = function () {
            $scope.hasAttachments = 0;
            log_id = $scope.message.message.log_id;
            console.log('getAttachments()');
            console.log($scope.message.message.log_id);
            infoChiSpinner.show();
            $http.get('/web/attachment/log_id/' + log_id).then(function (response) {
                $scope.message.attachments = response.data;
                if ($scope.message.attachments.length) {
                    $scope.hasAttachments = 1;
                }
                console.log(response.data);
                infoChiSpinner.hide();
            });

        }
        $scope.getFile = function (id) {
            $http.get('/web/attachment/download/' + id).then(function (response) {
                console.log(response.data);
            });

        }

        // Reply to Message
        $scope.replyMessage = function () {
            window.location.href = '#/queue_messages/create/pm_ids/' + $scope.message.message.person_method_id;
        };

        // Users with Copy of Message
        $scope.runFilterShare = function (item) {
            return infoChiList.filter(item.id, $scope.usersWithCopy, 'user_id')
        };
        $scope.checkFilterShare = function () {
            // If Variables are present, run generic test to determine size of list
            if ($scope.usersShare && $scope.usersWithCopy) {
                return infoChiList.filterAll($scope.usersShare, 'id', $scope.usersWithCopy, 'user_id');
            } else {
                return 0;
            }
        };
        $scope.shareAndNotify = function (item) {
            if (item.id) {
                // infoChiSpinner.show();
                data = {
                    "message_id": message_id,
                    "user_id": item.id
                };
                console.log(data);
                $http.post('/web/messages/share_notify', data).then(function (data) {
                    getUsersToShare();
                    getUsersWithCopy();
                    // infoChiSpinner.hide();
                });
            }
        };

//        $scope.updateUserWithCopy = function () {
//            data = [];
//            data.unshift($scope.shareUserEmpty);
//            // Logic is to avoid items already in the list.
//            for (var i = 0; i < $scope.usersShare.length; i++) {
//                if ($scope.usersShare[i].id > 0) {
//
//                    if (infoChiArray.in_array($scope.usersShare[i].id, $scope.usersWithCopy, 'id', ['User'])) {
//
//                    } else {
//                        data.shift($scope.shareUserEmpty);
//                    }
//                    break;
//                }
//            }
//        };
        contactLookup = function () {
            infoChiSpinner.show();
            $http.get('/web/messages/' + message_id + '/contact_lookup').then(function (response) {
                if (response.data.SetContact == 1) {
                    $scope.message.person_id = response.data.PersonMethod.person_id;
                    $scope.message.person_method_id = response.data.PersonMethod.id
                } else {
                    $scope.valContactCreate = 1;
                    $scope.no_person = {"id": 0, "label": "Merge with Selected Person"};
                    getPeople();
                }
                infoChiSpinner.hide();

            });
        };
        $scope.contactCreate = function () {
            infoChiSpinner.show();
            $http.get('/web/messages/' + message_id + '/contact_create/').then(function (result) {
                infoChiSpinner.hide();
                if (result.data.ContactCreated == 1) {
                    getMessage();
                } else {
                    // TODO Reset option list
                }
            });
        };
        getPeople = function () {
            infoChiSpinner.show();
//            $http.get('/events/json_people/list').then(function (data) {
            $http.get('/web/people/0').then(function (data) {
                // Sort data.data
                people = data.data;
                if (people != undefined) {
                    people.unshift($scope.no_person);
                } else {
                    // If list is empty
                    speople = [$scope.no_person];
                }
                people.push($scope.person_function);
                $scope.people = people;
                infoChiSpinner.hide();
            });
        };
        $scope.merge = function (item) {
            infoChiSpinner.show();
            person_id = item.id;
            if (person_id != 0) {
                $http.get('/web/messages/' + message_id + '/' + person_id + '/contact_create/').then(function (result) {
                    infoChiSpinner.hide();
                    if (result.data.ContactCreated == 1) {
                        getMessage();
                    } else {
                        // TODO Reset option list
                    }
                });
            }
        };
// --
        message_id = $routeParams.message_id;
        shareUserEmpty = {'id': 0, "label": 'Share Message & Notify via Email'};
        getMessage();
    }]);
