infoChi.controller('metaColumnsCreateController', ['$scope', '$http', '$routeParams', 'infoChiSpinner',
    function ($scope, $http, $routeParams, infoChiSpinner) {
        $scope.selectItem = function (item) {
            $scope.data.mc_type_id = item.id;
        };
        $scope.submit = function () {
            if ($scope.data.mc_type_id == 0) {
                alert("Type must be selected");
                return;
            }
            data = {
                "name": $scope.data.name,
                "vname": $scope.data.vname,
                "mc_type_id": $scope.data.mc_type_id,
                "meta_column_type_id": type_id
            };
            infoChiSpinner.show();
            $http.post('/web/meta_columns/create', data).then(function (response) {
                window.location.href = "#/meta_columns/view/" + response.data.id;
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
        getTypes = function () {
            console.log('getTypes()');
            infoChiSpinner.show();
            $http.get('/web/meta_columns/types').success(
                    function (data) {
                        $scope.types = data;
                        infoChiSpinner.hide();
                    });
        };

        // -- Starting Data and Processes
        type_id = $routeParams.type_id;
        $scope.type_id = type_id;
        $scope.optValues = {
            "column": {
                "id": 0,
                "name": "Select Column Type"
            }
        };
        $scope.data = {
            "name": "",
            "vname": "",
            "mc_type_id": 0
        };
        getTypes();


    }
]);
