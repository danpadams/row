infoChi.controller('metaColumnsIndexController', ['$scope', '$http', 'infoChiSpinner', '$routeParams',
    function ($scope, $http, infoChiSpinner, $routeParams) {
        $scope.deleteItem = function (item) {
            if (confirm("Are You Sure you wish to remove additiona person information: '" + item.name + "'?")) {
                data = {
                    'id': item.id
                };
                infoChiSpinner.show();
                $http.post('/web/meta_columns/delete', data).then(function (response) {
                    getMetaColumns();
                    infoChiSpinner.hide();
                }, function (response) {
                    infoChiSpinner.hide();
                });
            }
        };
        getMetaColumns = function () {
            infoChiSpinner.show();
            $http.get('/web/meta_columns/' + $scope.data.type_id).then(function (data) {
                $scope.metaColumns = data.data;
                infoChiSpinner.hide();
            });
        };
        $scope.showEdit = function (item) {
            if (item.m_c_type) {
                if (item.m_c_type.display == 'enum') {
                    return true;
                }
            }
        };
        getDisplay = function () {
            $scope.Choices = {};
            $scope.Choices.display = [
                {id: 'st',
                    label: 'Short Text'
                }, {
                    id: 'lt',
                    label: 'Long Text'
                }, {
                    id: 'enum',
                    label: 'List'
                }
            ];
        };
        $scope.fromList = function (item, haystack) {
            for (var i=0;i<haystack.length;i++) {
                if (haystack[i].id==item) {
                    return haystack[i].label;
                }
            }
            return item;
        };

        $scope.data = {};
        $scope.data.type_id = parseInt($routeParams.type_id);
        switch ($scope.data.type_id) {
            // 2 Person Notes
            case 2:
                $scope.data.title = 'Additional Person Notes';
                break;
            // 1 Person Information
            case 1:
            // default:
                $scope.data.type_id = 1;
                $scope.data.title = 'Additional Person Information';
                break;
        }
        $scope.pageSize = 15;
        getMetaColumns();
        getDisplay();
    }]);
