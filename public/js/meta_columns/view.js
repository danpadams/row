infoChi.controller('metaColumnViewController', ['$scope', '$http', '$location', '$uibModal',
    'infoChiSpinner', 'infoChiFlash', 'infoChiTooltip', '$routeParams',
    function ($scope, $http, $location, $uibModal, infoChiSpinner, infoChiFlash, infoChiTooltip, $routeParams) {
        $scope.no_method = {"id": undefined, "label": "Choose Contact Method"};
        $scope.choice = [];
        $scope.pageSize = 15;

        // Tooltips
        // Common Stuff
        $scope.tooltips = {};
        $scope.tooltip = function (id) {
            infoChiTooltip.tooltip(id).then(function (response) {
                $scope.tooltips[id] = response;
            });
        };
        // Get Item
        $scope.tooltip(8);
        // --
        $scope.getGroup = function () {
            infoChiSpinner.show();
            $http.get('/web/meta_columns/view/' + $routeParams.meta_column_id).success(
                function (data) {
                    $scope.summary = data;
                    $scope.sdata = data;
                    setOptions();
                    infoChiSpinner.hide();
                });
        };
        $scope.getDetail = function () {
            infoChiSpinner.show();
            $http.get('/web/meta_columns/people/' + $routeParams.meta_column_id).success(
                function (data) {
                    for (key = 0; key < data.length; key++) {
                        if (data[key].PersonMethod != undefined) {
                            data[key].PersonMethod.unshift($scope.no_method);
                        } else {
                            data[key].PersonMethod = [$scope.no_method];
                        }
                    }
                    $scope.detail = data;
                    console.log(data);
                    infoChiSpinner.hide();
                });
        };
        $scope.setSort = function (predicate) {
            if (predicate == 'name') {
                $scope.reverse = ($scope.sortColumn === predicate) ? !$scope.reverse : false;
                $scope.sortColumn = 'name';
                $scope.predicate = ['sname', 'fname'];
            } else {
                $scope.predicate = predicate;
                $scope.reverse = ($scope.sortColumn === predicate) ? !$scope.reverse : false;
                $scope.sortColumn = predicate;
            }
        };
        $scope.sendComm = function () {
            methods = '';
            count = 0;
            // foreach is used for Data Structure
            angular.forEach($scope.choice, function (value, key) {
                if (value.id != undefined) {
                    if (count)
                        methods += ',';
                    methods += value.id;
                    count++;
                }
            });
            if (count > 0) {
                window.location.href = "#/queue_messages/create/pm_ids/" + methods;
            }
        };
        $scope.setMethod = function (type_id) {
            for (key = 0; key < $scope.detail.length; key++) {
                value = $scope.detail[key];
                if ($scope.runFilter(value)) {
                    if (type_id) {
                        for (skey = 0; skey < value.PersonMethod.length; skey++) {
                            svalue = value.PersonMethod[skey];
                            if (svalue.person_method_type_id == type_id) {
                                $scope.choice[svalue.person_id] = svalue;
                            }
                        }
                    } else {
                        $scope.choice[value.id] = $scope.no_method;
                        // Set NO Contact
                    }
                }
            }
        };
        $scope.setFilter = function (item) {
            $scope.valFilter = item.id;
            if (item.name != undefined) {
                $scope.optChoice.column = item;
            }
        };
        $scope.setEFilter = function (item) {
            $scope.valEFilter = item.id;
            if (item.name != undefined) {
                $scope.optEChoice.column = item;
            }
        };
        $scope.showData = function () {
            if ($scope.sdata) {
                switch ($scope.sdata.m_c_type.display) {
                    case 'enum':
                    case 'st':
                        return true;
                }
            }
            return false;
        };
        $scope.isList = function () {
            if ($scope.summary && $scope.summary.mc_type_id>3) {
                return true;
            } else {
                return false;
            }
        }
        // Combination of Primary and Secondary Filter
        $scope.runFilter = function (item) {
            retval = (runFilter(item) && runBFilter(item));
            return retval;
        };
        // Primary Filter
        runFilter = function (item) {
            if ($scope.valFilter == 0) {
                return true;
            }
            if (item.MetaData.m_d_value_text) {
                if (item.MetaData.m_d_value_text.value) {
                    if ($scope.valFilter == item.MetaData.m_d_value_text.value) {
                        return true;
                    }
                }
            }
            return false;
        }
        // Secondary Filter
        runBFilter = function (item) {
            if ($scope.valEFilter == 0) {
                return true;
            }
            if (item) {
                for (i = 0; i < item.meta_data.length; i++) {
                    if (item.meta_data[i].meta_column_id == $scope.valEFilter) {
                        return true;
                    }
                }
            }
            return false;
        }
        inArray = function (needle, haystack, name) {
            // Helper function to search an array of item using its value 'name'
            name = name || 'id';
            for (a = 0; a < haystack.length; a++) {
                if (haystack[a].id == needle) {
                    return true;
                }
            }
            return false;
        };

        setOptions = function () {
            // Sorting for Display
            $scope.sortOrder = 'name';
            $scope.predicate = 'name';
            $scope.reverse = false;

            // Make arrays of options
            $scope.Options = {column: []};
            $scope.EOpt = {column: []};
            // Build options array partly using data from the list of people
            no_option = {
                column: {id: 0, name: 'Choose Option'},
            };
            $scope.optChoice = no_option;
            optBChoiceNone = {
                column: {id: 0, name: 'Choose Option'},
            };
            $scope.optEChoice = optBChoiceNone;


            // Get Current Values
            infoChiSpinner.show();
            $http.get('/web/meta_enum/options/' + $scope.summary.mc_type_id).then(function (response) {
                $scope.Options.column = response.data;
                $scope.Options.column.unshift(no_option.column);
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
            // Optional Boolean Value
            infoChiSpinner.show();
            $http.get('/web/meta_enum/boolean').then(function (response) {
                $scope.EOpt.column = response.data;
                $scope.EOpt.column.unshift(no_option.column);
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });


            $scope.setFilter({id: 0});
            $scope.setEFilter({id: 0});
            if ($scope.sdata.m_c_type.meta_enum_value) {
                for (i = 0; i < $scope.sdata.m_c_type.meta_enum_value.length; i++) {
                    // Filter out if id is already in array
//                sdata.m_c_type.meta_enum_value
                    if (!inArray($scope.sdata.m_c_type.meta_enum_value[i].id, $scope.Options.column)) {
                        $scope.Options.column.unshift({
                            id: $scope.sdata.m_c_type.meta_enum_value[i].id,
                            label: $scope.sdata.m_c_type.meta_enum_value[i].name
                        });
                    }
                }
            }


            $scope.Options.column.unshift(no_option.column);
        };

        // --
        // Edit MetaColumn
        $scope.editMetaColumn = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/html/meta_columns/modal-edit.html',
                controller: 'metaColumnEditController',
// 	            size: size,
                resolve: {
                    modal: function () {
                        return {"title": "Edit Additional Person Information"};
                    },
                    data: function () {
                        return $scope.summary;
                    }
                }
            });

            modalInstance.result.then(function (data) {
                // Update data on 'OK' in modal
                $http.post('/web/meta_columns/update', data).then(function (data) {
                    infoChiFlash.show('MetaColumn Information Updated', 'success', 5000);
                    $scope.event = data.data;
                });
                console.log('Modal dismissed with "Close" at: ' + new Date());
            }, function () {
                console.log('Modal dismissed with "Cancel" at: ' + new Date());
            });
        };

        // --
        $scope.getDetail();
        $scope.getGroup();
    }
]);

infoChi.controller('metaColumnEditController', function ($uibModalInstance, modal, data, $scope, $http, infoChiSpinner, infoChiTooltip) {
        $scope.ok = function () {
            $uibModalInstance.close($scope.data);
        };
        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        // --
        $scope.data = data;
        $scope.modal = modal;
    }
);