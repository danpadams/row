infoChi.controller('peopleAddressController', ['$scope', '$http', '$location',
    'infoChiSpinner',
    function ($scope, $http, $location, infoChiSpinner) {
        no_method = {"id": undefined, "label": "Choose Contact Method"};
        $scope.choice = [];
        $scope.pageSize = 15;
        // --
        $scope.getDetail = function () {
            infoChiSpinner.show();
            $http.get('/web/people/address/').success(
                    function (data) {
                        for (key = 0; key < data.length; key++) {
                            if (data[key].PersonMethod != undefined) {
                                data[key].PersonMethod.unshift(no_method);
                            } else {
                                data[key].PersonMethod = [no_method];
                            }
                        }
                        $scope.detail = data;
                        setOptions();
                        infoChiSpinner.hide();
                    });
        };
        $scope.sendComm = function () {
            // Prepare for the Send Message Page, pre-loading each contact method through the URL
            methods = '';
            count = 0;
            // foreach is used for Data Structure
            angular.forEach($scope.choice, function (value, key) {
                if (value.id != undefined) {
                    if (count)
                        methods += ',';
                    methods += value.id;
                    count++;
                }
            });
            if (count > 0) {
                window.location.href = "#/queue_messages/create/pm_ids/" + methods
            }
        };
        $scope.setMethod = function (type_id) {
            for (var key = 0; key < $scope.detail.length; key++) {
                value = $scope.detail[key];
                if ($scope.runFilter(value)) {
                    if (type_id) {
                        for (var skey = 0; skey < value.PersonMethod.length; skey++) {
                            svalue = value.PersonMethod[skey]
                            if (svalue.person_method_type_id == type_id) {
                                console.log('Found');
                                $scope.choice[svalue.person_id] = svalue;
                            }
                        }
                    } else {
                        $scope.choice[value.id] = no_method;
                        // Set NO Contact
                    }
                    console.log('-');
                }
            }
        };
        inArray = function (needle, haystack, name) {
            // Helper function to search an array of item using its value 'name'
            name = name || 'id';
            for (a = 0; a < haystack.length; a++) {
                if (haystack[a].id == needle) {
                    return true;
                }
            }
            return false;
        };
        resetDistance = function () {
            // Reset the ZipCode Distance Calculator and prepare for input again of ZipCode
            $scope.Options.distance = [];
            return;
        };

        $scope.setFilter = function (item, type) {
            // Store the filter data, allowing for multiple types
            if (type) {
                if ((type == 'distance') && (item == -1)) {
                    //  Allow for Reset of Calculator
                    $scope.Options.distance = [];
                    item = 0;
                    type = 'd';
                    $scope.optChoice.distance = no_option.distance;
                }
                for (i = 0; i < valTypes.length; i++) {
                    // Reset all valuess to default
                    if (valTypes[i] != type) {
                        $scope.optValues[valTypes[i]] = no_option[valTypes[i]];
                    }
                }
                $scope.valFilterType = type;
            }
            $scope.valFilter = item;
        };
        $scope.getDistance = function () {
            // Get distance values from server.
            infoChiSpinner.show();
            localZip = parseInt($scope.valLocalZip);
            $http.get('/web/people/distance/' + localZip).success(function (data) {
                for (i = 0; i < $scope.detail.length; i++) {
                    if (data[$scope.detail[i].zip]) {
                        $scope.detail[i].distance = data[$scope.detail[i].zip].Distance;
                    }
                }
                if ($scope.Options.distance.length == 0) {
                    $scope.Options.distance.unshift({id: -1, label: 'Reset Mileage Calculator'});
                    $scope.Options.distance.unshift({id: 25, label: '25 Miles'});
                    $scope.Options.distance.unshift({id: 10, label: '10 Miles'});
                    $scope.Options.distance.unshift(no_option.distance);
                }
                infoChiSpinner.hide();
            });
        };
        inArray = function (needle, haystack, name) {
            // Helper function to search an array of item using its value 'name'
            name = name || 'id';
            for (a = 0; a < haystack.length; a++) {
                if (haystack[a].id == needle) {
                    return true;
                }
            }
            return false;
        };
        $scope.runFilter = function (item) {
            // Process the filter, allowing for a valFilter of 0 to reset the filter
            if ($scope.valFilter == 0) {
                return true;
            } else {
                switch ($scope.valFilterType) {
                    case 'distance':
                        if (item.distance < $scope.valFilter) {
                            return true;
                        }
                        break;

                    default:
                        if (item[$scope.valFilterType] == $scope.valFilter) {
                            return true;
                        }
                        break;
                }
            }
            return false;
        };
        setOptions = function () {
            // Build options array partly using data from the list of people
            no_option = {
                city: {id: 0, label: 'Choose City Option'},
                state: {id: 0, label: 'Choose State Option'},
                zip: {id: 0, label: 'Choose Zip Option'},
                distance: {id: 0, label: 'Choose Distance Option'}
            };
            valTypes = ['city', 'state', 'zip', 'distance'];
            $scope.optChoice = no_option;

            // Make arrays of options
            $scope.Options = {city: [], state: [], zip: [], distance: []};
            for (i = 0; i < $scope.detail.length; i++) {
                // Filter out if id is already in array
                if (!inArray($scope.detail[i].city, $scope.Options.city)) {
                    $scope.Options.city.unshift({id: $scope.detail[i].city, label: $scope.detail[i].city});
                }
                if (!inArray($scope.detail[i].state, $scope.Options.state)) {
                    $scope.Options.state.unshift({id: $scope.detail[i].state, label: $scope.detail[i].state});
                }
                if (!inArray($scope.detail[i].zip, $scope.Options.zip)) {
                    $scope.Options.zip.unshift({id: $scope.detail[i].zip, label: $scope.detail[i].zip});
                }
            }
            console.log($scope.Options);
            $scope.Options.city.unshift(no_option.city);
            $scope.Options.state.unshift(no_option.state);
            $scope.Options.zip.unshift(no_option.zip);
            console.log($scope.Options);
        };
        setupVars = function () {
            $scope.optValues = {};
            $scope.valLocalZip = '';
        };
        // --
        setupVars();
        $scope.setFilter(0);
        $scope.getDetail();
    }
]);
