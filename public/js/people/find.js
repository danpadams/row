infoChi.controller('peopleFindController', ['$scope', '$http', 'infoChiSpinner',
    function ($scope, $http, infoChiSpinner) {
        $scope.submit = function () {
            if ($scope.text) {
                infoChiSpinner.show();
                var method = $scope.text;
                $http.get('/web/people/find/' + method).then(function (response) {
                    $scope.data = response.data;
                    console.log($scope.data);
                    $scope.personMethod_id = 1;
                    $scope.text = '';
                    infoChiSpinner.hide();
                });
            }
        };
        $scope.clear = function () {
            $scope.text = '';
            $scope.personMethod_id = 0;
        };
        $scope.clear();
    }
]);
