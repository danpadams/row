infoChi.controller('peopleViewController', ['$scope', '$http', '$log',
    '$routeParams', '$uibModal', 'infoChiSpinner', 'infoChiList', 'infoChiFlash',
    function ($scope, $http, $log, $routeParams, $uibModal, infoChiSpinner, infoChiList, infoChiFlash) {
        startData = function () {
            $scope.pageSize = 5; // For Extra Stuff         
            person_id = $routeParams.person_id;
            preferred_id = 0;

            // Person & Methods
            getPerson();
            getMethods();
            startDataNewMethod();
            getTypes();
            // Additional Informations
            getMetaColumns();
            getMetaData();
            // People Groups
            getGroups();
            getGroupList();
            // Additional Person Info (MetaColumns)
            $scope.metaDataEnum = [];
            $scope.new_reminder = {};
            $scope.data = {
                "dateDropDownInput": new Date()
            }
            $scope.choices = {
                "opt_out_status": [
                    {
                        id: "0",
                        name: 'Ok to Contact'
                    }, {
                        id: 1,
                        name: 'Do Not Contact'
                    }
                ]
            };
        };
        startDataNewMethod = function () {
            $scope.newMethod = {
                type: {
                    "id": "0",
                    "method": "Choose Type"
                },
                method: ''
            };
        };

        getPerson = function () {
            // Current Person Info
            infoChiSpinner.show();
            $http.get('/web/people/view/' + person_id).then(function (data) {
                $scope.person = data.data;
                $scope.modal = {
                    "title": "Edit Person Details"
                };
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };

        getMethods = function () {
            // Currently defined methods
            infoChiSpinner.show();
            $http.get('/web/people/view_methods/' + person_id).then(function (response) {
                    $scope.methods = response.data;
                    // Find a mark the preferred method as un-editable
                    for (var i = 0; i < $scope.methods.length; i++) {
                        if ($scope.methods[i].person_method_type_id == 10) {
                            $scope.methods[i].disallowPreferred = 1;
                            $scope.methods[i].disallowEdit = 1;
                            preferred_id = response.data[i].id;
                            preferredMethod = $scope.methods[i].method;
                        } else {
                            $scope.methods[i].disallowPreferred = 0;
                            $scope.methods[i].disallowEdit = 0;
                        }
                    }
                    if (preferred_id != 0) {
                        for (var i = 0; i < $scope.methods.length; i++) {
                            if ($scope.methods[i].disallowPreferred == 0) {
                                // Find where preferred came from
                                if ($scope.methods[i].method == preferredMethod) {
                                    $scope.methods[i].disallowPreferred = 1;
                                    // Still Allow the Edit of this method
                                }
                                // Don't allow preferred as phone if no numbers
                                if ((!$scope.$parent.Current.Client.hasNumbers)
                                    && ($scope.methods[i].method.indexOf('@') == -1)) {
                                    // ("@ not found inside '"+$scope.methods[i].method+"'");
                                    $scope.methods[i].disallowPreferred = 1;
                                }

                            }
                        }
                    }

                    infoChiSpinner.hide();
                }
                , function (response) {
                    infoChiSpinner.hide();
                });
        };
        $scope.setPreferred = function (item) {
            data = {
                'id': preferred_id,
                'person_id': person_id,
                'person_method_type_id': 10,
                'method': item
            };
            infoChiSpinner.show();
            $http.post('/web/people/update_method', data).then(function (response) {
                infoChiFlash.show(response.data.message, 'success', 5000, {container: 'areaMethod'});
                getMethods();
                infoChiSpinner.hide();
            });
        };
        $scope.filterMethodTypes = function (item) {
            if (item) {
                if (item.id == 10) {
                    return false;
                }
                return true;
            }
            return false;
        };

        getTypes = function () {
            infoChiSpinner.show();
            $http.get('/web/people/view_method_types').then(function (data) {
                methodTypes = data.data.slice(0); // Preserve this Data
                $scope.types = data.data;
                $scope.chooseType({'id': 0});
                // Add General Method to List
                $scope.types.unshift($scope.newMethod.type);
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });

        };

        $scope.addMethod = function () {
            if ($scope.newMethod.type.id == undefined) {
                alert('Please Select a Contact Method Type');
                return;
            }
            switch ($scope.newMethod.type.id) {
                case '1':
                case '2':
                case '4':
                    method = $scope.newMethod.method.replace(/[^+\d]+/g, "");
                    break;
                default:
                    method = $scope.newMethod.method;
                    break;
            }
            data = {
                "person_id": person_id,
                "person_method_type_id": $scope.newMethod.type.id,
                "method": method
            };
            infoChiSpinner.show();
            $http.post('/web/people/add_method', JSON.stringify(data)).then(function (response) {
                infoChiFlash.show(response.data.message, response.data.status, 5000, {container: 'areaMethod'});
                getMethods();
                startDataNewMethod();
                infoChiSpinner.hide();
            });
        }
        $scope.chooseType = function (item) {
            switch (item.id) {
                case 1:
                case 2:
                case 4:
                    $scope.newMethod.methodPlaceholder = 'Phone Number';
                    break;
                case 3:
                    $scope.newMethod.methodPlaceholder = 'Email Address';
                    break;
                default:
                    $scope.newMethod.methodPlaceholder = 'Phone Number or Email Address';
                    break;
            }
        }
        $scope.methodDelete = function (person_method_id) {
            infoChiSpinner.show();
            data = {
                "id": person_method_id
            };
            $http.post('/web/people/delete_method', data).then(function (response) {
                infoChiFlash.show(response.data.message, 'success', 5000, {container: 'areaMethod'});
                getMethods();
                infoChiSpinner.hide();
            });
        };

        $scope.getInvites = function () {
//            infoChiSpinner.show();
            $http.get('/people/json_invites/' + $location.search().person_id).then(function (data) {
                $scope.invites = data.data;
//                infoChiSpinner.hide();
            }, function (response) {
//                infoChiSpinner.hide();
            });
        }

        // Group Membership
        getGroups = function () {
            infoChiSpinner.show();
            $http.get('/web/person_groups/' + person_id + '/person').then(function (response) {
                $scope.groups = response.data;
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
        $scope.removeFromGroup = function (pgd_id) {
            data = {
                "pgd_id": pgd_id
            };
            infoChiSpinner.show();
            $http.post('/web/person_groups/remove_from_group', data).then(function (response) {
                getGroups();
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
        $scope.addToGroup = function (item) {
            data = {
                "person_id": person_id,
                "person_group_id": item.id
            };
            infoChiSpinner.show();
            $http.post('/web/person_groups/add_to_group', data).then(function (response) {
                getGroups();
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
        getGroupList = function () {
            infoChiSpinner.show();
            $http.get('web/person_groups').then(function (response) {
                $scope.group_list = response.data;
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
        $scope.filterGroupList = function (item) {
            return infoChiList.filter(item.id, $scope.groups, 'group_id');
        };
        $scope.filterGroupTest = function () {
            // If Variables are present, run generic test to determine size of list
            if ($scope.group_list && $scope.groups) {
                return infoChiList.filterAll($scope.group_list, 'id', $scope.groups, 'group_id');
            } else {
                return 0;
            }
        };
        // Group Membership
        // MetaColumns
        getMetaColumns = function () {
            infoChiSpinner.show();
            $http.get('/web/meta_columns/0').then(function (response) {
                $scope.metaColumns = response.data;
                $scope.metaColumnsByType = [];
                for (counter = 0; counter<response.data.length; counter++) {
                    if (!Array.isArray($scope.metaColumnsByType[response.data[counter].meta_column_type_id])) {
                        $scope.metaColumnsByType[response.data[counter].meta_column_type_id] = [];
                    }
                    $scope.metaColumnsByType[response.data[counter].meta_column_type_id].push(response.data[counter]);
                 }
                infoChiSpinner.hide();
            });
        };
        getMetaData = function () {
            infoChiSpinner.show();
            $http.get('/web/meta_data/person/' + person_id).then(function (response) {
                $scope.metaData = response.data;
                $scope.metaDataByType = [];
                for (i = 0; i < $scope.metaData.length; i++) {
                    if ($scope.metaData[i].m_d_value_text) {
                        $scope.metaData[i].m_d_value_text.value_orig = $scope.metaData[i].m_d_value_text.value;
                    }
                    if ($scope.metaData[i].meta_column.m_c_type.display == 'enum') {
                        getMetaEnum($scope.metaData[i].meta_column.m_c_type.id);
                    }

                    counter_getMetaData = i;
                    if (!Array.isArray($scope.metaDataByType[response.data[counter_getMetaData].meta_column.meta_column_type_id])) {
                        $scope.metaDataByType[response.data[counter_getMetaData].meta_column.meta_column_type_id] = [];
                    }
                    $scope.metaDataByType[response.data[counter_getMetaData].meta_column.meta_column_type_id].push(response.data[counter_getMetaData]);
                }
                console.log($scope.metaDataByType);
                infoChiSpinner.hide();
            }, function (response) {
//                infoChiSpinner.hide();
            });
        };

        $scope.addToMeta = function (item, location) {
            if (!location) {
                location = 'areaMetaColumn';
            }
            data = {
                "meta_column_id": item.id,
                "person_id": person_id
            };
            $http.post('/web/meta_data/column_add', data).then(function (response) {
                infoChiFlash.show(response.data.message, response.data.status, 5000, {container: 'location'});
                getMetaData(); // ? Re-use
                getMetaColumns();
// 			infoChiSpinner.hide();
            }, function (response) {
//             infoChiSpinner.hide();
            });
        }
        $scope.removeMeta = function (id) {
            data = {
                "id": id
            };
            infoChiSpinner.show();
            $http.post('/web/meta_data/column_remove', data).then(function (response) {
                infoChiFlash.show(response.data.message, response.data.status, 5000, {container: 'areaMetaColumn'});
                getMetaColumns();
                getMetaData(); // ? Re-use
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinner.hide();
            });
        };
        $scope.filterMetaList = function (item) {
//         Logic is to avoid items already in the list.
            if ($scope.metaData) {
                for (var i = 0; i < $scope.metaData.length; i++) {
                    if (item.id == $scope.metaData[i].meta_column_id) {
                        return false;
                    }
                }
            }
            return true;
        };
        $scope.updateMeta = function (item, extraValue) {
            if (item.m_d_value_text.id == undefined) {
                id = 0;
            } else {
                id = item.m_d_value_text.id;
            }
            data = {
                "id": id,
                "person_id": item.person_id,
                "meta_data_id": item.id,
                "value": item.m_d_value_text.value
            };
            infoChiSpinner.show();
            $http.post('/web/meta_data/update', data).then(function (response) {
                // --
                console.log(response.data);
                console.log(item);
                console.log(extraValue);
                for (i = 0; i < $scope.metaData.length; i++) {
                    if ($scope.metaData[i].MDValue) {
                        if ($scope.metaData[i].id == response.data.meta_data_id) {
                            // Set Text Value
                            $scope.metaData[i].MDValue.value_orig = data.value
                            // Set Dropdown Box
                            metaDat.m_d_value_text.meta_enum_value.value
                            metaDat.m_d_value_text.meta_enum_value.name = extraValue.name;
                            // extraValue = item;
                            // $scope.metaData[i].MDValue.MetaEnumValues = extraValue;
                        }
                    }
                }
                // --
                infoChiSpinner.hide();
            }, function (response) {
//                infoChiSpinner.hide();
            });
        };
        $scope.updateMetaEnum = function (choice, item) {
            item.m_d_value_text.value = choice.id;
            $scope.updateMeta(item, choice);
        };
        getMetaEnum = function (enum_id) {
            infoChiSpinner.show();
            $http.get('/web/meta_enum/options/' + enum_id).then(function (response) {
                $scope.metaDataEnum[enum_id] = response.data;
                infoChiSpinner.hide();
            }, function (response) {
                infoChiSpinnerService.hide();
            });
        };
        $scope.createReminder = function () {
            data = {
                "person_id": person_id,
                "message": $scope.new_reminder.message,
                "user_id": $scope.$parent.Current.User.id,
                "processtime": $scope.data.dateDropDownInput
            };
            console.log(data);
            infoChiSpinner.show();
            $http.post('/web/reminders/create', data).then(function (response) {
                infoChiFlash.show(response.data.message, 'success', 5000, {container: 'areaReminder'});
                $scope.new_reminder.message = '';
                $scope.data.dateDropDownInput = new Date();
                infoChiSpinner.hide();
            });
        }
//      --

        $scope.startDateBeforeRender = function ($dates) {
            const todaySinceMidnight = new Date();
            todaySinceMidnight.setUTCHours(0, 0, 0, 0);
            $dates.filter(function (date) {
                return date.utcDateValue < todaySinceMidnight.getTime();
            }).forEach(function (date) {
                date.selectable = false;
            });
        };

        // Modal Dialog - Edit Current User
        $scope.open = function (size) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/html/people/edit-person.html',
                controller: 'personEditCtrl',
                size: size,
                resolve: {
                    modal: function () {
                        return {"title": "Edit Person Name / Address"};
                    },
                    data: function () {
                        return $scope.person;
                    },
                    choices: function () {
                        return $scope.choices;
                    }
                }
            });

            modalInstance.result.then(function (data) {
                $http.post('/web/people/update', data).then(function (data) {
                    $scope.event = data.data;
                });
                $log.info('Modal dismissed with "Close" at: ' + new Date());
            }, function () {
                startData();
                $log.info('Modal dismissed with "Cancel" at: ' + new Date());
            });
        };

        // Modal Dialog - Edit Contact Method
        $scope.editMethod = function (size, item) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/html/people/edit-method.html',
                controller: 'personMethodEditCtrl',
                resolve: {
                    modal: function () {
                        return {"title": "Edit Contact Method"};
                    },
                    data: function () {
                        return item;
                    },
                    methodTypes: function () {
                        return methodTypes;
                    }
                }
            });

            modalInstance.result.then(function (data) {
                infoChiSpinner.show();
                method = data.method;
                switch (data.type_id) {
                    case '1':
                    case '2':
                    case '4':
                        method = method.replace(/[^+\d]+/g, "");
                        break;
                    default:
                        break;
                }
                data.method = method;
                $http.post('/web/people/update_method', data).then(function (response) {
                    infoChiFlash.show(response.data.message, 'success', 5000);
                    getMethods();
                    infoChiSpinner.hide();
                });

            }, function () {
                $log.info('Modal dismissed with "Cancel" at: ' + new Date());
                infoChiSpinner.hide();
            });
        };

       $scope.$on('view_getFeatures', function () {
            $scope.Features = $scope.$parent.Features;
        });
       $scope.hasFeature = function (feature_id) {
           return $scope.Features.indexOf(feature_id) != -1;
        }

        // --
        $scope.$parent.getFeatures('view_getFeatures');
        $scope.$parent.getCurrent();
        startData();
    }
]);

infoChi.controller('personEditCtrl', function ($scope, data, choices, $uibModalInstance) {
    $scope.ok = function () {
        $uibModalInstance.close($scope.data);
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.data = data;
    $scope.choices = choices;

    for (var i = 0; i < $scope.choices.opt_out_status.length; i++) {
        if ($scope.choices.opt_out_status[i].id == data.opt_out_status) {
            $scope.opt_out_status_name = $scope.choices.opt_out_status[i].name;
            break;
        }
    }

    $scope.update_opt_out_status = function (item) {
        id = item.id
        if (id == 1) {
            data.opt_out_status = 1;
            console.log(id);
        }
    }
});

// Please note that $uibModalInstance represents a modal window (instance) dependency.
// It is not the same as the $uibModal service used above.

infoChi.controller('personMethodEditCtrl', function ($scope, $uibModalInstance, modal, data, methodTypes) {
    $scope.ok = function () {
        $uibModalInstance.close($scope.data);
    };
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };

    $scope.filterTypes = function (item) {
        switch (item.id) {
//            case 3:     // Don't have email in list
            case 10:    // Preferred Contact
                return false;
                break;
            default:
        }
        return true;
    };
    $scope.setType = function (item) {
        $scope.data.person_method_type_id = item.id;
    };
    // --

    $scope.modal = modal;
    $scope.methodTypes = methodTypes;
    $scope.data = data;
    $scope.data.method = data.forMethod;
    $scope.newMethod = {
        'method': data.Type,
        'id': data.person_method_type_id
    };
});

