// $location is for Future Development
infoChi.controller('peopleController', ['$scope', '$http', '$localStorage',
    '$location', 'infoChiSpinner', 'infoChiTooltips',
    function ($scope, $http, $localStorage, $location, infoChiSpinner, infoChiTooltips) {

        getPeople = function () {
            infoChiSpinner.show();
            $http.get('/web/people/' + $scope.$storage.deleted).success(function (data) {
                $scope.people = data;
                infoChiSpinner.hide();
            });
        };

        $scope.$storage = $localStorage.$default({
            peopleListDeleted: 0
        });

        // Tooltips
        infoChiTooltips.tooltip(1);

        $scope.btnClearSearch = function () {
            $scope.q = '';
        };

        $scope.setDeletedList = function (deleted) {
            $scope.$storage.deleted = deleted;
            getPeople();
        };

        $scope.showStatus = function () {
            switch ($scope.$storage.deleted) {
                case 0:
                    return 'Active';
                case 1:
                    return 'Deleted';
            }

        };

        $scope.changeStatus = function (item) {
            data = {
                "id": item.id,
                "deleted": !item.deleted
            };
            infoChiSpinner.show();
            $http.post('/web/people/update', data).then(function (response) {
                getPeople();
                infoChiSpinner.hide();
            });
        };
        // --

        $scope.pageSize = 15;
        $scope.setDeletedList(0)
//        $scope.showStatus();
//        getPeople();
    }]);
