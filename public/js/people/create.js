// $location is for Future Development
infoChi.controller('peopleCreateController', ['$scope', '$http', '$location', 'infoChiSpinner', 'infoChiFlash',
    function ($scope, $http, $location, infoChiSpinner, infoChiFlash) {
        if ($location.search().person_group_id) {
            $scope.person_group_id = $location.search().person_group_id;
        }
        getMetaEnum = function () {
            enum_id = 9;
            infoChiSpinner.show();
            $http.get('/web/meta_enum/active/' + 9).then(function (response) {
                $scope.metaDataEnum = [];
                $scope.metaDataEnum[enum_id] = response.data;

                infoChiSpinner.hide();
            });
        };

        cleanMethod = function (method, type) {
            switch (type) {
                case '1':
                case '2':
                case '4':
                    method = method.replace(/[^+\d]+/g, "");
                    break;
                case 3:
                default:
                    // method = method;
                    break;
            }
            return method;
        };
        addContactMethod = function (person_id, method, method_type_id) {
            data = {
                "person_id": person_id,
                "person_method_type_id": method_type_id,
                "method": method
            };
            infoChiSpinner.show();
            $http.post('/web/people/add_method', JSON.stringify(data)).then(function (data) {
                infoChiSpinner.hide($scope);
            });
        }

        /**
         * @param person_id
         * @param column_id
         * @param value_id
         */
        addColumn = function (person_id, column_id, value_id) {
            data = {
                "person_id": person_id,
                "meta_column_id": column_id
            }
            infoChiSpinner.show();
            $http.post('/web/meta_data/column_add', data).then(function (response) {
                if (value_id) {
                    updateMeta(response.data.id, person_id, value_id)
                }
                infoChiSpinner.hide($scope);
            });
        }
        updateMeta = function (column_id, person_id, value_id) {
            data = {
                "id": 0,
                "person_id": person_id,
                "meta_data_id": column_id,
                "value": value_id
            };
            infoChiSpinner.show();
            $http.post('/web/meta_data/update', data).then(function (response) {
                infoChiSpinner.hide($scope);
            });
        }
        $scope.submit = function () {
            infoChiSpinner.show();
            person_id = 0;
            // Determine Landing Page name from last part of URL
            var url = window.location.href.replace(/\/$/, '');  /* remove optional end / */
            var lastSeg = url.substr(url.lastIndexOf('/') + 1); // substr() is Legacy Method
            $scope.data.domain=lastSeg;
            // Clean Contact Methods
            $scope.data.contact.cell = cleanMethod($scope.data.contact.cell, 1);
            // return;
            $http.post('/web/people/create', $scope.data).then(function (response) {
                person_id = response.data.id;
                stayHere = false;
                for (i in $scope.data.checkbox) {
                    if ($scope.data.checkbox[i] == true) {
                        addColumn(person_id, i);
                    }
                }
                if ($scope.data.cell) {
                    // Assign to Type 1 - Cellphone (SMS)
                    addContactMethod(person_id, $scope.data.cell, 1);
                    if ($scope.data.preferred == 'sms' && $scope.data.cell) {
                        // Assign to Type 10 - Preferred
                        addContactMethod(person_id, $scope.data.cell, 10);
                        addColumn(person_id, 56);
                    }
                }
                if ($scope.data.email) {
                    // Assign to Type 3 - Email
                    addContactMethod(person_id, $scope.data.email, 3);
                    if ($scope.data.preferred == 'email') {
                        // Assign to Type 10 - Preferred
                        addContactMethod(person_id, $scope.data.email, 10);
                        addColumn(person_id, 55);
                    }
                }

                if ($scope.data.metaDataEnum && $scope.data.metaDataEnum.value) {
                    addColumn(person_id, 60, $scope.data.metaDataEnum.value.id); // 'Event Name' for GlamourNails: 16
                }
                infoChiSpinner.hide($scope);
            });
        };
        $scope.$on('afterSubmit', function (e) {
            afterSubmit();
        });
        afterSubmit = function () {
            $scope.data = {contact: {}, checkbox: {}, preferred: {}};
            infoChiFlash.show("Information Submitted into Database", 'success', 5000, {});
        }

        //
        $scope.data = {contact: {}, checkbox: {}, preferred: {}};
        getMetaEnum();
    }
]);
