infoChi.controller('propertiesCreateCtrl', ['$scope', '$http',
    function ($scope, $http) {
        $scope.updateData = function () {
            // Create the date for the Property
            data = {
                'name': $scope.Data.name,
                'title': $scope.Data.description,
                'body': $scope.Data.address
            };
            console.log(data);
            // infoChiSpinner.show();
            $http.post('/web/properties/create', data).then(function (response) {
                $scope.Data = response.data;
                // infoChiSpinner.hide();
            });
        };
        // --
    }
]);
