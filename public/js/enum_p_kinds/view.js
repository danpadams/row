infoChi.controller('propertiesViewCtrl', ['$scope', '$http', '$routeParams',
    function ($scope, $http, $routeParams) {
        getInfo = function () {
            // Retreive the data for the Short
            // infoChiSpinner.show();
            $http.get('/web/properties/' + property_id).then(function (response) {
                $scope.Data = response.data;
                // infoChiSpinner.hide();
            });
        };
        $scope.updateData = function () {
            // Update the date for the Short
            data = {
                'id': $scope.Data.id,
                'name': $scope.Data.name,
                'title': $scope.Data.description,
                'body': $scope.Data.address
            };
            console.log(data);
            // infoChiSpinner.show();
            $http.post('/web/properties/update', data).then(function (response) {
                $scope.Data = response.data;
                // infoChiSpinner.hide();
            });
        };
        // --

        property_id = $routeParams.property_id;
        getInfo();
    }
]);
