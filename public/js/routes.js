infoChi.config(function ($routeProvider) {
    $routeProvider.otherwise({redirectTo: '/home'});

    $routeProvider.when("/clients", {
        templateUrl: "/html/clients/index.html",
        controller: "clientsIndexCtrl"
    });
    $routeProvider.when("/clients/edit", {
        templateUrl: "/html/clients/edit.html",
        controller: "clientsEditCtrl"
    });
    $routeProvider.when("/clients/create", {
        templateUrl: "/html/clients/create.html",
        controller: "clientsCreateCtrl"
    });

    $routeProvider.when("/home", {
        templateUrl: "/html/home.html",
        controller: "defaultHomeCtrl"
    });
    $routeProvider.when("/login", {
        templateUrl: "/html/login.html",
        controller: "defaultLoginCtrl"
    });

    $routeProvider.when('/events', {
        templateUrl: "/html/events/index.html",
        controller: "eventsIndexController"
    });
    $routeProvider.when('/events/create', {
        templateUrl: "/html/events/create.html",
        controller: "eventsCreateController"
    });
    $routeProvider.when('/events/:event_id/invite', {
        templateUrl: "/html/events/invite.html",
        controller: "eventsInviteController"
    });
    $routeProvider.when('/events/:event_id/attendance_history', {
        templateUrl: "/html/events/attendance_history.html",
        controller: "eventsAttendanceHistoryController"
    });
    $routeProvider.when('/events/:event_id/attendance', {
        templateUrl: "/html/events/attendance.html",
        controller: "eventsAttendanceController"
    });

    $routeProvider.when('/logs', {
        templateUrl: "/html/logs/index.html",
        controller: "logsIndexCtrl"
    });
    $routeProvider.when('/logs/person/:person_id', {
        templateUrl: "/html/logs/person.html",
        controller: "logsPersonCtrl"
    });
    $routeProvider.when('/logs/:log_id', {
        templateUrl: "/html/logs/view.html",
        controller: "logsViewCtrl"
    });

    $routeProvider.when('/messages', {
        templateUrl: "/html/messages/index.html",
        controller: "messagesIndexController"
    });
    $routeProvider.when('/messages/:message_id', {
        templateUrl: "/html/messages/view.html",
        controller: "messagesViewController"
    });

    $routeProvider.when('/meta_columns/view/:meta_column_id', {
        templateUrl: "/html/meta_columns/view.html",
        controller: "metaColumnViewController"
    });
    $routeProvider.when('/meta_columns/create', {
        templateUrl: "/html/meta_columns/create.html",
        controller: "metaColumnsCreateController"
    });
    $routeProvider.when('/meta_columns/create/:type_id', {
        templateUrl: "/html/meta_columns/create.html",
        controller: "metaColumnsCreateController"
    });
    $routeProvider.when('/meta_columns/:type_id', {
        templateUrl: "/html/meta_columns/index.html",
        controller: "metaColumnsIndexController"
    });

    $routeProvider.when("/numbers", {
        templateUrl: "/html/numbers/index.html",
        controller: "numbersIndexCtrl"
    });

    $routeProvider.when("/outgoing_messages", {
        templateUrl: "/html/outgoing_messages/index.html",
        controller: "outgoingMessagesController"
    });
    $routeProvider.when("/outgoing_messages/:outgoing_message_id", {
        templateUrl: "/html/outgoing_messages/view.html",
        controller: "outgoingMessagesViewController"
    });

    $routeProvider.when("/p_branches/:p_branch_id", {
        templateUrl: "/html/p_branches/view.html",
        controller: "pBranchesViewCtrl"
    })

    $routeProvider.when("/p_trees", {
        templateUrl: "/html/p_trees/index.html",
        controller: "pTreesIndexCtrl"
    });
    $routeProvider.when("/p_trees/add", {
        templateUrl: "/html/p_trees/view.html",
        controller: "pTreesViewCtrl"
    });

    $routeProvider.when("/p_trees/:p_tree_id", {
        templateUrl: "/html/p_trees/view.html",
        controller: "pTreesViewCtrl"
    });

    $routeProvider.when("/people", {
        templateUrl: "/html/people/index.html",
        controller: "peopleController"
    });
    $routeProvider.when('/people/create', {
        templateUrl: '/html/people/create.html',
        controller: 'peopleCreateController'
    });
    $routeProvider.when('/people/find', {
        templateUrl: '/html/people/find.html',
        controller: 'peopleFindController'
    });
    $routeProvider.when('/people/view/:person_id', {
        templateUrl: '/html/people/view.html',
        controller: 'peopleViewController'
    });
    $routeProvider.when('/people/address', {
        templateUrl: '/html/people/address.html',
        controller: 'peopleAddressController'
    });

    $routeProvider.when('/person_groups', {
        templateUrl: '/html/person_groups/index.html',
        controller: 'personGroupIndexController'
    });
    $routeProvider.when('/person_groups/create', {
        templateUrl: '/html/person_groups/create.html',
        controller: 'personGroupCreateController'
    });
    $routeProvider.when('/person_groups/view/:person_group_id', {
        templateUrl: '/html/person_groups/view.html',
        controller: 'personGroupViewController'
    });

    $routeProvider.when("/queue_messages/create/pm_ids/:pm_ids/event_id/:event_id", {
        templateUrl: "/html/queue_messages/create.html",
        controller: "createQueueMessagesCtrl"
    });
    $routeProvider.when("/queue_messages/create/pm_ids/:pm_ids", {
        templateUrl: "/html/queue_messages/create.html",
        controller: "createQueueMessagesCtrl"
    });
    $routeProvider.when("/queue_messages/create/log_id/:log_id", {
        templateUrl: "/html/queue_messages/create.html",
        controller: "createQueueMessagesCtrl"
    });
    $routeProvider.when("/queue_messages/create", {
        templateUrl: "/html/queue_messages/create.html",
        controller: "createQueueMessagesCtrl"
    });
    $routeProvider.when("/queue_messages", {
        templateUrl: "/html/queue_messages/index.html",
        controller: "queueMessagesIndexCtrl"
    });

    $routeProvider.when("/shorts", {
        templateUrl: "/html/shorts/index.html",
        controller: "shortsIndexController"
    });
    $routeProvider.when("/shorts/view/:short_id", {
        templateUrl: "/html/shorts/view.html",
        controller: "shortsViewController"
    });

    // Admin/tooltips
    $routeProvider.when("/tooltips", {
        templateUrl: "/html/enum_tooltips/index.html",
        controller: "tooltipsIndexCtrl"
    });
    $routeProvider.when("/tooltips/create", {
        templateUrl: "/html/enum_tooltips/create.html",
        controller: "tooltipsCreateCtrl"
    });
    $routeProvider.when("/tooltips/:dat_id", {
        templateUrl: "/html/enum_tooltips/view.html",
        controller: "tooltipsViewCtrl"
    });
    // Admin/p_kinds
//    $routeProvider.when("/p_kinds", {
//        templateUrl: "/html/enum_p_kinds/index.html",
//        controller: "pKindsIndexCtrl"
//    });
//    $routeProvider.when("/tooltips/create", {
//        templateUrl: "/html/enum_p_kinds/create.html",
//        controller: "pKindsCreateCtrl"
//    });
//    $routeProvider.when("/p_kinds/:dat_id", {
//        templateUrl: "/html/enum_p_kinds/view.html",
//        controller: "pKindsViewCtrl"
//    });
    // Admin/p_types
    $routeProvider.when("/p_types", {
        templateUrl: "/html/enum_p_types/index.html",
        controller: "pTypesIndexCtrl"
    });
    $routeProvider.when("/p_types/create", {
        templateUrl: "/html/enum_p_types/create.html",
        controller: "pTypesCreateCtrl"
    });
    $routeProvider.when("/p_types/:dat_id", {
        templateUrl: "/html/enum_p_types/view.html",
        controller: "pTypesViewCtrl"
    });
    // Admin/tooltips
    $routeProvider.when("/m_c_types", {
        templateUrl: "/html/enum_m_c_types/index.html",
        controller: "mCTypesIndexCtrl"
    });
    $routeProvider.when("/m_c_types/create", {
        templateUrl: "/html/enum_m_c_types/create.html",
        controller: "mCTypesCreateCtrl"
    });
    $routeProvider.when("/m_c_types/:dat_id", {
        templateUrl: "/html/enum_m_c_types/view.html",
        controller: "mCTypesViewCtrl"
    });

    $routeProvider.when("/users", {
        templateUrl: "/html/users/index.html",
        controller: "usersIndexCtrl"
    });
    $routeProvider.when("/users/client_id/:client_id", {
        templateUrl: "/html/users/index.html",
        controller: "usersIndexCtrl"
    });
   $routeProvider.when("/users/create", {
       templateUrl: "/html/users/create.html",
       controller: "usersCreateCtrl"
   });
    $routeProvider.when("/users/:dat_id", {
        templateUrl: "/html/users/view.html",
        controller: "usersViewCtrl"
    });

    $routeProvider.when('/custom_emails', {
        templateUrl: "/html/custom_emails/index.html",
        controller: "customEmailsIndexCtrl"
    });
    $routeProvider.when('/custom_emails/:dat_id', {
        templateUrl: "/html/custom_emails/view.html",
        controller: "customEmailsViewCtrl"
    });
    // Admin - Log Logins
    $routeProvider.when('/log_logins', {
        templateUrl: '/html/log_logins/index.html',
        controller: 'logLoginsIndexCtrl'
    });
    $routeProvider.when('/domains', {
       templateUrl: '/html/domains/index.html',
       controller: 'domainsIndexCtrl'
    });
});
