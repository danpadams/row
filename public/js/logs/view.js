infoChi.controller('logsViewCtrl', ['$scope', '$http', '$routeParams',
    'infoChiSpinner',
    function ($scope, $http, $routeParams, infoChiSpinner) {
        getLog = function () {
            infoChiSpinner.show();
            $http.get('/web/logs/' + $routeParams.log_id).then(
                function (response) {
                    response.data.brief_orig = response.data.brief;
                    // response.data.Action = {resend: false};
                    response.data.direction = 0;
                    if (response.data.action == "Outbound Message") {
                        response.data.resend = true;
                        response.data.direction = 1;
                    }

                    $scope.Log = response.data;
                    getMsgId();
                    infoChiSpinner.hide();
                }
            );
        };
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/logs/detail/' + $routeParams.log_id).then(
                function (response) {
                    $scope.Sections = response.data;
                    console.log('getData()');
                    console.log(response.data);
                    infoChiSpinner.hide();
                }
            );
        };
        getAttachments = function () {
            console.log('/web/logs/attachments/' + $routeParams.log_id);

            // infoChiSpinner.show();
            $http.get('/web/logs/attachments/' + $routeParams.log_id).then(
                function (response) {
                    $scope.Attachments = response.data;
                    console.log('getAttachments()');
                    console.log(response);
                    // infoChiSpinner.hide();
                }
            );
        };
        getMsgId = function () {
            console.log('/web/logs/get_msg_id/' + $routeParams.log_id);

            // infoChiSpinner.show();
            $http.get('/web/logs/get_msg_id/' + $routeParams.log_id).then(
                function (response) {
                    // $scope.Attachments = response.data;
                    console.log('getMsgId()');
                    console.log(response.data);
                    $scope.Log.msg_id = response.data;
                    console.log($scope.Log);
                    // infoChiSpinner.hide();
                }
            );
        };
        $scope.filterPerson = function (item) {
            return true;
        };
        $scope.updBrief = function () {
            infoChiSpinner.show();
            data = {
                "id": $routeParams.log_id,
                'brief': $scope.Log.brief
            };
            console.log('updBrief()');
            $http.post('/web/logs/update/' + $routeParams.log_id, data).then(function (data) {
                getLog();
                infoChiSpinner.hide('mainSpinner');
            });

        }
        $scope.getDirection = function (inbound) {
            if (inbound) {
                return 'Inbound';
            }
            return 'Outbound';
        }
        $scope.showLog = function () {
            getData();
        }


        getLog();
        getData();
        getAttachments();
    }
]);