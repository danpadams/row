infoChi.controller('logsPersonCtrl', ['$scope', '$http', '$routeParams',
    'infoChiSpinner',
    function ($scope, $http, $routeParams, infoChiSpinner) {
        getLog = function () {
            // infoChiSpinner.show();
            // $http.get('/web/logs/' + $routeParams.log_id).then(
            //     function (response) {
            //         response.data.brief_orig = response.data.brief;
            //         // response.data.Action = {resend: false};
            //         response.data.direction = 0;
            //         if (response.data.action == "Outbound Message") {
            //             response.data.resend = true;
            //             response.data.direction = 1;
            //         }
            //
            //         $scope.Log = response.data;
            //         infoChiSpinner.hide();
            //     }
            // );
        };
        $scope.getData = function () {
            getData();
        }
        getData = function () {
            console.log('getData()');
            infoChiSpinner.show();
            $http.get('/web/logs/person/' + $routeParams.person_id).then(
                function (response) {
                    $scope.Sections = response.data;
                    console.log(response.data);
                    infoChiSpinner.hide();
                }
            );
        };
        $scope.filterPerson = function (item) {
            return true;
        };
        $scope.updBrief = function () {
            infoChiSpinner.show();
            data = {
                "id": $routeParams.log_id,
                'brief': $scope.Log.brief
            };
            console.log('updBrief()');
            $http.post('/web/logs/update/' + $routeParams.log_id, data).then(function (data) {
                getLog();
                infoChiSpinner.hide('mainSpinner');
            });

        }

        getLog();
        getData();
    }
]);