infoChi.controller('logsIndexCtrl', ['$scope', '$http', '$localStorage',
    'infoChiSpinner',
    function ($scope, $http, $localStorage, infoChiSpinner) {
        setDefault = function () {
            // Set Default Values for logFilter
            $scope.$storage = $localStorage.$default({
                ['logFilterPerson_' + $scope.client_id]: $scope.noPerson
            });
            $scope.$storage = $localStorage.$default({
                ['logFilterBrief_' + $scope.client_id]: $scope.noBrief
            });
            // Write Current Values for logFilter
            $scope.filterBy.person = $scope.$storage['logFilterPerson_' + $scope.client_id];
            $scope.filterBy.brief = $scope.$storage['logFilterBrief_' + $scope.client_id];
        };
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/logs').then(function (response) {
                // Prepare Blanks of each list
                $scope.people = [];
                $scope.briefs = [];
                personIds = [];
                briefs = [];
                $scope.people.push($scope.noPerson);
                $scope.briefs.push($scope.noBrief);
                // Logs
                // if (response.data.Logs) {
                for (var i = 0; i < response.data.length; i++) {
                    // LogNote Entries
                    for (var j = 0; j < response.data[i].people.length; j++) {
                        // People List
                        if (response.data[i].people[j] &&
                            response.data[i].people[j].id &&
                            personIds.indexOf(response.data[i].people[j].id) === -1) {
                            element = {
                                "id": response.data[i].people[j].id,
                                "label": response.data[i].people[j].label,
                                "fname": response.data[i].people[j].fname,
                                "sname": response.data[i].people[j].sname
                            };
                            $scope.people.push(element);
                            personIds.push(response.data[i].people[j].id);
                        }
                    }
                    // Brief List
                    if (briefs.indexOf(response.data[i].brief) === -1 && response.data[i].brief !== '') {

                        element = {
                            "id": response.data[i].brief,
                            "label": response.data[i].brief
                        };
                        $scope.briefs.push(element);
                        briefs.push(response.data[i].brief);
                    }
                    // Allow Actions
                    response.data[i].Action = {};
                    if (response.data[i].action === "Outbound Message") {
                        response.data[i].Action.resend = true;
                    }
                }

                $scope.logs = response.data;
                infoChiSpinner.hide();
            });
        };
        $scope.filterPerson = function (item) {
            if ($scope.filterBy.person.id === 0) {
                return true;
            }
            for (var i = 0; i < item.people.length; i++) {
                if (item.people[i]) {
                    // && Logic
                    if (item.people[i].id === $scope.filterBy.person.id) {
                        // Test Filter Item Here
                        return true;
                    }
                }
            }
            return false;
        };
        $scope.filterBrief = function (item) {
            if ($scope.filterBy.brief.id === 0) {
                return true;
            }
            if (item.brief === $scope.filterBy.brief.id) {
                // Test Filter Item Here
                return true;
            }
        };
//        $scope.updateFilterBy = function () {
//            // Save new values for filterBy to sessionStorage
//            $scope.$storage['logFilterPerson_' + $scope.client_id] = $scope.filterBy.person;
//            $scope.$storage['logFilterBrief_' + $scope.client_id] = $scope.filterBy.brief;
//        };
        $scope.refresh = function () {
            // Function setup to receive refresh button
            getData();
        }
        // --
        $scope.btns = {};
        $scope.pageSize = 10;
        $scope.filterBy = {};
        $scope.noPerson = {
            "id": 0,
            "label": "Show All"
        };
        $scope.noBrief = {
            "id": 0,
            "label": "Show All"
        };
        getData();
        $scope.filterBy.person = $scope.noPerson;
        $scope.filterBy.brief = $scope.noBrief;

    }
]);