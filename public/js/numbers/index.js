infoChi.controller('numbersIndexCtrl', ['$scope', '$http', 'infoChiSpinner',
    function ($scope, $http, infoChiSpinner) {
        $scope.noPTree = {
            id: "0",
            label: "No pTree Chosen"
        };
        // Get Information
        getClient = function () {
            $scope.valClient = $scope.Current.Client;
            if ($scope.valClient.has_sms) {
                getPTreesSms();
            }
            if ($scope.valClient.has_voice) {
                getPTreesVoice();
            }
        };

        getNumbers = function () {
            infoChiSpinner.show();
            $http.get('/web/numbers').success(function (data) {
                $scope.valNumbers = data;
                console.log($scope.valNumbers);
                infoChiSpinner.hide();
            });
        };
        // SMS - pTrees
        getPTreesSms = function () {
            infoChiSpinner.show();
            $http.get('/web/numbers/1/p_tree').then(function (response) {
                // Set pTreesSms
                $scope.pTreesSms.push($scope.noPTree);
                for (var i = 0; i < response.data.length; i++) {
                    element = {
                        id: response.data[i].id,
                        label: response.data[i].name
                    };
                    $scope.pTreesSms.push(element);
                }
                $scope.$watch('valNumbers', function () {
                    setPTreeSms();
                });
                infoChiSpinner.hide();
            });
        };
        setPTreeSms = function () {
            for (var i = 0; i < $scope.valNumbers.length; i++) {
                p_tree_sms = $scope.valNumbers[i].p_tree_sms;
                for (var j = 0; j < $scope.pTreesSms.length; j++) {
                    if ($scope.pTreesSms[j].id === p_tree_sms) {
                        p_tree_label = $scope.pTreesSms[j].label;
                    }
                }
                number_id = $scope.valNumbers[i].id;
                $scope.pTreeSms[number_id] = {
                    id: p_tree_sms,
                    label: p_tree_label
                };
            }
        };

        getPTreesVoice = function () {
            infoChiSpinner.show();
            $http.get('/web/numbers/2/p_tree').then(function (response) {
                // Set pTreesSms
                $scope.pTreesVoice.push($scope.noPTree);
                for (var i = 0; i < response.data.length; i++) {
                    element = {
                        id: response.data[i].id,
                        label: response.data[i].name
                    };
                    $scope.pTreesVoice.push(element);
                }
                $scope.$watch('valNumbers', function () {
                    setPTreeVoice();
                });
                infoChiSpinner.hide();
            });
        };
        setPTreeVoice = function () {
            for (var i = 0; i < $scope.valNumbers.length; i++) {
                p_tree_voice = $scope.valNumbers[i].p_tree_voice;
                for (var j = 0; j < $scope.pTreesVoice.length; j++) {
                    if ($scope.pTreesVoice[j].id === p_tree_voice) {
                        p_tree_label = $scope.pTreesVoice[j].label;
                    }
                }
                number_id = $scope.valNumbers[i].id;
                $scope.pTreeVoice[number_id] = {
                    id: p_tree_voice,
                    label: p_tree_label
                };
            }
        };


        // Change Selection
        $scope.updPTreeSms = function (number_id) {
            infoChiSpinner.show();
            data = {
                "number_id": number_id,
                "pTree": $scope.pTreeSms[number_id].id,
                "column": "sms"
            };
            $http.post('/web/numbers/update_p_tree', data).then(function (data) {
                infoChiSpinner.hide();
            });
        };
        $scope.updPTreeVoice = function (number_id) {
            infoChiSpinner.show();
            data = {
                "number_id": number_id,
                "pTree": $scope.pTreeVoice[number_id].id,
                "column": "voice"
            };
            $http.post('/web/numbers/update_p_tree', data).then(function (data) {
                infoChiSpinner.hide();
            });
        };
        $scope.refresh = function ()
        {
            initData();
        }
        // Set Variables & Start Processes
        initData = function ()
        {
            $scope.valNumbers = [];
            $scope.pTreeSms = [];
            $scope.pTreesSms = [];
            $scope.pTreeVoice = [];
            $scope.pTreesVoice = [];
            $scope.pageSize = 10;
            if ($scope.current != undefined) {
                getClient();
            } else {
                $scope.$watch('current', function () {
                    getClient();
                });
            }
            getNumbers();
        }

        // --
        initData();
    }
]);
