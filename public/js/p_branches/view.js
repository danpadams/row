infoChi.controller('pBranchesViewCtrl', ['$scope', '$http', '$routeParams', 'infoChiSpinner', 'infoChiList',
    function ($scope, $http, $routeParams, infoChiSpinner, infoChiList) {
        getData = function () {
            // Get Data for pTree
            infoChiSpinner.show();
            $http.get('/web/p_branches/' + p_branch_id).then(function (response) {
                if (response.data.Settings == undefined || response.data.Settings.length == 0) {
                    response.data.Settings = {}
                }
                $scope.pBranch = response.data;

                getTypes();
                $scope.pBranch.type.filename = '/html/p_branches/view-' + $scope.pBranch.type.filename_view + '.html';
                if ($scope.pBranch.next_branch == undefined) {
                    $scope.pBranch.next_branch = {
                        type: {
                            id: 0,
                            name: 'Choose Type'
                        }
                    };
                }
                // Set Greeting - Voice
                if ($scope.pBranch.Settings.Greeting == undefined) {
                    $scope.pBranch.Settings.Greeting = {};
                }
                if ($scope.pBranch.Settings.robo_voice == undefined) {
                    $scope.pBranch.Settings.robo_voice = 'man';
                }
                if ($scope.pBranch.Settings.robo_voice != undefined) {
                    for (var i = 0; i < $scope.greetingOpts.voice.length; i++) {
                        if ($scope.greetingOpts.voice[i].value == $scope.pBranch.Settings.robo_voice) {
                            $scope.pBranch.Settings.Greeting.label = $scope.greetingOpts.voice[i].label;
                        }
                    }
                }
                $scope.show = {'next_module': $scope.pBranch.type.show_next_module};
                // Set Greeting
                infoChiSpinner.hide();
            });
        };

        /**
         * Save the data using $scope variable
         */
        $scope.saveData = function () {
            infoChiSpinner.show();
            data = {
                id: $scope.pBranch.id,
                name: $scope.pBranch.name,
                Settings: $scope.pBranch.Settings
            }
            $http.post('/web/p_branches/save', data).success(function (response) {
                // Add New Data to Current
//                infoChiFlash.show(response.message, 'success');
//                $scope.data = response;
                infoChiSpinner.hide();
            });
        }

        // Next Module Functions
        getTypes = function () {
            infoChiSpinner.show();
            console.log($scope.pBranch.type.p_kind_id);
            $http.get('/web/p_types/kinds/' + $scope.pBranch.type.p_kind_id).then(function (response) {
                $scope.pTypes = response.data;
                infoChiSpinner.hide();
            });
        };
        $scope.setType = function (item, variable) {
            infoChiSpinner.show();
            console.log('Before');
            console.log(item);
            console.log(variable);
            console.log($scope.pBranch[variable]);
            console.log('After');
            // Create Branch of the desired type to use as the next branch in the tree
            var valUrl = '/web/p_branches/generate/' + item.id + '/p_branch/' + $scope.pBranch.p_tree_id + '/' + p_branch_id;
            $http.get(valUrl).then(function (response) {
                $scope.pBranch[variable] = {
                        id: response.data.id,
                        name: response.data.name,
                        type: {'name': item.name}
                };
                if (variable != 'next_branch') {
                    if (!$scope.Settings) {
                        $scope.Settings = {};
                    }
                    $scope.pBranch.Settings[variable] = $scope.pBranch[variable];
                }
                infoChiSpinner.hide();
            });
        };
        // Recipients List
        getRecipContacts = function () {
            infoChiSpinner.show();
            $http.get('/web/lists/contacts_email/').then(function (response) {
                $scope.recip_contacts = response.data;
                infoChiSpinner.hide();
            });
        };
        if ($scope.dropdownChoice == undefined) {
            $scope.dropdownChoice = {};
        }
        $scope.dropdownChoice.recip = function (item) {
            if ($scope.pBranch.Settings.recipients == undefined) {
                $scope.pBranch.Settings.recipients = [];
            }

            $scope.pBranch.Settings.recipients.push(item);
        };
        if ($scope.customFilter == undefined) {
            $scope.customFilter = {};
        }
        $scope.customFilter.recip = function (item) {
            return infoChiList.filter(item.user_id, $scope.pBranch.Settings.recipients, 'user_id')
        };
        if ($scope.remove == undefined) {
            $scope.remove = {};
        }
        $scope.remove.recip = function (key) {
            $scope.pBranch.Settings.recipients.splice(key, 1);
        };
        // Recipients List
        // Greeting Options
        $scope.greetingOpts = {};
        $scope.greetingOpts.voice = [{
            "value": "man",
            "label": "Man"
        }, {
            "value": "woman",
            "label": "Woman"
        }];
        $scope.setGreetingVoice = function (item) {
            $scope.pBranch.Settings.robo_voice = item.value;
        }
        // Greeting Options
        // File - Partials
        if ($scope.file == undefined) {
            $scope.file = {};
        }
        $scope.file.contact_selector = '/html/p_branches/partial-contact_selector.html';
        $scope.file.greeting = '/html/p_branches/partial-greeting.html';
        $scope.file.nm_yn = '/html/p_branches/partial-next_module_yes_no.html';
        // File - Partials
        // Startup Needs - Call Functions rather than directly run code
        p_branch_id = $routeParams.p_branch_id;
        getData();
        getRecipContacts();
    }
]);
