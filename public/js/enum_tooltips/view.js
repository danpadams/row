infoChi.controller('tooltipsViewCtrl', ['$scope', '$http', '$routeParams',
    function ($scope, $http, $routeParams) {
        getInfo = function () {
            // Retreive the data for the Short
            // infoChiSpinner.show();
            $http.get('/web/tooltips/' + dat_id).then(function (response) {
                $scope.Data = response.data;
                // infoChiSpinner.hide();
            });
        };
        $scope.updateData = function () {
            // Update the date for the Short
            data = {
                'id': $scope.Data.id,
                'title': $scope.Data.title,
                'body': $scope.Data.body
            };
            console.log(data);
            // infoChiSpinner.show();
            $http.post('/web/tooltips/update', data).then(function (response) {
                $scope.Data = response.data;
                // infoChiSpinner.hide();
            });
        };
        // --

        dat_id = $routeParams.dat_id;
        getInfo();
    }
]);
