infoChi.controller('mCTypesIndexCtrl', ['$scope', '$http', 'infoChiSpinner',
    function ($scope, $http, infoChiSpinner) {
        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/m_c_types').then(function (response) {
                $scope.data = response.data;
                infoChiSpinner.hide();
            });
        };
        // ==

        getData();
    }
]);
