infoChi.controller('mCTypesCreateCtrl', ['$scope', '$http', 'infoChiSpinner',
    function ($scope, $http, infoChiSpinner) {
        $scope.updateData = function () {
            data = {
                'name': $scope.Data.name,
                'display': $scope.Data.display
            };
            console.log(data);
            infoChiSpinner.show();
            $http.post('/web/m_c_types/create', data).then(function (response) {
                $scope.Data = response.data;
                window.location.href = "#/m_c_types/" + response.data.id;
                infoChiSpinner.hide();
            });
        };
        $scope.updateChoice = function (type) {
            if (type == 'display') {
                console.log('updateChoice');
                $scope.Data.display = $scope.Selected.display.id;
                console.log($scope.Data);
            }
        };
        getDisplay = function () {
            $scope.Choices = {};
            $scope.Choices.display = [
                {id: 'st',
                    label: 'Short Text'
                }, {
                    id: 'lt',
                    label: 'Long Text'
                }, {
                    id: 'enum',
                    label: 'List'
                }

            ];
        }
        setDefault = function () {
            console.log('setDefault()');
            $scope.Selected = {};
            $scope.Data = {};
            $scope.Selected.display = {id: 'enum', label: 'List'};
            $scope.updateChoice('display');
        }
        // --
        getDisplay();
        setDefault();
    }
]);
