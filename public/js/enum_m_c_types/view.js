infoChi.controller('mCTypesViewCtrl', ['$scope', '$http', '$routeParams', 'infoChiSpinner', 'infoChiArray',
    function ($scope, $http, $routeParams, infoChiSpinner, infoChiArray) {
        // -- Level 1 
        getInfo = function () {
            // Retreive the data
            infoChiSpinner.show();
            $http.get('/web/m_c_types/' + dat_id).then(function (response) {
                $scope.Selected.display = infoChiArray.setFromList(response.data.display, $scope.Choices.display);
                $scope.Data = response.data;
                if (response.data.display == 'enum') {
                    getEnumOpts();
                }
                infoChiSpinner.hide();
            });
        };
        $scope.updateData = function () {
            // Update the date for the Short
            data = {
                'id': $scope.Data.id,
                'name': $scope.Data.name,
                'display': $scope.Data.display,
                'client_id': $scope.Data.client_id
            };
            infoChiSpinner.show();
            $http.post('/web/m_c_types/update', data).then(function (response) {
                $scope.Data = response.data;
                infoChiSpinner.hide();
            });
        };
        $scope.updateChoice = function (type) {
            if (type == 'display') {
                $scope.Data.display = $scope.Selected.display.id;
            }
        };
        getDisplay = function () {
            $scope.Choices = {};
            $scope.Choices.display = [
                {id: 'st',
                    label: 'Short Text'
                }, {
                    id: 'lt',
                    label: 'Long Text'
                }, {
                    id: 'enum',
                    label: 'List'
                }
            ];
        };
        // -- Level 1
        // -- Level 2 Text Enum
        $scope.showEnum = function () {
            if ($scope.Data.display == 'enum') {
                return true;
            }
            return false;
        };
        getEnumOpts = function () {
            infoChiSpinner.show();
            $http.get('/web/m_c_types/enum_view/' + dat_id).then(function (response) {
                for (var i = 0; i < response.data.length; i++) {
                    response.data[i].name_orig = response.data[i].name;
                }
                $scope.enumOpts = response.data;
                infoChiSpinner.hide();
            });
        };
        $scope.addEnumOpt = function () {
            data = {
                'mc_type_id': $scope.Data.id,
                'name': $scope.enumNewOpt
            };
            infoChiSpinner.show();
            $http.post('/web/m_c_types/enum_create', data).then(function (response) {
                response.data.name_orig = response.data.name;
                $scope.enumOpts.push(response.data);
                $scope.enumNewOpt = '';
                infoChiSpinner.hide();
            });

        };
        $scope.updateOpt = function (enumOption) {
            data = {
                'id': enumOption.id,
                'name': enumOption.name
            };
            infoChiSpinner.show();
            $http.post('/web/m_c_types/enum_update', data).then(function (response) {
                getEnumOpts();
                infoChiSpinner.hide();
            });

        };
        $scope.removeOpt = function (enumOption) {
            data = {
                'id': enumOption.id
            };
            infoChiSpinner.show();
            $http.post('/web/m_c_types/enum_remove', data).then(function (response) {
                $scope.enumOpts = infoChiArray.removeById(enumOption, $scope.enumOpts);
                getEnumOpts();
                infoChiSpinner.hide();
            });
        };
        // -- Level 2 Text Enum

        // For Level 1, Leve 2 is started from getInfo()
        setDefault = function () {
            dat_id = $routeParams.dat_id;
            $scope.Selected = {};
            $scope.Data = {};
        };
        setDefault();        // --
        getDisplay();
        getInfo();

    }

    // --


]);
