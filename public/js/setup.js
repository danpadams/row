// $location is for Future Development
infoChi.controller('setupController', ['$scope', '$http', '$location', 'infoChiSpinner',
    function ($scope, $http, $location, infoChiSpinner) {
        $scope.submit = function () {
            console.log($scope.data);
            infoChiSpinner.show();
            $http.post('/web/clients/create', $scope.data).then(function (response) {

                if (response.data.client_id) {
                    $scope.data = {};
                } else {
                    msg = response.data.msg;
                    console.log(msg);
                    alert(msg);
                }
                infoChiSpinner.hide();
            });
        };
        //

        $scope.data = {contact: {}};
    }
]);
