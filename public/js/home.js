infoChi.controller("defaultHomeCtrl", ['$scope', '$http', 'infoChiSpinner', 'infoChiFlash',
    function ($scope, $http, infoChiSpinner, infoChiFlash) {
        getData = function () {
            // Feature Buttons
            infoChiSpinner.show();
            $http.get('/web/home/features/').then(function (response) {
                $scope.btns.features = response.data;
                infoChiSpinner.hide();
            }, function myError(response) {
                window.location.href = "/login";
            });

            // Admin Buttons
            getAdminData();

            // Change Client Buttons
            infoChiSpinner.show();
            getCurrent('getCurrent');
            $http.get('/web/home/clients/').then(function (response) {
                $scope.btns.clients = response.data;
                infoChiSpinner.hide();
            }, function myError(response) {
                window.location.href = "login/";
            });
        };
        getAdminData = function () {
            if ($scope.$parent.Current.User.role == 'admin') {
                infoChiSpinner.show();
                $http.get('/web/home/admin/').then(function (response) {
                    $scope.btns.admin = response.data;
                    infoChiSpinner.hide();
                }, function myError(response) {
                    window.location.href = "/login";
                });
            }
        };

        /**
         * Listener pair for Change Client
         */
        $scope.$on('changedClient', function (e) {
            getData();
        });
        $scope.changeClient = function ($id) {
            // Pass to Function in Parent Scope
            $scope.$parent.changeClient($id);
        };

        /**
         * Listener Pair for setCurrent
         */
        $scope.$on('setCurrent', function (e) {
            setCurrent();
        });
        setCurrent = function () {
            getData();
        }

        // --
        $scope.$parent.getCurrent('setCurrent');
        $scope.Name = 'ReachoutWizard';
        $scope.btns = {};
        infoChiFlash.clear();
    }
]);