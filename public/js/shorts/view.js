infoChi.controller('shortsViewController', ['$scope', '$http', '$location', '$routeParams', 'infoChiSpinner',
    function ($scope, $http, $location, $routeParams, infoChiSpinner) {
        getData = function () {
            // Retreive the data for the Short
            if (short_id > 0) {
                infoChiSpinner.show();
                $http.get('/web/shorts/view/' + short_id).then(function (response) {
                    $scope.Data = response.data;
                    // Modify API call as we already have data
                    infoChiSpinner.hide();
                });
            } else {
                setBlank();
            }
        };
        $scope.updateData = function () {
            // Update the date for the Short
            data = $scope.Data;
            infoChiSpinner.show();
            $http.post('/web/shorts/update', data).then(function (response) {
                $scope.Data = response.data;
                infoChiSpinner.hide();
            });
        };
        $scope.getStatus = function () {
            if ($scope.Data) {
                if ($scope.Data.deleted) {
                    return 'Deleted';
                } else {
                    return 'Active';
                }
            }
        };
        $scope.changeStatus = function () {
            if ($scope.Data.deleted) {
                $scope.Data.deleted = 0;
            } else {
                $scope.Data.deleted = 1;
            }
            $scope.updateData();
        };
        $scope.getAction = function () {
            if ($scope.Data) {
                if ($scope.Data.id == 0) {
                    return 'Create';
                } else {
                    return 'View / Edit';
                }
            }
        }
        getBlank = function () {
            return data = {
                "id": 0,
                "name": '',
                "url": '',
            }
        };
        setBlank = function () {
            $scope.Data = getBlank();
        };
        // --

        short_id = $routeParams.short_id;
        getData();
    }
]);
