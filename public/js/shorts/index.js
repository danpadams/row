infoChi.controller('shortsIndexController', ['$scope', '$http', '$localStorage', 'infoChiSpinner',
    function ($scope, $http, $localStorage, infoChiSpinner) {
        $scope.pageSize = 15;

        getData = function () {
            infoChiSpinner.show();
            $http.get('/web/shorts/' + $scope.$storage.deleted).then(function (response) {
                $scope.Data = response.data;
                infoChiSpinner.hide();
            });
        };

        $scope.setDeletedList = function (deleted) {
            $scope.$storage.deleted = deleted;
            getData();
        };

        $scope.$storage = $localStorage.$default({
            deleted: 0
        });

        $scope.showStatus = function () {
            switch ($scope.$storage.deleted) {
                case 0:
                    return 'Active';
                case 1:
                    return 'Deleted';
            }

        };
        $scope.changeStatus = function (item) {
            data = {
                "id": item.id,
                "deleted": !item.deleted
            };
            infoChiSpinner.show();
            $http.post('/web/shorts/update', data).then(function (response) {
                getData();
                infoChiSpinner.hide();
            });
        };

        getData();
    }
]);