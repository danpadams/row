<?php

return [
    'domain' => [
        'System' => env('ROW_DOMAIN_SYSTEM', 'reachoutwizard.com'), // 'SystemDomain'
        'Web' => env('ROW_DOMAIN_WEB', 'reachoutwizard.com'), // 'WebDomain'
        'Short' => env('ROW_DOMAIN_SHORT', 'reachoutwizard.com') // 'ShortDomain;
    ],
    'twilio' => [
        'AccountSid' => env('TWILIO_AccountSid'), // 'AccountSid'
        'AuthToken' => env('TWILIO_AuthToken') // 'AuthToken'
    ],
    'system' => [
        'Name' => 'Reachout Wizard',
        'Port' => '80',
        'Protocol' => 'http'
    ],
    'email' => [
        'Default' => 'webmaster@reachoutwizard.com' // 'DefaultEmail'
    ]
];
