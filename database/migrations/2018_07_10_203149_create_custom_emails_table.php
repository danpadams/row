<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomEmailsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('custom_emails', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 32);
            $table->string('username', 32);
            $table->integer('domain_id');
            $table->text('settings');
            $table->integer('client_id');
            $table->integer('deleted')->default(0);
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::table('custom_emails', function (Blueprint $table) {
            //
        });
    }

}
