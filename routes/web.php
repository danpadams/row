<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

// Signup 16-GlamourNailsByJamie
Route::group(['domain' => 'signup.glamournailsbyjamie.com'], function () {
    Route::get('/', function () {
        return View::make('signup_16');
    })->name('signup');
    Route::post('/web/people/create', 'PersonController@create');
    Route::post('/web/people/add_method', 'PersonMethodController@create');
    Route::post('/web/meta_data/column_add', 'MetaDataController@column_add');
    Route::post('/web/meta_data/update', 'MetaDataController@update');

    Route::get('/web/meta_enum/active/{mc_type_id}', 'MetaEnumController@active');
});

// Default Route - Any Domain Name, Will redirect for Auth
Route::get('/', function () {
    return View::make('ang');
});
Route::get('/home', function () {
    return View::make('ang');
});
Route::get('/ang', function () {
    return View::make('ang');
});
Route::get('/angular', function () {
    return View::make('angular');
});
Route::get('/setup', function () {
    return View::make('setup');
});

Route::get('/landing/signup_glamournailsbyjamie', function () {
        return View::make('signup_16');
})->name('signup');

// Authentication Routes - No Auth Needed Yet
// Auth
$this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
$this->post('login', 'Auth\LoginController@login');
$this->post('logout', 'Auth\LoginController@logout')->name('logout');
// Password Reset
$this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
$this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
$this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
$this->post('password/reset', 'Auth\ResetPasswordController@reset');
// Logout 
Route::get('/logout', 'Auth\LoginController@logout');

// AJAX Client Setup
Route::post('/web/clients/create', 'Web\ClientController@create');

// Landing Signup Page
Route::get('/web/meta_enum/active/{mc_type_id}', 'MetaEnumController@active');// No Auth
Route::post('/web/people/create', 'PersonController@create'); // No Auth for Signup Page\
Route::post('/web/people/add_method', 'PersonMethodController@create');
Route::post('/web/meta_data/column_add', 'MetaDataController@column_add');
Route::post('/web/meta_data/update', 'MetaDataController@update');

// AJAX items
Route::group(['middleware' => ['auth']], function () {
    Route::get('/web/home/features', 'HomeController@features');
    Route::get('/web/home/get_features', 'HomeController@get_features');
    Route::get('/web/home/admin', 'HomeController@admin');
    Route::get('/web/home/clients', 'HomeController@clients');
    Route::get('/web/home/list_clients', 'HomeController@list_clients');
    Route::get('/web/home/current', 'HomeController@current');
});

Route::get('/web/events', 'EventController@index');
Route::get('/web/events/responses', 'EventController@responses');
Route::get('/web/events/view/{event_id}', 'EventController@view');
Route::get('/web/events/{val_num}/people_list', 'EventController@people_list');
Route::get('/web/events/groups', 'EventController@groups');
Route::post('/web/events/update', 'EventController@update');
Route::get('/web/events/{event_id}/invites', 'InviteController@invites');
Route::get('/web/events/{event_id}/attendance', 'EventAttendanceController@attendance');
Route::post('/web/events/delete', 'EventController@delete');

Route::get('/web/invites/responses', 'InviteController@responses');
Route::post('/web/invites/delete', 'InviteController@delete');
Route::post('/web/invites/update', 'InviteController@update');
Route::post('/web/invites/create', 'InviteController@create');

Route::get('/web/attendance/responses', 'EventAttendanceController@responses');
Route::post('/web/attendance/delete', 'EventAttendanceController@delete');
Route::post('/web/attendance/update', 'EventAttendanceController@update');
Route::post('/web/attendance/create', 'EventAttendanceController@create');

Route::get('/web/messages/{message_id}/contact_lookup', 'MessageController@contact_lookup');
Route::get('/web/messages/{message_id}/{person_id}/contact_create', 'MessageController@contact_create');
Route::get('/web/messages/{message_id}/contact_create', 'MessageController@contact_create');
Route::get('/web/messages/{message_id}/users_with_copy/', 'MessageController@users_with_copy');
Route::get('/web/messages/share_users', 'MessageController@share_users');
Route::post('/web/messages/share_notify', 'MessageController@share_notify');
Route::get('/web/messages/{message_id}', 'MessageController@view');
Route::get('/web/messages/{status}/{client_id}', 'MessageController@index');
Route::post('/web/messages/msg_close', 'MessageController@msg_close');
Route::post('/web/messages/msg_mark_spam', 'MessageController@msg_mark_spam');
Route::post('/web/messages/msg_status', 'MessageController@msg_status');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/web/meta_columns/', 'MetaColumnController@index');
    Route::get('/web/meta_columns/people/{id}', 'MetaColumnController@people');
    Route::get('/web/meta_columns/view/{id}', 'MetaColumnController@view');
    Route::post('/web/meta_columns/delete', 'MetaColumnController@delete');
    Route::post('/web/meta_columns/create', 'MetaColumnController@create');
    Route::post('/web/meta_columns/update', 'MetaColumnController@update');
    Route::get('/web/meta_columns/types', 'MetaColumnController@types');
    Route::get('/web/meta_columns/{type_id}', 'MetaColumnController@index');

    Route::get('/web/meta_data/person/{person_id}', 'MetaDataController@person');
    // Route::post('/web/meta_data/column_add', 'MetaDataController@column_add');
    Route::post('/web/meta_data/column_remove', 'MetaDataController@column_remove');
    // Route::post('/web/meta_data/update', 'MetaDataController@update');
});

Route::get('/web/outgoing_messages/{outgoing_message_id}', 'OutgoingMessageController@view');
Route::get('/web/outgoing_messages/', 'OutgoingMessageController@index');
Route::post('/web/outgoing_messages/save', 'OutgoingMessageController@save');
Route::post('/web/outgoing_messages/delete', 'OutgoingMessageController@delete');

Route::group(['middleware' => ['auth']], function () {
    Route::get('/web/meta_enum/options/{mc_type_id}', 'MetaEnumController@options');

    Route::get('/web/meta_enum/boolean', 'MetaEnumController@boolean_type');

    Route::get('/web/logs', 'LogController@index');
    Route::get('/web/logs/{log_id}', 'LogController@view');
    Route::get('/web/logs/detail/{log_id}', 'LogController@detail');
    Route::get('/web/logs/attachments/{log_id}', 'LogController@attachments');
    Route::get('/web/logs/get_msg_id/{log_id}', 'LogController@get_msg_id');
    Route::get('/web/logs/person/{person_id}', 'LogController@person');
    Route::post('/web/logs/update/{log_id}', 'LogController@update');
    Route::get('/web/queue_message/resend/{log_id}', 'QueueMessageController@resend');

    Route::get('/web/people/check', 'PersonController@checkZipCodes');
    //    See top of current file for '/web/people/create'
    Route::post('/web/people/update', 'PersonController@update');
    Route::get('/web/people/find/{method}', 'PersonController@find');
    Route::get('/web/people/view/{person_id}', 'PersonController@view');
    Route::get('/web/people/address', 'PersonController@address');
    Route::get('/web/people/view_methods/{person_id}', 'PersonController@view_methods');
    Route::get('/web/people/view_method_types', 'PersonMethodTypeController@lis');
    //    See top of current file for '/web/people/view_method_types'
    Route::post('/web/people/update_method', 'PersonMethodController@update');
    Route::post('/web/people/delete_method', 'PersonMethodController@delete');
    Route::get('/web/people/distance/{localZipCode}', 'PersonController@distance');
    Route::get('/web/people/{deleted}', 'PersonController@index');

    Route::get('/web/numbers', 'NumberController@index');
    Route::get('/web/numbers/{p_kind_id}/p_tree', 'NumberController@p_tree');
    Route::post('/web/numbers/update_p_tree', 'NumberController@update_p_tree');

    Route::post('/web/users/delete_from_client/', 'UserController@deleteFromClient');
    Route::post('/web/users/add_to_client/', 'UserController@addToClient');
    Route::post('/web/users/create/', 'UserController@create');

});

Route::get('/web/person_groups', 'PersonGroupController@index');
Route::get('/web/person_groups/{person_id}/person', 'PersonGroupController@person');
Route::get('/web/person_groups/view/{person_group_id}', 'PersonGroupController@view');
Route::get('/web/person_groups/detail/{person_group_id}', 'PersonGroupController@detail');
Route::get('/web/person_groups/add_list/{person_group_id}', 'PersonGroupController@add_list');
Route::post('/web/person_groups/add_to_group', 'PersonGroupController@add_to_group');
Route::post('/web/person_groups/remove_from_group', 'PersonGroupController@remove_from_group');
Route::post('/web/person_groups/create', 'PersonGroupController@create');
Route::post('/web/person_groups/delete', 'PersonGroupController@delete');

Route::get('/web/outgoing_messages', 'OutgoingMessageController@index');

Route::post('/web/queue_message/create', 'QueueMessageController@create');
Route::get('/web/queue_message/pending', 'QueueMessageController@pending');

Route::get('/web/tooltip/view/{tooltip_id}', 'TooltipController@view');

Route::get('/web/m_c_types', 'EnumMCTypeController@index');
Route::get('/web/m_c_types/{dat_id}', 'EnumMCTypeController@view');
Route::post('/web/m_c_types/update', 'EnumMCTypeController@update');
Route::post('/web/m_c_types/create', 'EnumMCTypeController@create');
Route::get('/web/m_c_types/enum_view/{dat_id}', 'EnumMCTypeController@enum_view');
Route::post('/web/m_c_types/enum_create', 'EnumMCTypeController@enum_create');
Route::post('/web/m_c_types/enum_update', 'EnumMCTypeController@enum_update');
Route::post('/web/m_c_types/enum_remove', 'EnumMCTypeController@enum_remove');

Route::get('/web/users', 'UserController@index');
Route::post('/web/users/update', 'UserController@update');
Route::get('/web/users/clients/{user_id}', 'UserController@clients');
Route::get('/web/users/people/{user_id}', 'UserController@people');
Route::post('/web/users/upd_password', 'UserController@updPasswd');

Route::group(['middleware' => ['auth']], function () {
    // User Management
    Route::get('/web/users/features_list', 'UserController@features_list');
    Route::get('/web/users/features_user/{user_id}', 'UserController@features_user');
    Route::post('/web/users/feature_add', 'UserController@feature_add');
    Route::post('/web/users/feature_remove', 'UserController@feature_remove');
});
Route::get('/web/users/{dat_id}', 'UserController@view');

Route::get('/web/p_kinds', 'EnumPKindController@index');
Route::get('/web/p_kinds/{dat_id}', 'EnumPKindController@view');
Route::post('/web/p_kinds/update', 'EnumPKindController@update');
Route::post('/web/p_kinds/create', 'EnumPKindController@create');

Route::get('/web/p_types', 'EnumPTypeController@index');
Route::get('/web/p_types/kinds/{p_kind_id}', 'EnumPTypeController@kinds');
Route::get('/web/p_types/{dat_id}', 'EnumPTypeController@view');
Route::post('/web/p_types/update', 'EnumPTypeController@update');
Route::post('/web/p_types/create', 'EnumPTypeController@create');

Route::get('/web/tooltips', 'EnumTooltipController@index');
Route::get('/web/tooltips/{dat_id}', 'EnumTooltipController@view');
Route::post('/web/tooltips/update', 'EnumTooltipController@update');
Route::post('/web/tooltips/create', 'EnumTooltipController@create');

Route::group(['middleware' => ['auth']], function () {
    Route::group(['namespace' => 'Web'], function () {
        Route::get('/web/lists/email', 'ListController@email');
        Route::get('/web/lists/phone', 'ListController@phone');
        Route::get('/web/lists/contact', 'ListController@contacts');
        Route::get('/web/lists/contacts_email', 'ListController@contacts_email');
        Route::get('/web/lists/outgoing_message', 'ListController@outgoing_message');
        Route::get('/web/lists/short', 'ListController@short');
        Route::get('/web/lists/features', 'ListController@features');

        // Admin - Clients
        Route::get('/web/clients', 'ClientController@index');
        Route::get('/web/client/change/{id}', 'ClientController@use_client');
        Route::get('/web/clients/{client_id}/view', 'ClientController@view');
        Route::post('/web/clients/remove_feature', 'ClientController@remove_feature');
        Route::post('/web/clients/give_default_features', 'ClientController@give_default_features');
        Route::post('/web/clients/add_feature', 'ClientController@add_feature');
        Route::post('/web/clients/create_client/', 'ClientController@create_client');

        // Email Parking
        Route::get('/web/custom_emails', 'CustomEmailController@index');
        Route::get('/web/custom_emails/view', 'CustomEmailController@view');
        Route::get('/web/custom_emails/{dat_id}', 'CustomEmailController@view');
        Route::post('/web/custom_emails/save', 'CustomEmailController@save');
        Route::post('/web/custom_emails/delete', 'CustomEmailController@delete');

        // Admin - Domains
        Route::get('/web/domains/admin', 'DomainController@admin');
        Route::get('/web/domains', 'DomainController@index');

        // Admin - LogLogins
        Route::get('/web/log_logins', 'LogLoginController@index');
        Route::get('/web/log_logins/{offset}', 'LogLoginController@index');

        // Reminders
        Route::post('/web/reminders/create', 'ReminderController@create');

        // Attachments
        Route::get('/web/attachment/log_id/{log_id}', 'AttachmentController@getAttachmentsByLogID');
        Route::get('/web/attachment/download/{log_id}', 'AttachmentController@downloadByID');
        Route::post('/web/attachment/upload_file', 'AttachmentController@uploadFile');

        // Custom Email List - User Edit
        Route::get('/web/custom_email_list/{dat_id}', 'CustomEmailListController@index');
        Route::post('/web/custom_email_list/save', 'CustomEmailListController@save');

        // PTrees - Call Flow
        Route::get('/web/p_trees', 'PTreeController@index');
        Route::get('/web/p_trees/{closed}', 'PTreeController@index');
        Route::get('/web/p_trees/view/{p_tree_id}', 'PTreeController@view');
        Route::post('/web/p_trees/update', 'PTreeController@update');
        Route::post('/web/p_trees/change_state', 'PTreeController@change_state');

        // PBranch
        Route::get('/web/p_branches/generate/{p_type_id}/p_tree/{p_tree_id}', 'PBranchController@gen_tree');
        Route::get('/web/p_branches/generate/{p_type_id}/p_branch/{p_tree_id}/{parent_id}', 'PBranchController@gen_branch');
        Route::get('/web/p_branches/{p_branch_id}', 'PBranchController@view');
        Route::post('/web/p_branches/save', 'PBranchController@save');
        
        // Shorts
        Route::get('/web/shorts/{deleted}', 'ShortController@index');
        Route::get('/web/shorts/view/{short_id}', 'ShortController@view');
        Route::post('/web/shorts/update', 'ShortController@update');
    });

    // User Management
    Route::get('/web/users/features_list', 'UserController@features_list');

    // Person Method Checks
    Route::get('/web/person_method/check_client/{pm_id}', 'PersonMethodController@check_client');
    Route::get('/web/person_method/check_recips/{pm_id}', 'PersonMethodController@check_recips');
});

// Shorts - Use It
Route::get('/s/{short_id}', 'ShortController@gotoShort');

// Twilio Endpoints
Route::group(['namespace' => 'Flow'], function () {
    Route::post('/flow/start', 'FlowController@start');
    Route::post('/flow/run/{p_branch_id}/{log_id}', 'FlowController@run');
});
