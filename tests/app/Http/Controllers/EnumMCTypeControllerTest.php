<?php

namespace Tests\App\Http\Controllers;

use App\Model\Tooltip as myModel;
use App\Http\Controllers\TooltipController as myCtrl;
use Illuminate\Http\Request;
use Tests\TestCase;

//use Tests\App\Http\infoChiTestController;

abstract class infoChiTestController extends TestCase {

    public function tearDown() {
        parent::tearDown();

        \Mockery::close();
    }

    public function assertArrayContainsElements($needles, $haystacks) {
        $this->assertTrue(is_array($haystacks));
        $this->assertTrue(is_array($needles));

        foreach ($needles as $key=>$needle) {
            $this->assertEquals( $haystacks[$key], $needle);
        }
    }

}

class EnumMCTypeControllerTest extends infoChiTestController {

    private $sampleData = [
        'id' => 1,
        'label' => '2'
    ];
    private $myModel;
    private $tooltipCtrl;
    private $request;

    public function setup() {
        $this->request = \Mockery::mock(Request::class);
        $this->tooltipCtrl = new myCtrl($this->request);

        $this->myModel = \Mockery::mock('overload:' . myModel::class);
        $this->myModel->shouldReceive('find')->andReturn($this->sampleData);
    }

    public function testConstruct() {
        $this->assertInstanceOf(myCtrl::class, $this->tooltipCtrl);
    }

    public function testView() {
        $Data = $this->tooltipCtrl->view($this->sampleData['id']);
        $this->assertArrayContainsElements($Data, $this->sampleData);
    }

}
