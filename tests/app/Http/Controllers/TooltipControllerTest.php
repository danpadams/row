<?php

namespace Tests;

use App\Model\Tooltip as Model;
use App\Http\Controllers\TooltipController;
use Illuminate\Http\Request;
use Tests\TestCase;

class TooltipControllerTest extends TestCase {

    private $sampleData = [
        'id' => 1,
        'label' => '2'
    ];
    private $model;
    private $tooltipCtrl;
    private $request;

    public function setup() {
        $this->request = \Mockery::mock(Request::class);
        $this->tooltipCtrl = new TooltipController($this->request);

        $this->model = \Mockery::mock('overload:' . Model::class);
        $this->model->shouldReceive('find')->andReturn($this->sampleData);
    }

    public function tearDown() {
        \Mockery::close();
    }

    public function testConstruct() {
        $this->assertInstanceOf(TooltipController::class, $this->tooltipCtrl);
    }

    public function testView() {
        $Data = $this->tooltipCtrl->view($this->sampleData['id']);
        $this->assertTrue(is_array($Data));
    }

}
